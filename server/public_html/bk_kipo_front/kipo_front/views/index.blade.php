@extends($viewPath . '.layouts.default')
@section("video")
    <div class="video-cont">
        <video id="home-video" preload="auto" autoplay="true" loop="true" muted="muted" poster="">
            {{--<source src="/video/{{ $settings['video'] }}" type="video/webm">--}}
            <source src="/video/{{ $settings['video'] }}" type="video/mp4">
        </video>
    </div>
@stop
@section('content')
    <div class="section pad services">
        <div class="black-shadow top"></div>
        <div class="cont">
            {{--<header>--}}
                {{--<h2>{{ $label['heading']['service'] }}</h2>--}}
            {{--</header>--}}
            <div class="list">
                <div class="item">
                    <a class="image-cont" href="{{ $langPrefix }}/service">
                        <img class="head" src="{{ $publicFolder }}/images/icon_production_outline.svg" alt="icon_production_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_production.svg" alt="icon_production">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['production'] }}</h3></a>
                </div>
                <div class="item">
                    <a href="{{ $langPrefix }}/service" class="image-cont">
                        <img class="head" src="{{ $publicFolder }}/images/icon_creative_outline.svg" alt="icon_creative_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_creative.svg" alt="icon_creative">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['creative'] }}</h3></a>
                </div>
                <div class="item">
                    <a class="image-cont" href="{{ $langPrefix }}/service">
                        <img class="head" src="{{ $publicFolder }}/images/icon_casting_outline.svg" alt="icon_casting_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_casting.svg" alt="icon_casting">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['casting'] }}</h3></a>
                </div>
                <div class="item">
                    <a class="image-cont" href="{{ $langPrefix }}/service">
                        <img class="head" src="{{ $publicFolder }}/images/icon_postproduction_outline.svg" alt="icon_postproduction_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_postproduction.svg" alt="icon_postproduction">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['post_production'] }}</h3></a>
                </div>
                <div class="item">
                    <a class="image-cont" href="{{ $langPrefix }}/service">
                        <img class="head" src="{{ $publicFolder }}/images/icon_equipment_outline.svg" alt="icon_equipment_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_equipment.svg" alt="icon_equipment">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['equipment'] }}</h3></a>
                </div>
                <div class="item">
                    <a class="image-cont" href="{{ $langPrefix }}/service">
                        <img class="head" src="{{ $publicFolder }}/images/icon_location_outline.svg" alt="icon_location_outline">
                        <img class="tail" src="{{ $publicFolder }}/images/icon_location.svg" alt="icon_location">
                    </a>
                    <a href="{{ $langPrefix }}/service"><h3>{{ $label['heading']['locations'] }}</h3></a>
                </div>
            </div>
            <div class="black-shadow-mild bottom"></div>
        </div>
    </div>
    @include($viewPath . '.commercials.render', ['viewMore' => 1])
    @if(count($films) > 0)
        <div class="section movie-section">
            <div class="cont">
                <header>
                    <h2>{{ $label['heading']['films'] }}</h2>
                </header>
                <div class="movie-cont cf">
                    @foreach($films as $film)
                        <a href="{{ $langPrefix }}/films">
                            <img class="movie-item down" src="/images/films/large_{{ $film->cover }}" alt="">
                        </a>
                    @endforeach
                </div>
                {{--<div class="more"><a href="{{ $langPrefix }}/films"><span>More films</span><img src="{{ $publicFolder }}/images/icon_view_all.svg" alt="More Films"></a></div>--}}
                <div class="more"><a href="{{ $langPrefix }}/films"><span class="view-more">More films</span><div style="background-image: url('{{ $publicFolder }}/images/icon_view_all.svg')"></div></a></div>
                <div class="black-shadow-mild bottom"></div>
            </div>
        </div>
    @endif
    @include($viewPath. ".tv-projects.render", ['viewMore' => 1])
    @if(count($photography) > 0)
        <div class="section photography-works-section">
            <div class="cont">
                <header>
                    <h2>{{ $label['heading']['photography'] }}</h2>
                </header>
                <div id="slider" class="slider">
                    @foreach($photography as $p)
                        @if($p->cover)
                            <div class="slide">
                                <a href="{{ $langPrefix }}/photography/{{ $p->keyword }}">
                                    <img src="/images/photography/large_{{ $p->cover }}" alt="">
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endif

@stop