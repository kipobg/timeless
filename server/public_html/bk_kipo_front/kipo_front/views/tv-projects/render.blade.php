@if(count($tvProjects) > 0)
    <div class="section tv-projects-section">
        <div class="cont">
            <header>
                <h2>{{ $label['heading']['tv_projects'] }}</h2>
            </header>
            <div class="caption-cont cf">
                @foreach($tvProjects as $key=>$project)
                    <div class="caption-item @if($key % 2 == 0) up @else down @endif"><a
                                href="@getVideoLink([$project->video, $project->type])"
                                data-title="{{ $project->title }}" data-description="{!! $project->description !!}"
                                class="popup-video"><img class="caption"
                                                         src="/images/tv_projects/large_{{ $project->cover }}"
                                                         alt=""><img src="{{ $publicFolder }}/images/icon_play.svg"
                                                                     alt="" class="play"></a></div>
                @endforeach
            </div>
            @if(isset($viewMore))
                {{--<div class="more"><a href="{{ $langPrefix }}/tv-projects"><span class="view-more">View all</span><img--}}
                                {{--src="{{ $publicFolder }}/images/icon_view_all.svg" alt=""></a></div>--}}
        <div class="more"><a href="{{ $langPrefix }}/tv-projects"><span class="view-more">View all</span><div style="background-image: url('{{ $publicFolder }}/images/icon_view_all.svg')"></div></a></div>
            @endif
        </div>
    </div>
@endif