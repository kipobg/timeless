<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>{{ $title }}</title>
    <meta name="msapplication-TileColor" content="#3372DF">
    {{--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">--}}
    <!--vendor css-->
    <link type="text/css" rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>
    <link type="text/css" rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css"/>
    <!--app css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/css/unite-gallery.css">
    <link rel="stylesheet" href="{{ $publicFolder }}/css/main.css">
    <meta name="description" content="{{ $description }}">
    <meta property="fb:app_id" content="199525750518934">
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:site_name" content="Timeless">
    <meta property="og:title" content="{{ $title }}"/>
    <meta property="og:description" content="{{ $description }}"/>
    <meta property="og:image" content=""/>
    {{--<link rel="icon" href="favicon.png" type="image/png" sizes="16x16">--}}
    <style>
        /* --loader-- */
        #loader-main-bg{position:fixed;top:0;right:0;bottom:0;left:0;width:100%;background-color:#fff;z-index:9999}.loader{width:49px;height:49px;position:absolute;top:50%;left:50%;margin:-25px 0 0 -25px;border:3px solid #eeecec;border-radius:50%;border-left-color:transparent;border-right-color:transparent;animation:sp 575ms infinite linear;-o-animation:sp 575ms infinite linear;-ms-animation:sp 575ms infinite linear;-webkit-animation:sp 575ms infinite linear;-moz-animation:sp 575ms infinite linear}@keyframes sp{100%{transform:rotate(360deg)}}@-o-keyframes sp{100%{-o-transform:rotate(360deg);transform:rotate(360deg)}}@-ms-keyframes sp{100%{-ms-transform:rotate(360deg);transform:rotate(360deg)}}@-webkit-keyframes sp{100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@-moz-keyframes sp{100%{-moz-transform:rotate(360deg);transform:rotate(360deg)}}
    </style>
</head>

<body>
@foreach($styles as $style)
    <link rel="stylesheet" href="{{ $style }}">
@endforeach
<!--app css-->
<div class="wrapper">
    <div id="loader-main-bg">
        <div class="loader"></div>
    </div>
    <div class="inner-wrapper">
        <!-- basic nav header -->
        <header class="main">
            <div class="cont">
                <a href="{{ $langPrefix }}/" id="logo-top" class="logo top " style="display: none"><img
                            src="{{ $publicFolder }}/images/logo.svg" alt="Timeless Production Group"></a>
                <nav id="menu" class="main header">
                    <ul class="cf">
                        <li><a href="{{ $langPrefix }}/">Home</a></li>
                        <li><a href="{{ $langPrefix }}/about">About</a></li>
                        <li><a href="{{ $langPrefix }}/service">Service</a></li>
                        <li><a href="{{ $langPrefix }}/commercials">Commercials</a></li>
                        <li><a href="{{ $langPrefix }}/films">Films</a></li>
                        <li><a href="{{ $langPrefix }}/tv-projects">TV</a></li>
                        <li><a href="{{ $langPrefix }}/photography">Photographies</a></li>
                        <li><a href="{{ $langPrefix }}/contacts">Contact</a></li>
                    </ul>
                </nav>
                <nav id="side-menu" class="side-menu">
                    <ul class="cf">
                        <li><a href="{{ $langPrefix }}/">Home</a></li>
                        <li><a href="{{ $langPrefix }}/about">About</a></li>
                        <li><a href="{{ $langPrefix }}/service">Service</a></li>
                        <li><a href="{{ $langPrefix }}/commercials">Commercials</a></li>
                        <li><a href="{{ $langPrefix }}/films">Films</a></li>
                        <li><a href="{{ $langPrefix }}/tv-projects">TV</a></li>
                        <li><a href="{{ $langPrefix }}/photography">Photographies</a></li>
                        <li><a href="{{ $langPrefix }}/contacts">Contact</a></li>
                    </ul>
                </nav>
                <div id="menu-btn" class="menu-mobile-btn burger">
                    <div class="top"></div>
                    <div class="middle"></div>
                    <div class="bottom"></div>
                </div>
            </div>
        </header>
        <!--.basic nav header -->
        <!-- front -->
        <div id="front" class="front">
            @yield("video")
            <div id="front-bgr" class="front-bgr">
                <div id="scene">
                    {{--0.16--}}
                    <div data-depth="0.20" id="logo-center-parallax-1" >
                        <a href="{{ $langPrefix }}/" class="logo center logo-center"><img
                                    src="{{ $publicFolder }}/images/logo1.svg" alt="Timeless Production Group"></a>
                    </div>
                    {{--0.12--}}
                    <div data-depth="0.16" id="logo-center-parallax-2">
                        <a href="{{ $langPrefix }}/" class="logo center logo-center"><img
                                    src="{{ $publicFolder }}/images/logo1.svg" alt="Timeless Production Group"></a>
                    </div>
                    {{--0.08--}}
                    <div data-depth="0.12" id="logo-center-parallax-3" class="logo center logo-center">
                        <a href="{{ $langPrefix }}/">
                            <img src="{{ $publicFolder }}/images/logo.svg" alt="Timeless Production Group">
                        </a>
                    </div>
                    <div data-depth="0.16" id="logo-center-parallax-4"><a href="{{ $langPrefix }}/" class="logo center logo-center"><img
                                    src="{{ $publicFolder }}/images/logo1.svg" alt="Timeless Production Group"></a>
                    </div>
                    <div data-depth="0.20" id="logo-center-parallax-5"><a href="{{ $langPrefix }}/" class="logo center logo-center"><img
                                    src="{{ $publicFolder }}/images/logo1.svg" alt="Timeless Production Group"></a>
                    </div>
                </div>
            </div>
        </div>
        <!--.front -->
    <!-- main cont -->
        <div id="main-cont" class="main-cont">
            <!-- .section cont -->
            <div id="section-cont" class="section-cont">
            @yield("content")
            <!-- footer -->
                <footer class="main">
                    <div class="cont shadow-bgr cf">
                        <div class="item">
                            <img src="{{ $publicFolder }}/images/icon_address.svg" alt="icon_address">
                            <p>31B,Gurgulqt Str. 1463 Sofia,BULGARIA</p>
                        </div>
                        <div class="item">
                            <img src="{{ $publicFolder }}/images/icon_tel.svg" alt="icon_tel">
                            <p>0888 40 70 36 / 0885 82 72 67</p>
                        </div>
                        <div class="item">
                            <img src="{{ $publicFolder }}/images/icon_e-mail.svg" alt="icon_email">
                            <p>timelessbulgaria@tpg-bulgaria.com</p>
                        </div>
                    </div>
                    <div class="credits cf">
                        <div class="copy-rights">&copy; Timeless All Rights Reserved</div>
                        <div class="agency">Website by <a href="https://kipo.bg/">Kipo</a></div>
                    </div>
                </footer><!--.footer -->
            </div> <!-- .section cont -->
        </div> <!-- .main cont -->
    </div><!--.inner-wrapper-->
</div><!--.wrapper-->
<!--app scripts-->
@foreach($footer['scripts'] as $script)
    <script src="{{ $script }}"></script>
@endforeach

<!--app scripts-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


<script src="{{ $publicFolder }}/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/js/unitegallery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/themes/tiles/ug-theme-tiles.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

<script>
    var mfp = $.magnificPopup;

    function linkEvents() {
        $('.popup-video').magnificPopup({
            type: 'iframe',
            callbacks: {
                markupParse: function (template, values, item) {
                    var mp = $.magnificPopup.instance;
                    t = $(mp.currItem.el[0]);

                    var title = t.data('title');
                    var description = t.data('description');
                    template.find(".mfp-title").text(title);
                    template.find(".mfp-description").text(description);
                }
            },
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: function (url) {
                            var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                            if (!m || !m[1]) return null;
                            return m[1];
                        },
                        src: '//www.youtube.com/embed/%id%?modestbranding=1&autohide=1&showinfo=0&autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: function (url) {
                            var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
                            if (!m || !m[5]) return null;
                            return m[5];
                        },
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    }
                },
                markup: '<div style="min-width:320px; height: 100%">' +
                '<div class="mfp-iframe-scaler" style="overflow: inherit" >' +
                '<div class="mfp-close"></div>' +
                '<div class="mfp-title" style="display: block; color: white; position: absolute; top: -19px; z-index: 1;"></div>' +
                '           <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                '<div class="mfp-description" style="color: white; position: relative; top: 15px;"></div>' +
                '</div>' +
                '</div>'
//                markup: '<div style="' + 900 + 'px;">' +
//                '<div class="mfp-iframe-scaler" style="overflow: inherit" >' +
//                '<div class="mfp-close"></div>' +
//                '<div class="mfp-title" style="display: block; color: white; position: absolute; top: -19px; z-index: 1;"></div>' +
//                '           <iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
//                '<div class="mfp-description" style="color: white; position: relative; top: 15px;"></div>' +
//                '</div>' +
//                '</div>'
            },
            closeMarkup: '<span title="%title%" class="mfp-close" style="z-index: 999999999; background-image: url(/kipo_front/images/close.svg); background-repeat: no-repeat; width: 80px; height: 80px;"></span>'
        });
    }

    linkEvents();
    var scene = document.getElementById('scene');
    new Parallax(scene, {
        relativeInput: true
    });
    $(document).ready(function () {
        var gallery = $('#gallery');
        if(gallery.length > 0) {
            gallery.unitegallery({
                lightbox_type: "compact",
                lightbox_arrows_position: "inside"
            });
        }
    })
</script>
</body>
</html>
