@extends('kipo_front.views.layouts.default')
@section('content')
@if ($errors->any() && !$errors->has('msg'))
<div class='danger'>
    @foreach ($errors->all() as $error)
    {{ $error }} <br>
    @endforeach
</div>
@endif
<div class="login">
    <div class="heading middle">
        <h1>{{ $title }}</h1>
    </div>
    <div class="login-form">  
        <form action="reset-password" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="email" value="{{$email}}" />
            {!! Form::label('password', $label['password']) !!}
            {!! Form::password( 'password', ['class' => 'pass'] ) !!}
            {!! Form::label('password_confirmation', $label['password_confirm']) !!}
            {!! Form::password( 'password_confirmation', ['class' => 'pass'] ) !!}
            <button class="btn">{{ $label['front_login']['send'] }}</button>
        </form>
    </div>
</div>
@stop