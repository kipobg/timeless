@extends('kipo_front.views.layouts.default')
@section('content')
@if ($errors->has() && !$errors->has('msg'))
<div class='danger'>
	@foreach ($errors->all() as $error)
	{{ $error }} <br>
	@endforeach
</div>
@endif
@if(session('status') != null)
<div class="success">{{ session('status') }}</div>
@endif
<div class="login">
	<div class="heading middle">
		<h1>{{ $title }}</h1>
	</div>
	<div class="login-form">
		
		{!! Form::open(['url' => $url, 'method' => 'POST']) !!}
		{!! Form::label('email', $label['email']) !!}
		{!! Form::email( 'email', null, ['class' => 'email'] )!!}
		<button class="btn">{{ $label['send'] }}</button>
		{!! Form::close() !!}
	</div>
</div>
@stop