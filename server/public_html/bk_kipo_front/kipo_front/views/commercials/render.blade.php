@if(count($commercials) > 0)
    <div class="section pad commercials-section">
        <div class="cont">
            <header>
                <h2>{{ $label['heading']['commercials'] }}</h2>
            </header>
            <div class="caption-cont cf">
                @foreach($commercials as $key=>$commercial)
                    <div class="caption-item  @if($key % 2 == 0) down @else up @endif"><a href="@getVideoLink([$commercial->video, $commercial->type])" data-title="{{ $commercial->title }}" data-description="{!! $commercial->description  !!}" class="popup-video"><img class="caption" src="/images/commercials/large_{{ $commercial->cover }}" alt="{{ $commercial->title }}"><img src="{{ $publicFolder }}/images/icon_play.svg" alt="" class="play"></a></div>
                @endforeach
            </div>
            @if(isset($viewMore))
                {{--<div class="more"><a href="{{ $langPrefix }}/commercials"><span>View all</span><img src="{{ $publicFolder }}/images/icon_view_all.svg" alt="View All"></a></div>--}}
                <div class="more"><a href="{{ $langPrefix }}/commercials"><span class="view-more">View all</span><div style="background-image: url('{{ $publicFolder }}/images/icon_view_all.svg')"></div></a></div>
            @endif
        </div>
    </div>
@endif