@extends($viewPath . '.layouts.default')
@section('content')
    <div class="list-section">
        @include($viewPath . '.general.breadcrumbs')
        <div class="br gray-bg"></div>
        <div class="section recipe-list-section {{ isset($page->class) ? $page->class : '' }}"
            {{--<div class="cont cf"--}}
            @if($page->cover) style="background-image: url({{ $pageImgFolder . '/' . $page->cover }})" @endif>
            @if(!(isset($page->show_title) && !$page->show_title))
                <header>
                    <h2>{{ $page->title }}</h2>
                </header>
                {{--<div class="col-m-4 left">--}}
                {{--<div class="image-cont">--}}
                {{--<img src="/kipo_front/images/icon_creative.svg" alt="icon_creative">--}}
                {{--</div>--}}
                {{--</div>--}}
            @endif
            {{--<div class="col-m-8 left">--}}
            {{--<p >{!! $page->description !!}</p>--}}
            {!! $page->description !!}
            {{--</div>--}}
            <div class="module">
                {!! $modules !!}
            </div>
            {{--</div>--}}
        </div>
    </div>
@stop