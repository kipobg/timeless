@extends($viewPath . '.layouts.default')
@section('content')
    {{--<div class="section pad commercials-section" id="gallery">--}}
    <div  class="section pad">
        <div class="cont">
            <header>
                <h2>{{ $photography->title }}</h2>
            </header>
            {{--<div id="gallery" class="caption-cont cf">--}}
                {{--@foreach($gallery as $key=>$g)--}}
                    {{--<div class="caption-item  @if($key % 2 == 0) down @else up @endif"><img class="caption" src="/images/photography/gallery/thumbnail_{{ $g->cover }}" alt="{{ $g->title }}"></div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
            <div class="gallery-cont">
                <div id="gallery" >
                    @foreach($gallery as $key=>$g)
                        <img src="/images/photography/gallery/thumbnail_{{ $g->cover }}"
                             data-image="/images/photography/gallery/{{ $g->cover }}"
                             data-description="{{ $g->title }}"
                             alt="{{ $g->title }}">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop