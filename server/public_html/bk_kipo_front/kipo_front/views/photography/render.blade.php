@if(count($photography) > 0)
    <div class="section photography-works-section">
        <div class="caption-cont cf">
            @foreach($photography as $p)
                    @if($p->cover)
                        <div class="caption-item   down ">
                            <a href="{{ $langPrefix }}/photography/{{ $p->keyword }}">
                                <img src="/images/photography/large_{{ $p->cover }}" alt="">
                            </a>
                        </div>
                    @endif
            @endforeach
        </div>
    </div>
@endif