{{--<div class="movie-cont">--}}
{{--@foreach($films as $film)--}}
{{--<div class="films">--}}
{{--<a href="{{ $langPrefix }}/films/{{ $film->keyword }}">--}}
{{--<img class="movie-item down" src="/images/films/large_{{ $film->cover }}" alt="film-image">--}}
{{--</a>--}}
{{--<div class="film-container">--}}
{{--<h3 class="heading">{{ $film->title }}</h3>--}}
{{--<p class="description">{!!  $film->description !!}</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endforeach--}}
{{--</div>--}}


<div class="movie-cont cf">
    @foreach($films as $key=>$film)
        {{--<div class="{{ $key % 2 != 0 ? 'col-m-4 right' : 'col-m-4 left' }}">--}}
        @if( $key % 2 == 0)
            <div class="film-cont"><div class="img-cont"><img class="movie-item" src="/images/films/large_{{ $film->cover }}" alt="film-image"></div><div class="text-cont-right" style="{{ 'order:' . $key }}"><header><h2 class="heading">{{ $film->title }}</h2></header><p class="description">{!!  $film->description !!}</p></div></div>
            @else
                <div class="film-cont"><div class="text-cont-right" style="{{ 'order:' . $key }}"><header><h2 class="heading">{{ $film->title }}</h2></header><p class="description">{!!  $film->description !!}</p></div><div class="img-cont"><img class="movie-item" src="/images/films/large_{{ $film->cover }}" alt="film-image"></div>
                </div>
        @endif
    @endforeach
</div>