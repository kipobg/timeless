@foreach($leftNavBar as $page)
    <li class=" 
        @if(in_array($page->base_keyword, $urlParams) && count($page->childrens) > 0)
           menu-open
        @endif

        @if(isset($urlParams[0]) && $urlParams[0] == $page->base_keyword)
            menu-active-link
        @endif">       

            <a href="{{ $langPrefix . $page->keyword }}">{{ $page->title }}</a>
            @if(count($page->childrens) > 0)
                <span class="arrow expand 
                    @if( in_array($page->base_keyword, $urlParams) && $page->base_keyword != $urlParams[0]) 
                        active
                    @endif">
                </span>
                <ul class="drop clean 
                    @if( in_array($page->base_keyword, $urlParams) && $page->base_keyword != $urlParams[0]) 
                        active
                    @endif">
                    @include('kipo_front.views.general.left-nav', ['leftNavBar' => $page->childrens])
                </ul>
            @endif
    </li>

@endforeach
