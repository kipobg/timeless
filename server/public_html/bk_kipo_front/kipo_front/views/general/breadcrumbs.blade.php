<div class="breadcrumbs">
    <div class="cont">
        @foreach($breadcrumbs as $key=>$breadcrumb)
            <a href="{{ $breadcrumb['href'] }}">{{ $breadcrumb['title'] }}</a>
            @if($key > 0)
                /
            @endif
        @endforeach
    </div>
</div>