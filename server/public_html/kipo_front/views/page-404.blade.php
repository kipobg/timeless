@extends($viewPath . '.layouts.default')
@section('content')
    <div class="intro">
            <h1 style="text-align: center">{{ $label['page404'] }}</h1>
        </div>
    </div>
@stop