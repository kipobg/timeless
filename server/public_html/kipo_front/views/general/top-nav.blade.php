@foreach($topMenu as $page)
    <li>
        <a href="{{ \App\KipoModel::checkLangCode($locale) . $page->keyword }}">{{ $page->title }}</a>
        @if(count($page->childrens) > 0)
            <span class="expand">+</span>
            <ul class="drop-ul clean">
                @include('kipo_front.views.general.top-nav', ['topMenu' => $page->childrens])
            </ul>
        @endif
    </li>
@endforeach