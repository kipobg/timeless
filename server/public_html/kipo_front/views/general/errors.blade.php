@if($errors->has('success'))
    <div class="success">
        <div>{{$errors->first('success')}}</div>
    </div>
@elseif($errors->any())
    <div class='message'>
        @foreach ($errors->all() as $error)
            {{ $error }} <br>
        @endforeach
    </div>
@endif