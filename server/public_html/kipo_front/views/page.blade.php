@extends($viewPath . '.layouts.default')
@if($videos)
    @foreach($videos as $video)
@section("video")
    <div class="video-cont">
        {{--autobuffer--}}
        {{-- autoplay loop  autobuffer preload="auto" muted playsinline--}}


        {{--crossorigin="anonymous" autoplay="true" loop="true" muted="true" preload="false" webkit-playsinline="true" playsinline="true"--}}
        <video id="home-video" class="video" autoplay="true" loop="true" muted="true" preload="false" webkit-playsinline="true" playsinline="true" poster="/images/gallery/{{ $video->cover }}">
            {{--<source src="/video/{{ $settings['video'] }}" type="video/webm">--}}
            <source src="/images/gallery/{{ $video->video }}" type="video/mp4">
        </video>
    </div>
@stop
@endforeach
@endif
@section('content')
    <div class="list-section">
        @include($viewPath . '.general.breadcrumbs')
        <div class="br gray-bg"></div>
        <div class="section recipe-list-section {{ isset($page->class) ? $page->class : '' }}"
            {{--<div class="cont cf"--}}
            @if($page->cover) style="background-image: url({{ $pageImgFolder . '/' . $page->cover }})" @endif>
            @if(!(isset($page->show_title) && !$page->show_title))
                <header>
                    <h2>{{ $page->title }}</h2>
                </header>
                {{--<div class="col-m-4 left">--}}
                {{--<div class="image-cont">--}}
                {{--<img src="/kipo_front/images/icon_creative.svg" alt="icon_creative">--}}
                {{--</div>--}}
                {{--</div>--}}
            @endif
            {{--<div class="col-m-8 left">--}}
            {{--<p >{!! $page->description !!}</p>--}}
            {!! $page->description !!}
            {{--</div>--}}
            <div class="module">
                {!! $modules !!}
            </div>
            {{--</div>--}}
        </div>
    </div>
@stop