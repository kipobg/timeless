<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <base href="{{ URL::to('/') }}" >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .inner-container {
                width: 100% !important;
            }
            .full-width-img {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }
        @media only screen and (max-width: 540px) {
            .columnContainer {
                display: block !important;
                width: 100% !important;
            }
            .col_1_2 {
                display: block !important;
                width: 100%!important;
                height: auto;
                padding: 0px !important;
            }
            .col_1_2 img {
                margin: 10px auto;
            }
            .col_40 {
                display: block !important;
                width: 100% !important;
                padding: 0 0 10px 0;
            }
            .links td,
            .social td,
            .contacts td {
                padding: 0!important;
                text-align: center!important;
            }
            .contacts {
                height: 120px!important;
            }
            .padding-reset {
                padding: 0!important;
            }
            .services-col td {
                display: block!important;
                width: 100%!important;
                padding: 5px 0 5px 0;
            }
            .services .heading {
                height: 90px!important;
            }
            .focus .inner-container, .services .inner-container {
                padding-top: 20px!important;
                padding-bottom: 20px!important;
            }
        }
    </style>

</head>

<body style="margin: 0; padding: 0; line-height: 1.8em; font-family: verdana, sans-serif; font-size: 14px; color: #666666; ">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="line-height: 1.8em; font-family: verdana, sans-serif; font-size: 14px; background-color: #ecf0f1; padding: 20px 0 20px 0;">
    <tr>
        <td>
            <table class="inner-container" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;  background-color: #ffffff;">
                <tr class="header">
                    <td style="">

                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                            <tr>
                                <td class="col_1_2" width="50%" style="padding: 30px 30px 30px 30px;">
                                    <a href="#" target="_blank">
                                        <img style="display: block;" src="{{URL::to('/')}}/images/email/logo.png" alt="Logo" />
                                    </a>
                                </td>
                                <td class="col_40" width="40%">
                                    &nbsp;
                                </td>
                                <td width="10%" style="padding: 0 0 0px 0px" align="right" valign="top">
                                    <img src="{{URL::to('/')}}/images/email/corner-top-right.png" alt="" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <table class="outer-container" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;  background-color: #dcdfe0;">
                <tr>
                    <td>
                        <table class="inner-container" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;  background-color: #ffffff;">
                            <tr class="front" style="height: auto;">
                                <td style="height: auto;">
                                    <img class="full-width-img" style="display: block;" src="{{URL::to('/')}}/images/email/header.png?v=2" alt="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <!--Content goes here-->
            <table class="inner-container" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;  background-color: #ffffff;">
                <tr class="section" style="border-bottom: 1px dashed #bdbdbd">
                    <td style="text-align: left;">

                        <table cellpadding="0" cellspacing="0" width="100%" style="padding: 25px 25px 25px 25px;">
                            <tr>
                                <td class="heading" height="40px" valign="top" style="font-size: 24px; color: #333333;">{{ $title }}</td>
                            </tr>
                            <tr>
                                <td class="text-reg" style="font-size: 14px;">
                                    {!!  strip_tags($content, '<br><a>') !!}
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--//Content goes here-->

            <table class="inner-container" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;  background-color: #ffffff;">
                <tr class="contacts" style="height: 80px;">
                    <td style="">
                        <table cellpadding="0" cellspacing="0" width="100%" style="font-size: 18px; font-weight: bold">
                            <tr>
                                <td class="col_1_2 footer-col" width="50%" align="left" style="text-align: center; vertical-align: middle;">
                                    <img style="vertical-align: middle;" src="{{URL::to('/')}}/images/email/icon-phone.png" alt="" /> +359 89 866 779 0</td>
                                <td class="col_1_2 footer-col" width="50%" align="right" style="text-align: left; vertical-align: middle;">
                                    <img style="vertical-align: middle;" src="{{URL::to('/')}}/images/email/icon-mail.png" alt="" /> play@displaybg.com</td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                            <tr style="height: 15px;">
                                <td style="padding: 0 0 0 0;" align="left" valign="bottom">
                                    <img style="display: block;" src="{{URL::to('/')}}/images/email/corner-bottom-left.png" alt="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="inner-container" align="center" border="0" cellpadding="0" cellspacing="0" width="600px" style="border-collapse: collapse;  background-color: #ffffff;">
                <tr class="footer">
                    <td style="text-align: center;">
                        <table cellpadding="0" cellspacing="0" width="100%" style="padding: 25px 25px 25px 25px; line-height: 1.3em; font-family: arial; font-style: italic; font-size: 13px; background-color: #ecf0f1;">
                            <tr>
                                <td>С уважение, Екипът на DisplayBg.</td>
                            </tr>
                            <tr>
                                <td>Това съобщение е генерирано автоматично, моля, не отговаряйте.
                                    <br /> Уеб дизайн и програмиране от <a class="link-orange" style="color: #00ADEF; text-decoration: none;" href="http://kipo.bg" target="_blank">kipo.bg</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
</body>

</html>