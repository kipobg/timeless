@extends('kipo_front.views.layouts.default')
@section('content')
@if ($errors->any() && !$errors->has('msg'))
<div class='danger'>
    @foreach ($errors->all() as $error)
    {{ $error }} <br>
    @endforeach
</div>
@endif
<div class="cont">
<div class="heading">
    <h1>създаване на нов профил</h1>

</div>
    
    <form action="{{url('register')}}" method="POST">
        {{ csrf_field() }}
        <div class="name col-8-m custom">
            {!! Form::label('name', $label['name'], ['class' => 'name']) !!}
            {!! Form::text( 'name', null, ['class' => 'name'] )!!}
        </div>
        <div class="col-4-m custom ssn right">
            {!! Form::label('ssn', $label['ssn']) !!}
            {!! Form::text( 'ssn', null, ['class' => 'ssn'] )!!}
        </div>
        <div class="city col-4-m custom">
            {!! Form::label('city', $label['city']) !!}
            {!! Form::text( 'city', null, ['class' => 'city'] )!!}
        </div>
        <div class="address col-8-m custom right">
            {!! Form::label('address', $label['address']) !!}
            {!! Form::text( 'address', null, ['class' => 'address'] )!!}
        </div>
        <div class="phone col-6-m custom">
            {!! Form::label('phone', $label['phone']) !!}
            {!! Form::text( 'phone', null, ['class' => 'phone'] )!!}
        </div>
        <div class="mail col-6-m custom right">
            {!! Form::label('email', $label['email']) !!}
            {!! Form::email( 'email', null, ['class' => 'email'] )!!}
        </div>
        
        <div class="pass col-6-m custom">
            {!! Form::label('password', $label['password']) !!}
            {!! Form::password( 'password', ['class' => 'pass'] ) !!}
        </div>
        <div class="pass-conf col-6-m custom right">
            {!! Form::label('password_confirmation', $label['password_confirm']) !!}
            {!! Form::password( 'password_confirmation', ['class' => 'pass'] ) !!}
        </div>
        <div class="pass-conf col-12-m custom right">
            {!! Form::checkbox('terms-conditions', null, null, ['class' => 'terms'] ) !!}
            {!! Html::decode(Form::label('terms-conditions', sprintf($label['terms_conditions'], '/registraciya-obshti-usloviya') )) !!}
        </div>
        <button class="btn cf">{{ $label['register'] }}</button>
    </form>
</div>
@stop