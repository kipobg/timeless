@php
    $index = 0
@endphp

@if(count($commercials) > 0)
    @if(!\Request::get('ajax'))
        <div class="section pad commercials-section @if(isset($isInner)) ajax @endif">

            <div class="cont">
                <header>
                    <h2>{{ $label['heading']['commercials'] }}</h2>
                </header>
                <div class="caption-cont cf">
                    @foreach($commercials as $key=>$commercial)
                        <div class="caption-item @if($key == 1 || $index == 2) up @else down @endif "><a href="@getVideoLink([$commercial->video, $commercial->type])" data-title="{{ $commercial->title }}" data-description="{!! $commercial->description  !!}" class="popup-video"><img class="caption" src="/images/commercials/large_{{ $commercial->cover }}" alt="{{ $commercial->title }}"><img src="{{ $publicFolder }}/images/icon_play.svg" alt="" class="play"></a></div>
                        @php
                            if($key == 1 || $index == 2) {
                                $index = 0;
                            } else {
                                $index++;
                            }
                        @endphp

                        {{--<div class="caption-item"><a href="@getVideoLink([$commercial->video, $commercial->type])" data-title="{{ $commercial->title }}" data-description="{!! $commercial->description  !!}" class="popup-video"><img class="caption" src="/images/commercials/large_{{ $commercial->cover }}" alt="{{ $commercial->title }}"><img src="{{ $publicFolder }}/images/icon_play.svg" alt="" class="play"></a></div>--}}
                    @endforeach
                </div>

                @if(isset($viewMore))
                    {{--<div class="more"><a href="{{ $langPrefix }}/commercials"><span>View all</span><img src="{{ $publicFolder }}/images/icon_view_all.svg" alt="View All"></a></div>--}}
                    <div class="more"><a href="{{ $langPrefix }}/commercials"><span class="view-more">View all</span><div style="background-image: url('{{ $publicFolder }}/images/icon_view_all.svg')"></div></a></div>
                @endif
            </div>
        </div>
    @else
        @foreach($commercials as $key=>$commercial)
            <div class="caption-item @if($key == 1 || $index == 2) up @else down @endif "><a href="@getVideoLink([$commercial->video, $commercial->type])" data-title="{{ $commercial->title }}" data-description="{!! $commercial->description  !!}" class="popup-video"><img class="caption" src="/images/commercials/large_{{ $commercial->cover }}" alt="{{ $commercial->title }}"><img src="{{ $publicFolder }}/images/icon_play.svg" alt="" class="play"></a></div>
            @php
                if($key == 1 || $index == 2) {
                    $index = 0;
                } else {
                    $index++;
                }
            @endphp

            {{--<div class="caption-item"><a href="@getVideoLink([$commercial->video, $commercial->type])" data-title="{{ $commercial->title }}" data-description="{!! $commercial->description  !!}" class="popup-video"><img class="caption" src="/images/commercials/large_{{ $commercial->cover }}" alt="{{ $commercial->title }}"><img src="{{ $publicFolder }}/images/icon_play.svg" alt="" class="play"></a></div>--}}
        @endforeach
    @endif
    @if(isset($isInner))
        @if(!\Request::get('ajax'))
            <script>
                @if($commercials->lastPage() > $commercials->currentPage())
                    var scrollPage = true;
                @endif
                var url = '{{ $langPrefix }}/commercials-ajax';
                var page = '{{ $commercials->currentPage() }}';

            </script>
        @endif
    @endif
@endif