import ui from './main'

function Scroll (spec) {
    let { root = '' } = spec;
    let $el = $(root);
    let $logoTop = $('#logo-top');
    let $logoCenter = $('.logo-center');
    let state = {
        top: true,
    };
    const $win = $(window);
    const winWidth = $win.width();
    const getState = _ => state;
    const init = _ => {
        if(winWidth > 960) {
            $(window).on('scroll resize', function () {
                let scroll = $(this).scrollTop();
                if (scroll > 50 && state.top) {
                    down();
                } else if (scroll < 50 && !state.top) {
                    top();
                }
            });
        } else {
            down();
        }
    };

    const down = _ => {
        $logoTop.showVl(1000);
        $logoCenter.hideVl(1000);
        state.top = false;
        onFirstScroll();
        console.log("down");
        $(document).trigger('scroll:down');
    };
    const top = _ => {
        $logoTop.hideVl(1000);
        $logoCenter.showVl(1000);
        state.top = true;
        onScrollTop();
        console.log($(document).trigger('scroll:top'));
        $(document).trigger('scroll:top');
    };
    function onFirstScroll() {
        console.log(ui.menuBtn);
        $("#menu").hideVl(1000);
        ui.menu.init();
        ui.menuBtn.show();
    }
    function onScrollTop () {
        $("#menu").showVl(1000);
        ui.menuBtn.hide(1000);
    }
    return Object.freeze({getState, init, down, top});
}
export { Scroll }