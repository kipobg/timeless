function Loader (spec) {
    let { root = '#loader-main' } = spec;
    let $el = $(root);
    let state = {
        expanded: true
    };
    const getState = _ => state;
    const init = _ => { return true; };
    const show = _ => {
        $el.showVl(100);
        state.expanded = true;
    };
    const hide = _ => {
        $el.hideVl(400);
        state.expanded = false;
    };
    return Object.freeze({getState, init, show, hide});
}

export { Loader };
