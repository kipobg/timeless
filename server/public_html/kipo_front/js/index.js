
'use strict';
// require('slick-carousel')

import './animate'

import { Loader } from './Loader'
import { Menu } from './Menu'
import { MenuBtn } from './MenuBtn'
import { Scroll } from './Scroll'
import { Fit } from './Slider'




// import './animate';

// require('fullpage.js');
//
// import {Loader} from './Loader';
// import {Menu} from './Menu';
// import {MenuBtn} from './MenuBtn';
// import {Animation} from './Animation';
// import {Scroll, setBackgroundHeight, SetContentPosition} from './helpers/helper';
//
// const animation = new Animation();

let ui = {
    loader: new Loader({root: '#loader-main-bg'}),
    menu: new Menu({root: 'nav.header'}),
    scroll: new Scroll({root: ''}),
    menuBtn: new MenuBtn({root: '.menu-mobile-btn.burger'}),
};

function app(ui) {
    console.log('app running ...');
    ui.menuBtn.init();
    ui.menu.init();
    let selector = $('.about');

    if($('.clients').length > 0) {
        selector = $('.clients');
    }
    new SetContentPosition(selector);
}


function changeHeader(color, backgroundColor, backgroundColorAlpha = 0.8) {
    const logo = $('.logo svg');
    const navHeader = $('.main.nav.inner-header');
    logo.velocity({'fill': color}, {duration: 200});
    navHeader.velocity({backgroundColor: backgroundColor, backgroundColorAlpha:backgroundColorAlpha}, {duration: 200});
}

$(document).ready(function () {
    app(ui);
});




export default ui;