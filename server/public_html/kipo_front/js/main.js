'use strict';
// require('slick-carousel')

import './animate'

import {Loader} from './Loader'
import {Menu} from './Menu'
import {MenuBtn} from './MenuBtn'
import {Scroll} from './Scroll'
import {Fit} from './Slider'


// import './animate';

// require('fullpage.js');
//
// import {Loader} from './Loader';
// import {Menu} from './Menu';
// import {MenuBtn} from './MenuBtn';
// import {Animation} from './Animation';
// import {Scroll, setBackgroundHeight, SetContentPosition} from './helpers/helper';
//
// const animation = new Animation();

let ui = {
    loader: new Loader({root: '#loader-main-bg'}),
    menu: new Menu({root: '#side-menu'}),
    scroll: new Scroll({root: ''}),
    menuBtn: new MenuBtn({root: '.menu-mobile-btn.burger'}),
    sliderImages: new Fit({root: '#slider'})
};


function app(ui) {
    console.log('app running ...');
    ui.menuBtn.init();
    ui.menuBtn.hide();
    ui.menu.init();

    console.log('CHANGE 3');

}

$(window).on('load', function () {
    console.log('window');
    ui.loader.hide();
    ui.scroll.init();


    // var newVideo = document.getElementById('home-video');
    // newVideo.addEventListener('ended', function () {
    //     console.log('ended')
    //     this.currentTime = 0;
    //     this.play();
    //     alert('video ended')
    // }, false);
    // newVideo.play();
});
$(document).ready(function () {
    app(ui);
});


export default ui;