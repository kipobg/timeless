const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: {
        main: path.resolve(__dirname,'main.js'),
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: "main.js"
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: '/node_modules/',
            loader: "babel-loader",
        }]
    },
    plugins: [
        // new UglifyJSPlugin()
    ]
};
