@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#redirect').val('back'); $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
                <a class="btn btn-warning" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_close_btn']}}</a>
                <a class="btn btn-danger" href="/admin/nav-bar" role="button">{{$label['close_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>

            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif
            <h1><i class='fa fa-globe'></i> {{$title}}</h1>
            {!! Form::open($form_data) !!}
            {!! Form::hidden('redirect', '',['id'=>'redirect']) !!}
            {!! Form::hidden('nav-bar-id', $navBarId) !!}
            {!! Form::hidden('pages-serialize') !!}
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div class="form-group">
                            {!! Form::label('name', $label['name']) !!}
                            {!! Form::text('name',  $navBar->name, ['placeholder' => $label['name'], 'class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                {{--<div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{!! Form::label('position', $label['position']) !!}</div>
                        <div class="panel-body">
                            <div class='form-group'>

                                {!! Form::select('position', $positions, $navBar->position, ['class' => 'form-control']) !!}
                            </div>

                        </div>
                    </div>

                </div>--}}

                <div style="margin-top: 20px" class="col-lg-12">

                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><span class="glyphicon glyphicon-list"></span> {{ $label['pages'] }}</div>
                            <div class="panel-body" style=" height:230px; overflow:scroll;">

                                @foreach($pages as $id=>$pageName)
                                    @if(!in_array($id, $navBar->selected_pages))
                                        <div class="checkbox" data-page-id="{{ $id }}">
                                            <label>
                                                <input data-title="{{ $pageName }}" data-id="{{ $id }}" name="pages[{{ $id }}]" type="checkbox"> {{ $pageName }}
                                            </label>
                                        </div>
                                    @else
                                        <div class="checkbox" data-page-id="{{ $id }}" style="display: none">
                                            <label>
                                                <input data-title="{{ $pageName }}" data-id="{{ $id }}" name="pages[{{ $id }}]" type="checkbox"> {{ $pageName }}
                                            </label>
                                        </div>
                                    @endif
                                @endforeach




                            </div>
                            <button type="button" onclick="addToNavBar()" class="btn btn-success" style="margin: 10px;">Добави</button>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><span class="glyphicon glyphicon-list"></span> {{ $label['pages'] }}</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12 dd" id="nestable">
                                        <ol class="dd-list">
                                            @include('kipo_admin.views.nav-bar.render')
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            {!!  Form::close() !!}


        </div>
    </div>
    </div>
    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };

        var htmlTemplate = '<li class="dd-item cf" data-nav-id="%id">' +
                '<input type="hidden" name="nav-page[%index]" value="%id"/> ' +
                '<div class="dd-content cf">' +
                '<div class="list-header">' +
                '<div class="dd-handle pull-left">' +
                '<span class="glyphicon glyphicon-option-vertical"></span>' +
                '</div>' +
                '<div>%t</div>' +
                '</div>' +
                '<div class="list-btn-group cf">' +
                '<div class="list-btn">' +
                '<a class="glyphicon glyphicon-trash" onclick="deletePage(\'%id\')" role="button" title="Изтрий"></a>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</li>';

        $('#nestable')
        var token = $('[name="_token"]').val();
        $(document).ready(function() {
            MOD.checkbox();
        });

        $('.dd').nestable({
            maxDepth: 10,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', updateOutput);

        function updateOutput() {
            var list   =  $('.dd');
            var pageSerialize = JSON.stringify(list.nestable('serialize'));
            $('[name="pages-serialize"]').val(pageSerialize)
        }

        updateOutput();

        function addToNavBar() {
            console.log('ok');
            var container = $('ol:first');

            $( "input:checked" ).each(function() {

                var index = container.find('li').length;
                var page = $(this);
                var pageTitle = page.data('title');
                var pageId = page.data('id');
                console.log(pageId)
                var html = htmlTemplate.replaceAll('%id', pageId);
                html = html.replaceAll('%t', pageTitle);
                html = html.replaceAll('%index', index);

                container.append(html);
                var pageCont = page.closest('.checkbox');
                pageCont.hide();
                page.prop('checked', false);
                pageCont.find('label').removeClass('btn-success');
                updateOutput();
            });

        }

        function deletePage(pageId) {
            var page = $('[data-page-id="' + pageId + '"]').show();
            var navPage = $('[data-nav-id="' + pageId + '"]').remove();
            updateOutput();

        }
    </script>
@stop