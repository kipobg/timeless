@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">


        {{--<nav class="navbar navbar-default navbar-fixed">
            <a href="{{$url}}/create" class="btn btn-success">{{$label['create']}}</a>
        </nav>--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{$title}} <small></small>
                </h1>

            </div>
        </div>


        @if($errors->any() )
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div>{{$errors->first()}}</div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        @endif

        <div class="row">
            <div class="col-lg-12 dd" id="nestable">
                <ol class="dd-list">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    @foreach ($navBar as $n)
                        <li class="dd-item cf" data-id="{{ $n->id }}">
                            <div class="dd-content static cf">
                                <div class="list-header">
                                    <a href="{{$url}}/{{$n->id}}/edit">
                                        <div class="">{{$n->name}}</div>
                                    </a>
                                </div>
                                <div class="list-btn-group cf">
                                    <div class="list-btn">
                                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete('{{$n->id}}','nav-bar');" role="button" title="{{$label['delete']}}"></a>
                                    </div>
                                    <div class="list-btn">
                                        <a class="glyphicon glyphicon-edit"  href="{{$url}}/{{ $n->id }}/edit" role="button" title="{{$label['edit']}}"></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach

                </ol>
            </div>
        </div>

        <!-- /.row -->
    </div>
    <script>
        var token = $('[name="_token"]').val();

    </script>
@stop