@foreach($navBarPages as $page)
    <li class="dd-item cf" data-keyword="{{ $page->keyword }}" data-nav-id="{{ $page->page_id }}">
        <input type="hidden" name="nav-page[{{ $page->page_id }}]" value="{{ $page->page_id }}"/>
        <div class="dd-content cf">
            <div class="list-header">
                <div class="dd-handle pull-left">
                    <span class="glyphicon glyphicon-option-vertical"></span>
                </div>
                <div>{{ $page->title }}</div>
            </div>
            <div class="list-btn-group cf">
                <div class="list-btn">
                    <a class="glyphicon glyphicon-trash" onclick="deletePage('{{ $page->page_id }}')" role="button" title="Изтрий"></a>
                </div>
            </div>
        </div>
        @if(count($page->childrens) > 0)
            <ol class="dd-list">
                @include('kipo_admin.views.nav-bar.render', ['navBarPages' => $page->childrens])
            </ol>
        @endif
    </li>

@endforeach