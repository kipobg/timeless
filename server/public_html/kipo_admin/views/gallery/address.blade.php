@if(!empty($address))
    @foreach($address as $a)
        <li class="dd-item cf" data-id="{{$a->id}}">
            <div class="dd-content cf">
                <div class="list-header" style="width: 95%">
                    <div class="dd-handle pull-left">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    &nbsp;{{ $a->address }}
                </div>
                <div class="list-btn-group cf" style="width: 5%">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete({{$a->id}}, 'settings-address')"></a>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
@endif