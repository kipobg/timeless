@if(!empty($gallery))
    @foreach($gallery as $g)
        <li class="dd-item cf" data-id="{{$g->id}}">
            <div class="dd-content cf">
                <div class="list-header" style="width: 75%">
                    <div class="">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    <img class="img-responsive" src="/images/gallery/{{$g->cover}}" >
                    &nbsp;{{ $g->title }}
                </div>
                <div class="list-btn-group cf" style="width: 25%">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete({{$g->id}}, 'settings-gallery')"></a>
                    </div>
                    <div class="list-btn">
                        <a class="btn btn-link btn-xs star" href="javascript:void(0)"  role="button">
                            <span class="glyphicon {{ $g->status == 2 ? "glyphicon-star" : "glyphicon-star-empty" }}" data-id="{{ $g->id }}" onclick="MOD.recommended(this, 'settings-gallery')" ></span>
                        </a>
                    </div>
                </div>

            </div>
        </li>
    @endforeach
@endif