@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>
            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif

            <h1><i class='fa fa-globe'></i>Video</h1>

            <div class="row">
                {!! Form::open($form_gallery) !!}
                <br>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <div role="tabpanel">
                            <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                                @foreach($languages as $key=>$language)
                                    <li role="presentation" @if($key == 0) class="active" @endif>
                                        <a href="#gallery-{{$language->language_code}}" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content">
                            @foreach($languages as $key=>$language)
                                <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="gallery-{{$language->language_code}}">
                                    <div class="form-group">
                                        {!! Form::label('gallery-title['.$language->language_id.']', $label['title'] . ' (' . $language->language_name . ')') !!}
                                        {!! Form::text('gallery-title['.$language->language_id.']',  '', ['placeholder' => $label['title'] . ' (' . $language->language_name . ')', 'class' => 'form-control']) !!}
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        {!! Form::label('gallery-description['.$language->language_id.']', $label['description'] . ' (' . $language->language_name . ')') !!}--}}
{{--                                        {!! Form::textarea('gallery-description['.$language->language_id.']',  '', ['placeholder' => $label['description']. ' (' . $language->language_name . ')', 'class' => 'form-control']) !!}--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        {!! Form::label('gallery-url['.$language->language_id.']', $label['link'] . ' (' . $language->language_name . ')') !!}--}}
{{--                                        {!! Form::text('gallery-url['.$language->language_id.']',  '', ['placeholder' => $label['link'] . ' (' . $language->language_name . ')', 'class' => 'form-control']) !!}--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        {!! Form::label('btn-text['.$language->language_id.']', $label['btn'] . ' (' . $language->language_name . ')') !!}--}}
{{--                                        {!! Form::text('btn-text['.$language->language_id.']',  '', ['placeholder' => $label['btn'] . ' (' . $language->language_name . ')', 'class' => 'form-control']) !!}--}}

{{--                                    </div>--}}
                                </div>
                            @endforeach
{{--                                {!! Form::label('btn-type['.$language->language_id.']', 'Button') !!}--}}
{{--                                {!! Form::radio('btn-type', 'btn') !!}--}}
{{--                                {!! Form::label('btn-type['.$language->language_id.']', 'Text link') !!}--}}
{{--                                {!! Form::radio('btn-type', 'txt') !!}--}}

                            <div class="form-group">
                                {!! Form::label('cover', $label['cover'], ['class' => 'unbold'])  !!}
                                {!! Form::file('cover',  [ 'class' => 'form-control' , 'accept' => 'image/*'])  !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('video', $label['video'], ['class' => 'unbold'])  !!}
                                {!! Form::file('video',  [ 'class' => 'form-control' , 'accept' => 'video/mp4'])  !!}
                            </div>
                            <a class="btn btn-success pull-right"  onclick="unsaved = false; $('#form_brand').submit();" role="button">Add</a>
                            <br>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{ $label['gallery'] }}</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12 dd" id="nestable" style="width: 100%;">
                                        <ol class="dd-list form_brand">
                                            @include('kipo_admin.views.gallery.gallery')
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var token = $('[name="_token"]').val();
                </script>
                {!! Form::close() !!}
            </div>



    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        var token = $('[name="_token"]').val();

        $('#nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', updateOutput);

        function updateOutput() {
            MOD.updateOutput('settings-gallery');
        }

        $('#address-nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', pleachAddress);

        function pleachAddress() {
            var list   =  $('#address-nestable');
            var pleach = list.nestable('serialize');

            $('#address-nestable').fadeTo("fast","0.2");


            $.ajax({
                url: "/admin/settings-address/pleach",
                type: "PUT",
                data: {pleach:pleach},
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(data) {
                    $('.dd').fadeTo("slow","1");
                }
            });
        }

        $('#partners-nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', pleachAddress);

        function pleachAddress() {
            var list   =  $('#partners-nestable');
            var pleach = list.nestable('serialize');

            $('#partners-nestable').fadeTo("fast","0.2");


            $.ajax({
                url: "/admin/settings/partners/pleach",
                type: "PUT",
                data: {pleach:pleach},
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(data) {
                    $('.dd').fadeTo("slow","1");
                }
            });
        }

        $("#add-address").on("click", function () {
            $.ajax({
                url: "/admin/settings/address",
                type: "POST",
                data: $('[name^="address"]'),
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(html) {
                    $('.dd-list-address').prepend(html);
                    $("input.address").val("");

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 422) {
                        var response = JSON.parse(jqXHR.responseText);
                        console.log(response);

                        for (var i in response) {
                            var index = i.split('.');
                            console.log(index);
                            if (index.length == 1) {
                                $('#' + i).css('border', '1px solid red');
                            } else {
                                console.log('#' + index[0] + "[" + index[1] + "]");
                                $('[name="' + index[0] + '[' + index[1] + ']"]').css('border', '1px solid red');
                            }
                        }
                    }

                }
            });
        })

    </script>

@stop
