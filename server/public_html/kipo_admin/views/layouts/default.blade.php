<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="{{URL::to('/') }}/admin/" >

    <title>@yield('title') | Kipo Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="/kipo_admin/lib/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/kipo_admin/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="/kipo_admin/css/kipo-admin.css">
    <link rel="stylesheet" href="/kipo_admin/lib/cropper.css">
    <link rel="stylesheet" href="/kipo_admin/css/components.css">
    <link rel="stylesheet" href="/kipo_admin/lib/bootstrap-datetimepicker.min.css">
    <script src="/kipo_admin/lib/jquery-1.11.1.min.js"></script>
    <script src="/kipo_admin/lib/tinymce/js/tinymce/tinymce.min.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

            <a class="navbar-brand" href="/admin">Timeless Administration</a>
<!--            <a class="navbar-brand" target="_blank" href="{{URL::to('/') }}">Към сайта</a>-->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="nav-text">
                Здравей, <span>{{$userlogin->getFullName()}}</span>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="users/{{$userlogin['id']}}/edit"><i class=""></i> Профил</a>
                    </li>

                    <li class="divider"></li>
                    <li>
                        <a href="/logout"><i class=""></i> Изход</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul id="left-nav-bar" class="nav navbar-nav side-nav">
                <li class="{{$activeMenu == 'commercials' ? 'active' : ''}}">
                    <a href="commercials"><i class="fa fa-fw fa-edit"></i> Commercials</a>
                </li>
                <li class="{{$activeMenu == 'films' ? 'active' : ''}}">
                    <a href="films"><i class="fa fa-fw fa-edit"></i> Films</a>
                </li>
                <li class="{{$activeMenu == 'tv_projects' ? 'active' : ''}}">
                    <a href="tv-projects"><i class="fa fa-fw fa-edit"></i> TV Projects</a>
                </li>
                <li class="{{$activeMenu == 'photography' ? 'active' : ''}}">
                    <a href="photography"><i class="fa fa-fw fa-edit"></i> Photographies</a>
                </li>
                <li class="{{$activeMenu == 'gallery' ? 'active' : ''}}">
                    <a href="gallery"><i class="fa fa-fw fa-edit"></i> Video</a>
                </li>
                <li class="{{$activeMenu == 'pages' ? 'active' : ''}}">
                    <a href="pages"><i class="fa fa-fw fa-edit"></i> Страници</a>
                </li>
                <li class="{{$activeMenu == 'users' ? 'active' : ''}}">
                    <a href="users"><i class="fa fa-fw fa-edit"></i> Потребители</a>
                </li>
                <li class="{{$activeMenu == 'settings' ? 'active' : ''}}">
                    <a href="settings"><i class="fa fa-fw fa-edit"></i> Настройки</a>
                </li>
                <li class="{{$activeMenu == 'nav_bar' ? 'active' : ''}}">
                    <a href="nav-bar"> Менюта</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">



        @yield('content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!-- jQuery -->

<script type="text/javascript" src="/kipo_admin/lib/moment.js"></script>
<script src="/kipo_admin/lib/bootstrap.min.js"></script>
<script src="/kipo_admin/lib/cropper.js"></script>

<!-- <script type="text/javascript" src="/path/to/bootstrap/js/transition.js"></script>
<script type="text/javascript" src="/path/to/bootstrap/js/collapse.js"></script> -->

<script type="text/javascript" src="/kipo_admin/lib/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap Core JavaScript -->

<!-- App js -->
<script src="/kipo_admin/js/main.js?v=1.6"></script>
<script src="/kipo_admin/js/kipo-admin.js?v=1.7"></script>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker();
    });
    $('#left-nav-bar li.active').parent().css({'display': 'block'});
    $('#left-nav-bar li.active > a').attr('style', 'background-color: #337ab7 !important');
</script>

</body>

</html>