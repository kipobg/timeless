@if(!empty($images))
    @foreach($images as $image)
        <li class="dd-item cf" data-id="{{$image->id}}">
            <div class="dd-content cf">
                <div class="list-header">
                    <div class="dd-handle pull-left">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    <img class="img-responsive" src="/images/photography/gallery/{{$image->cover}}" >
                    {{ $image->title }}
                </div>
                <div class="list-btn-group cf">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete({{$image->id}}, 'photography/gallery')"></a>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
@endif