@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#redirect').val('back'); $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
                <a class="btn btn-warning" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_close_btn']}}</a>
                <a class="btn btn-danger" href="{{ $url }}" role="button">{{$label['close_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>

            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif
            <h1><i class='fa fa-globe'></i> {{$title}}</h1>
            {!! Form::open($form_data) !!}
            {!! Form::hidden('redirect', '',['id'=>'redirect']) !!}
            {!! Form::hidden('photography-id', $elementId) !!}
            <div class="row">
                <div class="col-lg-9">
                    <div role="tabpanel">
                        <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                            @foreach($languages as $key=>$language)
                                <li role="presentation" @if($key == 0) class="active" @endif>
                                    <a href="#{{$language->language_code}}" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="tab-content">
                        @foreach($languages as $key=>$language)
                            <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="{{$language->language_code}}">
                                <div class="form-group">
                                    {!! Form::label('title['.$language->language_id.']', $label['title'] . '(' . $language->language_name . ')') !!}
                                    {!! Form::text('title['.$language->language_id.']',  $photography->title[$language->language_id], ['placeholder' => $label['title'], 'class' => 'form-control']) !!}
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        {{$label['seotool']}}
                                    </div>
                                    <div class="panel-body">
                                        <div class="seo-snippet-area">
                                            <div class="rc">
                                                <h3><a href="" id="preview_title[1]">{{ $photography->title[$language->language_id] }}</a></h3>
                                                <div class="desc-container">
                                                    <div class="preview-link">
                                                        {{ URL::to('/') }}/{{$language->language_code == 'bg' ? '' : 'en/'}}photographys/{!! Form::text('sefurl['.$language->language_id.']',  $photography->sefurl[$language->language_id], ['placeholder' => $label['sefurl'] . '(' . $language->language_name . ')', 'class' => 'inline-form-control ' . $edit, 'id' => 'sefurl['.$language->language_id.']']) !!}
                                                    </div>
                                                    <span class="st" id="preview_description[1]"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('meta_title['.$language->language_id.']', $label['meta_title'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::text('meta_title['.$language->language_id.']',  $photography->meta_title[$language->language_id], ['placeholder' => $label['meta_title'], 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('meta_description['.$language->language_id.']', $label['meta_description'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::textarea('meta_description['.$language->language_id.']',  $photography->meta_description[$language->language_id], ['size' => '30x5','placeholder' => $label['meta_description'], 'class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <br>


                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-lg-6">
                        <div role="tabpanel">
                            <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                                @foreach($languages as $key=>$language)
                                    <li role="presentation" @if($key == 0) class="active" @endif>
                                        <a href="#{{$language->language_code}}-gallery" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="tab-content">
                            @foreach($languages as $key=>$language)
                                <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="{{$language->language_code}}-gallery">
                                    <div class="form-group">
                                        {!! Form::label('gallery-text['.$language->language_id.']', $label['name'] . '(' . $language->language_name . ')') !!}
                                        {!! Form::text('gallery-text['.$language->language_id.']',  null, ['placeholder' => $label['name'], 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('gallery-cover', $label['gallery_cover'], ['class' => 'unbold'])  !!}
                                {!! Form::file('gallery-cover',  [ 'class' => 'form-control' , 'accept' => 'image/*'])  !!}
                            </div>
                            <a class="btn btn-success pull-right"  onclick="unsaved = false; MOD.uploadGallery(this)" role="button">{{ $label['create'] }}</a>
                            <br>
                        </div>
                    </div>



                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{ $label['gallery'] }}</div>
                            <div class="panel-body" style="height: 210px; overflow-y: scroll">
                                <div class="row">
                                    <div class="col-lg-12 dd" id="nestable" style="width: 100%;">
                                        <ol class="dd-list">
                                            @include('kipo_admin.views.photography.gallery')
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>

                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$label['option_panel']}}</div>
                        <div class="panel-body">
                            <div class='form-group'>
                                {!! Form::label('status', $label['status']) !!}
                                {!! Form::select('status',array('0'=>$label['disable'],'1'=>$label['enable'], '2'=>$label['home_page'] ),$photography->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{$label['cover_list']}} - {{ $dimension }} пропорция</div>
                        <div class="panel-body">
                            <div id="covre_preview">
                                {!! Form::label('cover') !!}
                                {!! Form::file('cover',  [ 'class' => 'form-control' , 'accept' => 'image/*']) !!}
                                <div class="divider"></div>
                                @if($photography->cover)
                                    <img class="img-responsive" src="/images/photography/large_{{$photography->cover}}" >
                                @else
                                    <img class="img-responsive" src="/images/no_image.jpg" >
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{$label['cover']}} - {{ $dimension }} пропорция</div>
                        <div class="panel-body">
                            <div id="covre_preview">
                                {!! Form::label('cover_list') !!}
                                {!! Form::file('cover_list',  [ 'class' => 'form-control' , 'accept' => 'image/*']) !!}
                                <div class="divider"></div>
                                @if($photography->cover_list)
                                    <img class="img-responsive" src="/images/photography/{{ $photography->cover_list }}" >
                                @else
                                    <img class="img-responsive" src="/images/no_image.jpg" >
                                @endif
                            </div>
                        </div>
                    </div>

        </div>

        </div>

        {!!  Form::close() !!}
        </div>
        </div>
        </div>
    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        var token = $('[name="_token"]').val();
        var MOD = MOD || {};
        $('.dd').nestable({
            maxDepth: 2,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', updateOutput);

        function updateOutput() {
            MOD.updateOutput('photography/gallery');
        }
    </script>
@stop