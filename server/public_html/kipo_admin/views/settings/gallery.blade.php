@if(!empty($gallery))
    @foreach($gallery as $g)
        <li class="dd-item cf" data-id="{{$g->id}}">
            <div class="dd-content cf">
                <div class="list-header" style="width: 95%">
                    <div class="dd-handle pull-left">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    <img class="img-responsive" src="/images/settings/gallery/{{$g->cover}}" >
                    &nbsp;{{ $g->title }}
                </div>
                <div class="list-btn-group cf" style="width: 5%">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete({{$g->id}}, 'settings-gallery')"></a>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
@endif