@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>
            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif

            <h1><i class='fa fa-globe'></i> {{$title}}</h1>

            <div class="row">
                {!! Form::open($form_data) !!}
                <div class="col-lg-9">

                    <div role="tabpanel">
                        <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                            @foreach($languages as $key=>$language)
                                <li role="presentation" @if($key == 0) class="active" @endif>
                                    <a href="#{{$language->language_code}}" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="tab-content">
                        @foreach($languages as $key=>$language)
                            <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="{{$language->language_code}}">
                                <div class="form-group">
                                    {!! Form::label('home-meta-title['.$language->language_id.']', $label['home_meta_title']) !!}
                                    {!! Form::text('home-meta-title['.$language->language_id.']',  $settings->home_meta_title[$language->language_id], ['placeholder' => $label['home_meta_title'], 'class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('home-meta-description['.$language->language_id.']', $label['home_meta_description']) !!}
                                    {!! Form::text('home-meta-description['.$language->language_id.']',  $settings->home_meta_description[$language->language_id], ['placeholder' => $label['home_meta_description'], 'class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('contacts['.$language->language_id.']', $label['contacts']) !!}
                                    {!! Form::textarea('contacts['.$language->language_id.']',  $settings->contacts[$language->language_id], ['placeholder' => $label['contacts'], 'class' => 'form-control editor']) !!}
                                </div>
                            </div>
                        @endforeach
                        <div class="form-group">
                            {!! Form::label('email', $label['email']) !!}
                            {!! Form::text('email',  $settings->email, ['placeholder' => $label['email'], 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', $label['phone']) !!}
                            {!! Form::text('phone',  $settings->phone, ['placeholder' => $label['phone'], 'class' => 'form-control']) !!}
                        </div>
                        {{--  <div class="form-group">
                            {!! Form::label('coordinates', $label['coordinates']) !!}
                            {!! Form::text('coordinates',  $settings->coordinates, ['placeholder' => $label['coordinates'], 'class' => 'form-control']) !!}

                        </div>
                        --}}
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$label['files']}}</div>
                        <div class="panel-body">
                            <div class='form-group'>
                                {!! Form::label('video', $label['video']) !!}
                                {!! Form::file('video', ['class' => 'form-control', 'accept' => "video/*"]) !!}
                                <input type="hidden" name="old_video" value="{{ $settings->video }}"/>
                                <input type="hidden" name="video_rename" value="{{ $label['terms'] }}"/>
                                @if(isset($settings->video) && !empty($settings->video))
                                    <br>
                                    <a href="/video/{{ $settings->video }}" target="_blank">{{ $label['video'] . ' ' . $label['link'] }}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}


            </div>



    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        var token = $('[name="_token"]').val();

        $('#nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', updateOutput);

        function updateOutput() {
            MOD.updateOutput('settings-gallery');
        }

        $('#address-nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', pleachAddress);

        function pleachAddress() {
            var list   =  $('#address-nestable');
            var pleach = list.nestable('serialize');

            $('#address-nestable').fadeTo("fast","0.2");


            $.ajax({
                url: "/admin/settings-address/pleach",
                type: "PUT",
                data: {pleach:pleach},
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(data) {
                    $('.dd').fadeTo("slow","1");
                }
            });
        }

        $('#partners-nestable').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', pleachAddress);

        function pleachAddress() {
            var list   =  $('#partners-nestable');
            var pleach = list.nestable('serialize');

            $('#partners-nestable').fadeTo("fast","0.2");


            $.ajax({
                url: "/admin/settings/partners/pleach",
                type: "PUT",
                data: {pleach:pleach},
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(data) {
                    $('.dd').fadeTo("slow","1");
                }
            });
        }

        $("#add-address").on("click", function () {
            $.ajax({
                url: "/admin/settings/address",
                type: "POST",
                data: $('[name^="address"]'),
                headers: {
                    'X-CSRF-TOKEN': token
                },
                success: function(html) {
                    $('.dd-list-address').prepend(html);
                    $("input.address").val("");

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 422) {
                        var response = JSON.parse(jqXHR.responseText);
                        console.log(response);

                        for (var i in response) {
                            var index = i.split('.');
                            console.log(index);
                            if (index.length == 1) {
                                $('#' + i).css('border', '1px solid red');
                            } else {
                                console.log('#' + index[0] + "[" + index[1] + "]");
                                $('[name="' + index[0] + '[' + index[1] + ']"]').css('border', '1px solid red');
                            }
                        }
                    }

                }
            });
        })

    </script>

@stop
