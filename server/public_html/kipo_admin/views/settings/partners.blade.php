@if(!empty($partners))
    @foreach($partners as $p)
        <li class="dd-item cf" data-id="{{$p->id}}">
            <div class="dd-content cf">
                <div class="list-header" style="width: 95%">
                    <div class="dd-handle pull-left">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    <img class="img-responsive" src="/images/partners/{{$p->cover}}" >
                </div>
                <div class="list-btn-group cf" style="width: 5%">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete({{$p->id}}, 'settings/partners')"></a>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
@endif