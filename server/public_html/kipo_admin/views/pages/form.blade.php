@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#redirect').val('back'); $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
                <a class="btn btn-warning" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_close_btn']}}</a>
                <a class="btn btn-danger" href="/admin/pages" role="button">{{$label['close_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>

            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif
            <h1><i class='fa fa-globe'></i> {{$title}}</h1>
            {!! Form::open($form_data) !!}
            {!! Form::hidden('redirect', '',['id'=>'redirect']) !!}
            {!! Form::hidden('page-id', $pageId) !!}
            <div class="row">
                <div class="col-lg-9">
                    <div role="tabpanel">
                        <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                            @foreach($languages as $key=>$language)
                                <li role="presentation" @if($key == 0) class="active" @endif>
                                    <a href="#{{$language->language_code}}" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="tab-content">
                        @foreach($languages as $key=>$language)
                            <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="{{$language->language_code}}">
                                <div class="form-group">
                                    {!! Form::label('title['.$language->language_id.']', $label['title'] . '(' . $language->language_name . ')') !!}
                                    {!! Form::text('title['.$language->language_id.']',  $page->title[$language->language_id], ['placeholder' => $label['title'], 'class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description['.$language->language_id.']', $label['description'] . '(' . $language->language_name . ')') !!}
                                    {!! Form::textarea('description['.$language->language_id.']',  $page->description[$language->language_id], ['placeholder' => $label['description'], 'class' => 'form-control editor']) !!}
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        {{$label['seotool']}}
                                    </div>
                                    <div class="panel-body">
                                        <div class="seo-snippet-area">
                                            <div class="rc">
                                                <h3><a href="" id="preview_title[1]">{{ $page->title[$language->language_id] }}</a></h3>
                                                <div class="desc-container">
                                                    <div class="preview-link">
                                                        {{ URL::to('/') }}/{{$language->language_code == 'bg' ? '' : 'en/'}}{!! Form::text('sefurl['.$language->language_id.']',  $page->sefurl[$language->language_id], ['placeholder' => $label['sefurl'] . '(' . $language->language_name . ')', 'class' => 'inline-form-control ' . $edit, 'id' => 'sefurl['.$language->language_id.']']) !!}
                                                    </div>
                                                    <span class="st" id="preview_description[1]"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('meta_title['.$language->language_id.']', $label['meta_title'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::text('meta_title['.$language->language_id.']',  $page->meta_title[$language->language_id], ['placeholder' => $label['meta_title'], 'class' => 'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('meta_description['.$language->language_id.']', $label['meta_description'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::textarea('meta_description['.$language->language_id.']',  $page->meta_description[$language->language_id], ['size' => '30x5','placeholder' => $label['meta_description'], 'class' => 'form-control']) !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$label['option_panel']}}</div>
                        <div class="panel-body">
                            <div class='form-group'>
                                {!! Form::label('status', $label['status']) !!}
                                {!! Form::select('status',array('0'=>$label['disable'],'1'=>$label['enable'] ),$page->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{$label['cover']}}</div>
                        <div class="panel-body">
                            <div id="covre_preview">
                                {!! Form::label('cover', $label['cover']) !!}
                                {!! Form::file('cover',  [ 'class' => 'form-control' , 'accept' => 'image/*']) !!}
                                <div class="divider"></div>
                                @if($page->cover)
                                    <img class="img-responsive" src="/images/pages/{{$page->cover}}" >
                                @else
                                    <img class="img-responsive" src="/images/no_image.jpg" >
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-list"></span> {{ $label['modules'] }}</div>
                        <div class="panel-body" style=" height:230px; overflow:scroll;">

                            @foreach($modules as $id=>$module)
                                @if(in_array($id, $page->selectedModules))
                                    <div class="checkbox">
                                        <label class="btn-success" >
                                            <input checked="checked" name="modules[{{ $id }}]" type="checkbox"> {{ $module['name'] }}
                                        </label>
                                    </div>
                                @else

                                    <div class="checkbox">
                                        <label>
                                            <input name="modules[{{ $id }}]" type="checkbox"> {{ $module['name'] }}
                                        </label>
                                    </div>

                                @endif
                            @endforeach
                        </div>
                    </div>
        </div>
        </div>
            <br>
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list"></span> {{ $label['video'] }}</div>
                <div class="panel-body" style=" height:100%; overflow:scroll;">
                    @foreach($videos as $id=>$video)
                        @if(in_array($id, $page->selectedVideos))
                            <div class="checkbox">
                                <label class="btn-success" >
                                    <div class="list-header" style="width: 75%">
                                    <input checked="checked" name="videos[{{ $id }}]" type="checkbox"> {{ $video->title }}
                                    </div>
                                    <video style="width: 15%" controls>
                                        <source src="{{URL::asset("/images/gallery/$video->video")}}" type="video/mp4">
                                    </video>
                                </label>
                            </div>
                        @else
                            <div class="checkbox">
                                <label>
                                    <div class="list-header" style="width: 75%">
                                        <input name="videos[{{ $id }}]" type="checkbox">
                                        {{ $video->title }}
                                    </div>
                                    <video style="width: 15%" controls>
                                        <source src="{{URL::asset("/images/gallery/$video->video")}}" type="video/mp4">
                                    </video>
                                </label>
                            </div>

                        @endif
                    @endforeach
                </div>
            </div>


        {!!  Form::close() !!}
        </div>
        </div>
        </div>
        <script>
        var token = $('[name="_token"]').val();
        $(document).ready(function() {
        MOD.checkbox();
        });
        </script>
        @stop