@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">

        <nav class="navbar navbar-default navbar-fixed">
            <a href="/admin/pages/create" class="btn btn-success">Добави</a>
        </nav>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{$title}} <small></small>
                </h1>

            </div>
        </div>

        @if($errors->any() )
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div>{{$errors->first()}}</div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        @endif

        <div class="row">
            <div class="col-lg-12 dd" id="nestable">
                {!! Form::hidden('_token', csrf_token()) !!}
                @include('kipo_admin.views.pages.page', array('pages' => $pages, 'subPage' => $subPage))
            </div>
        </div>
        <!-- /.row -->
        <script src="/kipo_admin/lib/jquery.nestable.js"></script>
        <script>
            var token = $('[name="_token"]').val();


            $('.dd').nestable({
                maxDepth: 1,
                expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
                collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
            }).on('change', updateOutput);

            function updateOutput() {
                MOD.updateOutput('pages');
            }

        </script>


    </div>

@stop
