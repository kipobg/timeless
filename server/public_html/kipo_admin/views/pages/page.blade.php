
<ol class="dd-list">
    @foreach ($pages as $key=>$page)

        <li class="dd-item cf" data-id="{{ $page['page_id'] }}">
            <div class="dd-content cf">
                <div class="list-header">
                    <div class="dd-handle pull-left">
                        <span class="glyphicon glyphicon-option-vertical"></span>
                    </div>
                    <a href="/admin/{{$activeMenu}}/{{$page['page_id']}}/edit">
                        @if($page['cover'])
                            <img src="/images/{{$folder . '/' . $page['cover']}}"/>
                        @else
                            <img src="/images/no_image.jpg"/>
                        @endif
                        <div class="">{{$page['title']}}</div>
                    </a>
                </div>
                <div class="list-btn-group cf">
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-trash"   onclick="MOD.delete('{{$page['page_id']}}','pages');" role="button" title="Изтрий"></a>

                    </div>
                    <div class="list-btn">
                        <a class="glyphicon glyphicon-edit"  href="pages/{{ $page['page_id'] }}/edit" role="button" title="Редактирай"></a>
                    </div>
                    <div class="list-btn">
                        <div data-id="{{$page['page_id']}}" onclick="MOD.visible(this, 'pages')" class="btn-switch @if($page['status']) active @endif" title="Публикувай">
                            <div class="btn-toggle"></div>
                        </div>
                    </div>
                </div>
            </div>
            @if($page['children'])
                @include('kipo_admin.views.pages.page', array('pages' => $page['children']))
            @endif
            @if(array_key_exists($page['page_id'], $subPage))
                @include('kipo_admin.views.pages.page', array('pages' => $subPage[$page->page_id]))
            @endif
        </li>

    @endforeach

</ol>
       