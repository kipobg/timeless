@foreach($pages as $page)
    <div>
        {{$page->page_name}}
    </div>
    <div class="checkbox">
        <label class="btn-success" >
            <input checked="checked" name="pages[{{$page->id}}][1]" type="checkbox"> {{$label['1']}}
        </label>
    </div>
    <div class="checkbox">
        <label class="btn-success" >
            <input checked="checked" name="pages[{{$page->id}}][2]" type="checkbox"> {{$label['2']}}
        </label>
    </div>
    <div class="checkbox">
        <label class="btn-success" >
            <input checked="checked" name="pages[{{$page->id}}][3]" type="checkbox"> {{$label['3']}}
        </label>
    </div>
@endforeach