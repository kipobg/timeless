@extends('kipo_admin.views.layouts.default')
 
@section('title') {{$title}} @stop
 
@section('content')
 
<div id="page-wrapper">
     <nav class="navbar navbar-default navbar-fixed">

         <div class="pull-right">
             <a class="btn btn-success" href="#" onclick="unsaved = false; $('#redirect').val('back'); $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
             <a class="btn btn-warning" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_close_btn']}}</a>
             <a class="btn btn-danger" href="/admin/users" role="button">{{$label['close_btn']}}</a>
         </div>
     </nav>
    <div class="container-fluid">
         <!-- Page Heading -->
        <div class="row fixed-nav-height">

        </div>
        @if ($errors->any() && !$errors->has('msg'))
            <div class='bg-danger alert'>
                
                @foreach ($errors->all() as $error)
                {{ $error }} <br>
                @endforeach
            </div>    
        @endif
        @if($errors->has('msg'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div>{{$errors->first('msg')}}</div>
                    </div>
                </div>
            </div>
        @endif
 
        <h1><i class='fa fa-globe'></i> {{$title}}</h1>
        {!! Form::open($form_data) !!}
        {!! Form::hidden('redirect', '', array('id' => 'redirect')) !!}
        {!! Form::hidden('user-id', $userId) !!}
        <div class="row">
            <div class="col-lg-9">



                <div class="tab-content">
                    <div class="form-group">
                        {!! Form::label('full-name', $label['fullname']) !!}
                        {!! Form::text('full-name',  $user->fullname, ['placeholder' => $label['fullname'], 'class' => 'form-control']) !!}
                    </div>
                     <div class="form-group">
                        {!! Form::label('email', $label['email']) !!}
                        {!! Form::text('email',  $user->email, ['placeholder' => $label['email'], 'class' => 'form-control']) !!}
                    </div>
                    <div class='form-group'>
                        {!! Form::label('password',$label['password']) !!}
                        {!! Form::password('password', ['placeholder' => $label['password'], 'class' => 'form-control']) !!}
                    </div>
                    <div class='form-group'>
                        {!! Form::label('password_confirmation',$label['password_confirm']) !!}
                        {!! Form::password('password_confirmation', ['placeholder' => $label['password_confirm'], 'class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$label['option_panel']}}</div>
                    <div class="panel-body">
                        <div class='form-group'>
                            {{Form::label('receive-email', 'Получаване на известия')}}
                            {{ Form::select('receive-email',array('0'=>$label['no'],'1'=>$label['yes'] ),$user->receive_email, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
       {!! Form::close() !!}
    </div>
</div>

@stop