<div class="row">
    <div class="col-lg-12 dd" id="nestable">
        <ol class="dd-list">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            @foreach ($userGroups as $group)
                <li>
                    <h3 class="page-header">{{$group['name']}}</h3>
                    <ol class="dd-list">
                        @foreach ($users as $user)
                            @if($user->group_id == $group['id'])
                                <li class="dd-item cf" data-id="{{$user->id}}">
                                    <div class="dd-content static cf">
                                        <div class="list-header" style="width: 35%;">


                                            <div class="">{{ $user->getFullName() }}</div>

                                        </div>
                                        <div class="list-btn-group cf" style="width: 65%;">
                                            <div class="list-btn">
                                                <div data-id="{{ $user->id }}" onclick="MOD.disable(this, 'users')" class="btn-switch @if($user->status == 1 || $user->status == 0) active @endif" title="Публикувай">
                                                    <div class="btn-toggle"></div>
                                                </div>
                                            </div>
                                            <div class="list-btn">
                                                <a class="glyphicon glyphicon-edit"  href="{{$url}}/{{ $user->id }}/edit" role="button" title="Редактирай"></a>
                                            </div>

                                            <div class="list-btn" title="от дата">
                                                {{$user->created_at->format('d.M.Y') }}
                                            </div>
                                            <div class="list-btn fixed-width" >{{ $user->email }}</div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                </li>
            @endforeach

        </ol>
    </div>
</div>
<script>
    var token = $('[name="_token"]').val();
</script>