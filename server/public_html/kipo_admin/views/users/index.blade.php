@extends('kipo_admin.views.layouts.default')
 
@section('title') {{$title}} @stop
 
@section('content')
    <div class="container-fluid">


        <nav class="navbar navbar-default navbar-fixed">
            <a href="users/create" class="btn btn-success">Добави</a>
        </nav>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">

                </h1>

            </div>
        </div>


        @if($errors->any() )
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div>{{$errors->first()}}</div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        @endif

        <div class="row">
            <div class="col-lg-12 dd" id="nestable">
                <h2>Администратори</h2>
                <ol class="dd-list">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    @foreach ($admins as $user)


                        <li class="dd-item cf" data-id="{{$user->id}}">
                            <div class="dd-content static cf">
                                <div class="list-header" style="width: 35%;">


                                    <div class=""><a href="users/{{ $user->id }}/edit">{{ $user->getFullName() }}</a></div>

                                </div>
                                <div class="list-btn-group cf" style="width: 65%;">
                                    <div class="list-btn">
                                        <form action="{{ url('/admin/users/' . $user->id)}}" method="DELETE">
                                            <span class="glyphicon glyphicon-trash"></span>
                                            <input type="button" class="btn btn-danger btn-xs btn-icon" onclick="if (window.confirm('Сигурен ли си, че искаш да изтриеш  записа?')) { deleteUser('{{$user->id}}'); }else{ return false; }" />
                                        </form>
                                    </div>
                                    <div class="list-btn">
                                        <a class="glyphicon glyphicon-edit"  href="users/{{ $user->id }}/edit" role="button" title="Редактирай"></a>
                                    </div>

                                    <div class="list-btn" title="от дата">
                                        {{$user->created_at->format('d.M.Y') }}
                                    </div>
                                    <div class="list-btn fixed-width" >{{ $user->email }}</div>
                                </div>
                            </div>
                        </li>

                    @endforeach

                </ol>
                @if(count($users) > 0)
                    <h2>Потребители</h2>
                    <ol class="dd-list">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                        @foreach ($users as $user)


                            <li class="dd-item cf" data-id="{{$user->id}}">
                                <div class="dd-content static cf">
                                    <div class="list-header" style="width: 35%;">


                                        <div class=""><a href="users/{{ $user->id }}/edit">{{ $user->getFullName() }}</a></div>

                                    </div>
                                    <div class="list-btn-group cf" style="width: 65%;">
                                        <div class="list-btn">
                                            <form action="{{ url('/admin/users/' . $user->id)}}" method="DELETE">
                                                <span class="glyphicon glyphicon-trash"></span>
                                                <input type="button" class="btn btn-danger btn-xs btn-icon" onclick="if (window.confirm('Сигурен ли си, че искаш да изтриеш  записа?')) { deleteUser('{{$user->id}}'); }else{ return false; }" />
                                            </form>
                                        </div>
                                        <div class="list-btn">
                                            <a class="glyphicon glyphicon-edit"  href="users/{{ $user->id }}/edit" role="button" title="Редактирай"></a>
                                        </div>

                                        <div class="list-btn" title="от дата">
                                            {{$user->created_at->format('d.M.Y') }}
                                        </div>
                                        <div class="list-btn fixed-width" >{{ $user->email }}</div>
                                    </div>
                                </div>
                            </li>

                        @endforeach

                    </ol>
                @endif
            </div>
        </div>
        <!-- /.row -->




        <div class="row">
            <div class="col-lg-12">
                <nav class="center-text">

                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <nav class="center-text">

                </nav>
            </div>
        </div>


    </div>
    <script>
        function deleteUser(id) {
            var token = $('[name="_token"]').val();
            console.log(token);
            $.ajax({
                url: '/admin/users/' + id,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'DELETE',
                success: function(id) {

                    $('[data-id="' + id + '"]').remove();
                }
            });
        }
    </script>
@stop
 
