@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div id="page-wrapper">
        <nav class="navbar navbar-default navbar-fixed">

            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="unsaved = false; $('#redirect').val('back'); $('#data_form').submit(); return false;" role="button">{{$label['save_btn']}}</a>
                <a class="btn btn-warning" href="#" onclick="unsaved = false; $('#data_form').submit(); return false;" role="button">{{$label['save_close_btn']}}</a>
                <a class="btn btn-danger" href="{{ $url }}" role="button">{{$label['close_btn']}}</a>
            </div>
        </nav>
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row fixed-nav-height">

            </div>

            @if ($errors->any() && !$errors->has('msg'))
                <div class='bg-danger alert'>

                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            @if($errors->has('msg'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <div>{{$errors->first('msg')}}</div>
                        </div>
                    </div>
                </div>
            @endif
            <h1><i class='fa fa-globe'></i> {{$title}}</h1>
            {!! Form::open($form_data) !!}
            {!! Form::hidden('redirect', '',['id'=>'redirect']) !!}
            {!! Form::hidden('commercial-id', $elementId) !!}
            <div class="row">
                <div class="col-lg-9">
                    <div role="tabpanel">
                        <ul id="language-tabs" class="nav nav-tabs" role="tablist">
                            @foreach($languages as $key=>$language)
                                <li role="presentation" @if($key == 0) class="active" @endif>
                                    <a href="#{{$language->language_code}}" aria-controls="home" role="tab" data-toggle="tab">{{$language->language_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="tab-content">
                        @foreach($languages as $key=>$language)
                            <div role="tabpanel" class="tab-pane @if($key == 0) active @endif " id="{{$language->language_code}}">
                                <div class="form-group">
                                    {!! Form::label('title['.$language->language_id.']', $label['title'] . '(' . $language->language_name . ')') !!}
                                    {!! Form::text('title['.$language->language_id.']',  $commercial->title[$language->language_id], ['placeholder' => $label['title'], 'class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description['.$language->language_id.']', $label['description'] . '(' . $language->language_name . ')') !!}
                                    {!! Form::textarea('description['.$language->language_id.']',  $commercial->description[$language->language_id], ['placeholder' => $label['description'], 'class' => 'form-control editor']) !!}
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        {{$label['seotool']}}
                                    </div>
                                    <div class="panel-body">
                                        <div class="seo-snippet-area">
                                            <div class="rc">
                                                <h3><a href="" id="preview_title[1]">{{ $commercial->title[$language->language_id] }}</a></h3>
                                                <div class="desc-container">
                                                    <div class="preview-link">
                                                        {{ URL::to('/') }}/{{$language->language_code == 'bg' ? '' : 'en/'}}commercials/{!! Form::text('sefurl['.$language->language_id.']',  $commercial->sefurl[$language->language_id], ['placeholder' => $label['sefurl'] . '(' . $language->language_name . ')', 'class' => 'inline-form-control ' . $edit, 'id' => 'sefurl['.$language->language_id.']']) !!}
                                                    </div>
                                                    <span class="st" id="preview_description[1]"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('meta_title['.$language->language_id.']', $label['meta_title'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::text('meta_title['.$language->language_id.']',  $commercial->meta_title[$language->language_id], ['placeholder' => $label['meta_title'], 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('meta_description['.$language->language_id.']', $label['meta_description'] . '(' . $language->language_name . ')') !!}
                                            {!! Form::textarea('meta_description['.$language->language_id.']',  $commercial->meta_description[$language->language_id], ['size' => '30x5','placeholder' => $label['meta_description'], 'class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <br>


                                </div>
                            </div>
                        @endforeach
                        <div class="form-group">
                            {!! Form::label('video-code', $label['video_code']) !!}
                            {!! Form::text('video-code',  $commercial->video_code, ['placeholder' => $label['video_code'], 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <br>
                    <div class="video-container form-group">
                        <div class="video-container form-group">
                            @if($commercial->type == $videoType['youtube'])
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $commercial->video_code }}" height="446" style="width: 100%; height: 345px; {{ !$commercial->video_code ? " ;display: none;" : "" }}" frameborder="0" allowfullscreen></iframe>
                            @elseif($commercial->type == $videoType['vimeo'])
                                <iframe src="https://player.vimeo.com/video/{{ $commercial->video_code }}" style="width: 100%; height: 345px; {{ !$commercial->video_code ? " ;display: none;" : "" }}" height="446" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            @else
                                <iframe src="" height="446" allowfullscreen style="display: none; width: 100%" height="446"></iframe>
                            @endif
                        </div>
                    </div>


                    </div>

                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$label['option_panel']}}</div>
                        <div class="panel-body">
                            <div class='form-group'>
                                {!! Form::label('status', $label['status']) !!}
                                {!! Form::select('status',array('0'=>$label['disable'],'1'=>$label['enable'], '2'=>$label['home_page'] ),$commercial->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> {{$label['cover']}} - {{ $dimension }} пропорция</div>
                        <div class="panel-body">
                            <div id="covre_preview">
                                {!! Form::label('cover') !!}
                                {!! Form::file('cover',  [ 'class' => 'form-control' , 'accept' => 'image/*']) !!}
                                <div class="divider"></div>
                                @if($commercial->cover)
                                    <img class="img-responsive" src="/images/commercials/large_{{$commercial->cover}}" >
                                @else
                                    <img class="img-responsive" src="/images/no_image.jpg" >
                                @endif
                            </div>
                        </div>
                    </div>

        </div>

        </div>

        {!!  Form::close() !!}
        </div>
        </div>
        </div>
    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        var token = $('[name="_token"]').val();
        var MOD = MOD || {};

        String.prototype.isValidLink = function() {
            var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return regexp.test(this);
        };

        String.prototype.iframeLink = function() {
            if(this.isValidLink()) {
                if(this.indexOf("youtu") > 0) {
                    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                    var match = this.match(regExp);
                    return (match && match[7].length == 11) ? "https://www.youtube.com/embed/" + match[7] : false;
                } else if(this.indexOf("vimeo")) {
                    return "https://player.vimeo.com/video/" + this.replace("https://vimeo.com/", "");
                }

                return false;
            }
        }

        $("#video-code").on("focusout", function() {
            var videoLink = $(this).val();

            if(videoLink.isValidLink()) {
                var iframeLink = videoLink.iframeLink();
                console.log(iframeLink);
                if(iframeLink) {
                    var iframe = $(".video-container").find("iframe");
                    iframe.attr("src", iframeLink);

                    iframe.show();

                }
            }
        });
        
        </script>
@stop