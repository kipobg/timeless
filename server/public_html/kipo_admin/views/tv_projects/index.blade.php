@extends('kipo_admin.views.layouts.default')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">


        <nav class="navbar navbar-default navbar-fixed">
            <a href="{{$url}}/create" class="btn btn-success">{{$label['create']}}</a>
        </nav>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    {{$title}} <small></small>
                </h1>

            </div>
        </div>


        @if($errors->any() )
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div>{{$errors->first()}}</div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        @endif

        <div class="row">
            <div class="col-lg-12 dd" id="nestable">
                <ol class="dd-list">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    @foreach ($tvProjects as $tvProject)
                        <li class="dd-item cf" data-id="{{ $tvProject->id }}">
                            <div class="dd-content cf">
                                <div class="list-header">
                                    <div class="dd-handle pull-left">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </div>
                                    <a href="{{$url}}/{{$tvProject->id}}/edit">
                                        @if($tvProject->cover)
                                            <img src="/images/{{$folder}}/large_{{$tvProject->cover}}" />
                                        @endif
                                        <div class="">{{$tvProject->title}}</div>
                                    </a>
                                </div>
                                <div class="list-btn-group cf">
                                    <div class="list-btn">
                                        <a class="glyphicon glyphicon-trash" onclick="MOD.delete('{{$tvProject->id}}','tv-projects');" role="button" title="{{$label['delete']}}"></a>
                                    </div>
                                    <div class="list-btn">
                                        <a class="glyphicon glyphicon-edit"  href="{{$url}}/{{ $tvProject->id }}/edit" role="button" title="{{$label['edit']}}"></a>
                                    </div>
                                    <div class="list-btn">
                                        <div data-id="{{$tvProject->id}}" onclick="MOD.visible(this, 'tv-projects')" class="btn-switch @if($tvProject->status) active @endif" title="{{$label['publish']}}">
                                            <div class="btn-toggle"></div>
                                        </div>
                                    </div>
                                    <div class="list-btn">
                                        <a class="btn btn-link btn-xs star" href="javascript:void(0)"  role="button"  title="{{ $label['home_page'] }}">
                                            <span class="glyphicon {{ $tvProject->status == 2 ? "glyphicon-star" : "glyphicon-star-empty" }}" data-id="{{ $tvProject->id }}" onclick="MOD.recommended(this, 'tv-projects')" ></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach

                </ol>
            </div>
        </div>

        <!-- /.row -->
    </div>
    <script src="/kipo_admin/lib/jquery.nestable.js"></script>

    <script>
        var token = $('[name="_token"]').val();


        $('.dd').nestable({
            maxDepth: 1,
            expandBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-right expand-btn"></span></button>',
            collapseBtnHTML: '<button data-action="expand"><span class="glyphicon glyphicon-triangle-down expand-btn"></span></button>'
        }).on('change', updateOutput);

        function updateOutput() {
            MOD.updateOutput('tv-projects');
        }

        $('')
    </script>
@stop