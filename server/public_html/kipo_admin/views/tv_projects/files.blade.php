@foreach($files as $file)
    <li class="dd-item cf" data-id="{{$file->id}}">
        <div class="dd-content cf">
            <div class="list-header" style="width: 95%">
                <div class="dd-handle pull-left">
                    <span class="glyphicon glyphicon-option-vertical"></span>
                </div>
                <a class="img-responsive" target="_blank" @if($url) href="{{ $url }}/{{$file->file_name}}" @else href="/images/{{ $folder }}/{{$file->file_name}}" @endif>
                    {{ $file->title }}
                </a>
            </div>
            <div class="list-btn-group cf" style="width: 5%">
                <div class="list-btn">
                    <a class="glyphicon glyphicon-trash" onclick="if(confirm('Файлът ще бъде изтрит!')) { MOD.delete({{$file->id}}, '{{ $deleteUrl }}') }"></a>
                </div>
            </div>
        </div>
    </li>
@endforeach