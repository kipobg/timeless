<?php
namespace App;
use \Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use App\KipoModel;
use \DateTime;
use App\Http\Controllers\admin\ImageController;
use \File;

class BaseModel extends Model {
    protected $url;
    protected $folder;
    protected $label;
    protected $imgPath;
    protected $messages;
    protected $languages;
    protected $userlogin;
    protected $indexConstants;
    public $timestamps = false;
    protected $table;
    protected $primaryKey;
    protected $constantsName;
    protected $foreignKey;
    protected $activeMenu;
    protected $isAdmin = false;
    protected $viewData = [];
    protected $hasTableDescription = true;
    protected $hasSeoUrl = true;
    protected $emptyLang;
    protected $baseData = [];
    protected $baseDataDescription;
    protected $className;
    protected $resizeWidth = [];
    protected $videoType = [
        'youtube' => 1,
        'vimeo' => 2,
    ];
    protected $langId;
    protected $hasOrderNumber = true;

    public function __construct($url = '', $folder = '', $isAdmin = false, $languages = [], $isGet = true) {
        $this->isAdmin = $isAdmin;
        $this->languages = [];

        if($this->isAdmin) {
            $this->url = $url;
            $this->folder = $folder;
            if($this->isAdmin) {
                $this->label = trans('admin/constants.' . $this->constantsName);
            } else {
                $this->label = trans('constants.' . $this->constantsName);
            }
            $this->userlogin = Auth::user();
            $this->imgPath = Config::get('app.image_path');
            $this->indexConstants = trans('admin/constants.index');

            $this->languages = $languages;
            if($isGet) {
                foreach ($this->languages as $language) {
                    $this->emptyLang[$language->language_id]  = '';
                }
                $this->viewData['languages'] = $this->languages;
            }

            $this->activeMenu = mb_strlen($this->activeMenu) > 0 ? $this->activeMenu : $this->table;
            $this->label = array_merge($this->indexConstants, $this->label);
            $this->viewData['userlogin'] = $this->userlogin;
            $this->viewData['activeMenu'] = $this->activeMenu;
            $this->viewData['indexLabel'] = $this->indexConstants;
            $this->viewData['label'] = $this->label;
            $this->viewData['url'] = $this->url;
            $this->viewData['folder'] = $this->folder;
            $this->viewData['videoType'] = $this->videoType;
        }


        $this->messages = trans('admin/messages.' . $this->constantsName);
        $this->langId = KipoModel::getCurrentLangId();
    }

    public function getLabel() {
        return $this->label;
    }

    public function getMessages() {
        return $this->messages;
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        if($callByController) {
            KipoModel::insertCurrentUrl();
        }

        $title = '';

        if($this->isAdmin) {
            $title = $this->label['heading_title_index'];
        }

        $this->viewData['title'] = $title;

        return $this->viewData;
    }

    public function createElement() {
        $title = $this->label['heading_title_form'];
        $emptyLang = [];
        $sections = [];

        foreach ($this->languages as $lang) {
            $sections[$lang->language_id] = [];
            $emptyLang[$lang->language_id] = '';
        }

        $this->viewData['title'] = $title;
        $this->viewData['form_data'] = ['files' => true, 'role' => 'form', 'url' => $this->url, 'id' => 'data_form'];
        $this->viewData['edit'] = '';
        $this->viewData['activeMenu'] = $this->activeMenu;
        $this->viewData['elementId'] = '0';


        return $this->viewData;
    }

    public function editElement($id) {
        $title = $this->label['heading_title_form'];

        $this->viewData['title'] = $title;
        $this->viewData['form_data'] = ['role' => 'form', 'url' => $this->url . '/' . $id, 'method' => 'put', 'id' => 'data_form', 'files' => true];
        $this->viewData['edit'] = ' edit';
        $this->viewData['elementId'] = $id;

        return $this->viewData;
    }

    public function updateElement($data, $id) {
        $element = $this->className::find($id);

        if($this->hasTableDescription) {
            $whereClause = [
                'template' => $this->table,
                'query' => $id
            ];

            DB::table($this->table . '_descriptions')->where($this->foreignKey, $id)->delete();
        }

        if($this->hasSeoUrl) {
            DB::table('seourl')->where($whereClause)->delete();
        }

        $this->addElementData($data, $element);

        return [ 'msg' => $this->messages['success-update-messages'] ];
    }

    public function insertElement($data) {
        if($this->hasOrderNumber) {
            $this->className::increment('order_number');
        }

        $element = new $this->className();

        $elementId = $this->addElementData($data, $element);

        return ['id' => $elementId, 'msg' => $this->messages['success-insert-messages']];
    }

    protected function addElementData($data, $element) {
        $elementDescription = [];
        $seoUrlData = [];

        if(isset($data['cover'])) {
            $cover = $data['cover'];

            $coverName = ImageController::changeImageName($cover);
            $elementImgPath = $this->imgPath . $this->folder;

            File::delete($elementImgPath . '/' . $element->cover);
            KipoModel::saveImage($cover, $elementImgPath, $coverName, $this->resizeWidth, $this->viewData['dimension']);

            $element->cover = $coverName;
        }

        foreach ($this->baseData as $property=>$value) {
            $k = $value['key'] == null ? '' : $value['key'];

            $element->$property = isset($value['default']) ? KipoModel::checkIsSet($data, $value['index'], $k, $value['default']) : $value['value'];
        }

        $element->save();
        $elementId = $element->id;

        foreach ($this->languages as $key=>$lang) {
            $languageId = $lang->language_id;

            if($this->hasTableDescription) {
                $elementDescription[$key][$this->foreignKey] = $elementId;
                $elementDescription[$key]['language_id'] = $languageId;
                foreach ($this->baseDataDescription as $field=>$value) {
                    $k = $value['key'];

                    $elementDescription[$key][$field] = isset($value['default']) ? KipoModel::checkIsSet($data, $value['index'], $$k, $value['default']) : $value['value'];
                }
            }

            if($this->hasSeoUrl) {
                $seoUrlData[] = array(
                    'language_id' => $languageId,
                    'query' => $elementId,
                    'template' => $this->table,
                    'keyword' => KipoModel::checkIsSet($data, 'sefurl', $lang->language_id, '')
                );
            }
        }

        if($this->hasSeoUrl) {
            DB::table('seourl')->insert($seoUrlData);
        }

        if($this->hasTableDescription) {
            DB::table($this->table . '_descriptions')->insert($elementDescription);
        }

        return $elementId;
    }

    public function deleteElement($id) {
        $elementQuery = $this->className::where($this->primaryKey, $id);
        $element = $elementQuery->first();

        if($this->hasSeoUrl) {
            $whereClause = [
                'template' => $this->table,
                'query' => $id
            ];

            DB::table('seourl')->where($whereClause)->delete();
        }

        if(property_exists($element, 'cover')) {
            $cover = KipoModel::getImgPath() . $this->folder . '/' . $element->cover;
            KipoModel::deleteFiles([$cover]);
        }


        $elementQuery->delete();

        if($this->hasTableDescription) {
            DB::table($this->table . '_descriptions')->where($this->foreignKey, $id)->delete();
        }
    }

    protected function convertElementsToArray($elements, $index, $key, $property) {
        $result = [];

        $result[0] = $this->label[$index];

        foreach ($elements as $element) {
            $result[$element->$key] = $element->$property;
        }

        return $result;
    }

    public function getElements($langId, $hasTableDescription = true, $hasSeoUrl = true, string $whereRaw = '', array $whereData = [], $select = ['title', 'keyword', 'cover'], $paginate = true, $limit = 15, $orderBy = 'order_number') {
        $elements = $this->className;
        $static = true;
        $whereClauseData = [];
        $whereClause = 'status > 0';

        if($hasTableDescription) {
            $whereClauseData[] = $langId;
            $whereClause .= ' AND ' . $this->table . '_descriptions.language_id = ?';
        }

        if($hasSeoUrl) {
            $whereClauseData[] = $this->table;
            $whereClause .= ' AND seourl.language_id = ' . $this->table . '_descriptions.language_id  AND template = ?';
        }

        if($hasTableDescription) {
            if($static) {
                $elements = $elements::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey);
                $static = false;
            } else {
                $elements = $elements->join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey);
            }
        }

        if($hasSeoUrl) {
            if($static) {
                $elements = $elements::join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query');
                $static = false;
            } else {
                $elements = $elements->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query');
            }
        }

        if($static) {
            $elements = $elements::whereRaw($whereClause . $whereRaw, array_merge($whereClauseData, $whereData));

        } else {
            $elements = $elements->whereRaw($whereClause . $whereRaw, array_merge($whereClauseData, $whereData));
        }

        $elements = $elements
            ->orderBy($orderBy)
            ->select(DB::raw(implode(",", $select)));

        if($paginate) {
            $elements = $elements->paginate($limit);
        } else {
            if($limit == -1) {
                $elements = $elements
                    ->get();
            } else {
                $elements = $elements
                    ->take($limit)
                    ->get();
            }
        }

        return $elements;
    }



    public function getElementByKeyword($keyword, $langId, $whereRaw = '', $whereData = []) {
        $element = $this->className::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('status > 0 AND seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '_descriptions.language_id = ? AND template = ? AND keyword = ?' . $whereRaw, array_merge([$langId, $this->table, $keyword], $whereData))
            ->first();

        return $element;
    }

    public function getPageByModuleClass($class) {
        $where = "%" . $class . "%";
        $module = DB::table("pages_modules")->where("serialize", "Like", $where)->first();

        if(count($module) > 0) {
            return $module->page_id;
        }

        return 0;
    }
}