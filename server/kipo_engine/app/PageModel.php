<?php
namespace App;
use App\GalleryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use \File;
use \Image;
use App\Http\Controllers\admin\ImageController;
use App\KipoModel;

class PageModel extends Model {
    public $timestamps = false;
    protected $table = 'pages';
    protected $primaryKey = 'id';
    private $foreignKey = 'page_id';
    private $url;
    private $folder;
    private $label;
    private $messages;
    private $modules;
    private $videos;
    private $languages;
    private $userlogin;
    private $activeMenu = 'pages';
    private $imgPath;
    private $isAdmin = false;

    public function __construct($url = '', $folder = '', $isAdmin = false) {
        if($isAdmin) {
            $this->url = $url;
            $this->folder = $folder;
            $this->label = trans('admin/constants.pages');
            $this->messages = trans('admin/messages.pages');
            $this->languages = KipoModel::getLanguages();
            $this->userlogin = Auth::user();
            $this->imgPath = Config::get('app.image_path');
            $this->modules = [
                0 => [
                    'name' => $this->label['commercials'],
                    'model' => CommercialModel::class
                ],
                1 => [
                    'name' => $this->label['tv-projects'],
                    'model' => TVProjectModel::class
                ],
                2 => [
                    'name' => $this->label['photography'],
                    'model' => PhotographyModel::class
                ],
                3 => [
                    'name' => $this->label['films'],
                    'model' => FilmModel::class
                ]
            ];
            $this->videos = (new GalleryModel)->getAllGallery();
            $this->isAdmin = true;
        }
    }

    public function index($langId, $callByController = false) {
        $title = '';

        if($callByController) {
            KipoModel::insertCurrentUrl();
        }

        if($this->isAdmin) {
            $title = $this->label['heading_title_index'];
        }

        $whereClause = [
            'pages_descriptions.language_id' => $langId,
            'pages.parent_id' => '0'
        ];
        $pages =  [];
        $subPage = [];
        $parentPages = PageModel
            ::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->orderBy('order_number')
            ->where($whereClause)
            ->select(
                array(
                    $this->table . '.' . $this->primaryKey,
                    'title',
                    'status',
                    'cover'
                ))
            ->get();

        foreach($parentPages as $parentPage) {
            $whereClause = [
                'pages_descriptions.language_id' => $langId,
                'pages.parent_id' => $parentPage->id
            ];

            $children = PageModel::orderBy('order_number','ASC')
                ->join('pages_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
                ->where($whereClause)
                ->select(
                    array(
                        $this->table . '.' . $this->primaryKey . ' as page_id',
                        'title',
                        'status',
                        'cover'
                    ))
                ->get();
            if($parentPage->parent_id > 0) {
                $subPage[$parentPage->id] = $children;
            } else {
                $pages[] =  array(
                    'page_id' => $parentPage->id,
                    'status' => $parentPage->status,
                    'title' => $parentPage->title,
                    'children' => $children,
                    'cover' => $parentPage->cover,
                );
            }
        }



        return array(
            'userlogin' => $this->userlogin,
            'title' => $title,
            'pages' => $pages,
            'subPage' => $subPage,
            'activeMenu' => $this->activeMenu,
            'folder' => $this->folder
        );
    }

    public function createPage() {
        $title = $this->label['heading_title_form'];
        $emptyLang = [];
        $emptyArr = [];
        foreach ($this->languages as $lang) {
            $emptyLang[$lang->language_id] = '';
            $emptyArr[$lang->language_id] = array();
        }

        $page = (object) array(
            'title' => $emptyLang,
            'sub_title' => $emptyLang,
            'description' => $emptyLang,
            'sefurl' => $emptyLang,
            'meta_title'=> $emptyLang,
            'meta_description' => $emptyLang,
            'status' => 0,
            'cover' => '',
            'selectedModules' => KipoModel::getSelectedData('modules')
        );


        $viewData = array(
            'userlogin' => $this->userlogin,
            'title' => $title,
            'languages' => $this->languages,
            'page' => $page,
            'form_data' => ['role' => 'form', 'url' => $this->url, 'id' => 'data_form', 'files' => true],
            'label' => $this->label,
            'edit' => '',
            'activeMenu' => $this->activeMenu,
            'pageId' => '0',
            'modules' => $this->modules,
            'folder' => $this->folder
        );

        return $viewData;
    }

    public function updatePage($data, $id) {
        $pages = PageModel::find($id);
        $whereClause = array($this->foreignKey => $id);

        DB::table($this->table . '_descriptions')->where($whereClause)->delete();

        $whereClause = array(
            'query' => $id,
            'template' => 'pages'
        );


        DB::table('seourl')->where($whereClause)->delete();
        DB::table('pages_modules')->where('page_id', $id)->delete();
        DB::table('pages_videos')->where('page_id', $id)->delete();

        $this->addDataToObj($data, $pages);
        $this->addpagesData($data, $id);

        return [ 'msg' => $this->messages['success-update-messages'] ];
    }

    public function editPage($id) {
        $title = $this->label['heading_title_form'];

        $page = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->join('pages_modules', $this->table . '.' . $this->primaryKey, '=', 'pages_modules.page_id', 'left')
            ->join('pages_videos', $this->table . '.' . $this->primaryKey, '=', 'pages_videos.page_id', 'left')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND template = ?' . ' AND ' . $this->table . '.' .$this->primaryKey . ' = ?', array($this->table, $id))
            ->select(
                array(
                    'serialize',
                    'video_serialize',
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'seourl.language_id',
                    'keyword',
                    'cover'
                )
            )
            ->get();

        $selectedModules = [];

        if(!old() && count($page) > 0 && $page[0]->serialize) {
            $modulesUnserialize = unserialize($page[0]->serialize);

            foreach($modulesUnserialize as $key=>$module) {
                $selectedModules[] = $key;
            }
        } else {
            $selectedModules = KipoModel::getSelectedData('modules');
        }

        $selectedVideos = [];

        if(!old() && count($page) > 0 && $page[0]->video_serialize) {
            $videosUnserialize = unserialize($page[0]->video_serialize);

            foreach($videosUnserialize as $key=>$video) {
                $selectedVideos[] = $key;
            }
        } else {
            $selectedVideos = KipoModel::getSelectedData('videos');
        }

        $data = (object) array(
            'title' => array(),
            'sub_title' => array(),
            'description' => array(),
            'meta_title' => array(),
            'meta_description' => array(),
            'status',
            'keyword',
            'cover',
            'selectedModules' => $selectedModules,
            'selectedVideos' => $selectedVideos
        );

        if(count($page) > 0) {
            $data->status = $page[0]->status;
            $data->cover = $page[0]->cover;
        }

        foreach ($page as $p) {
            $data->title[$p->language_id] = $p->title;
            $data->sub_title[$p->language_id] = $p->sub_title;
            $data->description[$p->language_id] = $p->description;
            $data->meta_title[$p->language_id] = $p->meta_title;
            $data->meta_description[$p->language_id] = $p->meta_description;
            $data->sefurl[$p->language_id] = $p->keyword;
        }


        $viewData =  array(
            'userlogin' => $this->userlogin,
            'title' => $title,
            'languages' => $this->languages,
            'page' => $data,
            'form_data' => ['role' => 'form', 'url' => $this->url . '/' . $id, 'method' => 'put', 'id' => 'data_form', 'files' => true],
            'label' => $this->label,
            'activeMenu' => $this->activeMenu,
            'edit' => ' edit',
            'pageId' => $id,
            'folder' => $this->folder,
            'modules' => $this->modules,
            'videos' => $this->videos
        );
        return $viewData;
    }

    public function insertPage($data) {
        PageModel::increment('order_number');
        $pages = new PageModel();
        $pages->order_number = 1;
        $pageId = $this->addDataToObj($data, $pages);

        $this->addPagesData($data, $pageId);

        return ['id' => $pageId, 'msg' => $this->messages['success-insert-messages']];
    }

    public function getNavBarPages($langId, $navBar) {
        $result = [];
        $where = [
            $this->table . '_descriptions.language_id' => $langId,
            'pages_to_nav_bar.parent_id' => 0,
            'nav_bar_id' => $navBar,

        ];

        $result = $this->getChildPages($where);

        return $result;
    }

    private function getChildPages($where, $keyword = '') {
        $pages = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('pages_to_nav_bar', $this->table . '_descriptions.' . $this->foreignKey, '=', 'pages_to_nav_bar.page_id')
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND query = ' . $this->table . '_descriptions.page_id AND template = "pages"')
            ->where($where)
            ->orderBy('pages_to_nav_bar.order_number')
            ->get(array(
                'pages_to_nav_bar.parent_id',
                'keyword',
                'pages_to_nav_bar.page_id',
                'title',
                'nav_bar_id',

            ));


        foreach ($pages as $page) {
            if($page->parent_id == 0) {
                $keyword = '';
            }

            $page->base_keyword = $page->keyword;
            $keyword .= '/' . $page->keyword;
            $page->keyword = $keyword;

            $where['pages_to_nav_bar.parent_id'] = $page->page_id;

            $page->childrens = $this->getChildPages($where, $keyword);

            $keyword = str_replace('/' . $page->base_keyword, '', $keyword);

        }

        return $pages;

    }

    public function getPagesWhereKeywordIn($data, $langId, $position = 1) {
        $whereInStr = '';
        $whereInArr = [];
        foreach ($data as $el) {
            if(!empty($el)) {
                $whereInStr .= '"' . $el . '", ';
                $whereInArr[] = $el;
            }
        }

        $whereInStr = rtrim($whereInStr, ', ');


        $pages = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join(
                'seourl',
                $this->table . '.' . $this->primaryKey, '=', 'seourl.query'
            )
            ->whereRaw('status > 0 AND seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '_descriptions.language_id = ? AND query = pages.id AND template = "pages"', [$langId])
            ->whereIn('keyword', $whereInArr)
            ->orderByRaw(DB::raw("FIELD(keyword, $whereInStr)"))
            ->groupBy('page_id')
            ->get(array($this->table . '_descriptions.page_id', 'title', 'keyword'));

        return ['pages' => $pages, 'params' => $whereInArr];

    }

    public function getPages($langId, $whereRaw, $whereData, $select, $isArray = false) {
        $pages = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
//            ->join('pages_modules', $this->table . '.' . $this->primaryKey, '=', 'pages_modules.page_id', 'left')
            ->join(
                'seourl',
                $this->table . '.' . $this->primaryKey, '=', 'seourl.query'
            )->whereRaw($whereRaw, $whereData)
            ->orderBy('order_number')
            ->orderBy('page_id')
            ->get($select);

        if($isArray) {
            $pagesData = [];

            foreach ($pages as $page) {
                $pagesData[$page->page_id] = $page->title;
            }

            return $pagesData;
        }

        return $pages;
    }

    private function addDataToObj($data, &$pages) {
        $pages->status = $data['status'];


        if(isset($data['cover'])) {
            $cover = $data['cover'];

            $pageName = ImageController::changeImageName($cover);
            $pageImgPath = $this->imgPath . $this->folder;

            File::delete($pageImgPath . '/' . $pages->cover);
            KipoModel::saveImage($cover, $pageImgPath, $pageName);

            $pages->cover = $pageName;
        }

        $pages->save();
        $pageId = $pages->id;

        return $pageId;
    }

    private function addPagesData($data, $pageId) {
        $pagesDescription = [];
        $pageSections = [];
        $pageSubSections = [];
        $sefurl = [];
        $moduleData = [];
        $modules = KipoModel::checkIsSet($data, 'modules', '', []);

        foreach($modules as $id=>$true) {
            if(isset($this->modules[$id])) {
                $model = $this->modules[$id]['model'];
                $moduleData[$id] = $model::pageModule();
            }
        }

        $videoData = [];
        $videos = KipoModel::checkIsSet($data, 'videos', '', []);

        foreach($videos as $id=>$true) {
            if(isset($this->videos[$id])) {
                $videoData[$id] = $this->videos[$id];
            }
        }

        foreach ($this->languages as $language) {
            $langId = $language->language_id;

            $pagesDescription[] = array(
                $this->foreignKey => $pageId,
                'language_id' => $langId,
                'title' => KipoModel::checkIsSet($data, 'title', $langId, ''),
                'description' => KipoModel::checkIsSet($data, 'description', $langId, ''),
                'meta_title' => KipoModel::checkIsSet($data, 'meta_title', $langId, ''),
                'meta_description' => KipoModel::checkIsSet($data, 'meta_description', $langId, '')
            );

            $sefurl[] = array(
                'query' => $pageId,
                'language_id' => $langId,
                'template' => $this->table,
                'keyword' => KipoModel::checkIsSet($data, 'sefurl', $langId, '')
            );
        }

        if(count($moduleData) > 0) {
            DB::table('pages_modules')->insert([
                'page_id' => $pageId,
                'serialize' => serialize($moduleData)
            ]);
        }

        if(count($videoData) > 0) {
            DB::table('pages_videos')->insert([
                'page_id' => $pageId,
                'video_serialize' => serialize($videoData)
            ]);
        }

        DB::table($this->table . '_descriptions')->insert($pagesDescription);

        DB::table('seourl')->insert($sefurl);
    }

    public function deletePage($id) {
        $whereClause = array(
            'query' => $id,
            'template' => 'pages'
        );

        DB::table('seourl')->where($whereClause)->delete();
        DB::table($this->table . '_descriptions')->where($this->foreignKey, $id)->delete();
        $page = PageModel::find($id);
        File::delete($this->imgPath . $this->folder . '/' . $page->cover);
        PageModel::destroy($id);
    }



    public function getTopBarPages(&$head) {
        $langId = KipoModel::getCurrentLangId();
        $head['topBarPages'] = PageModel
            ::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('parent_id = 0 AND status =1 AND seourl.language_id = ' . $this->table . '_descriptions.language_id AND template = ? AND seourl.language_id = ?', array($this->table, $langId))
            ->orderBy('order_number')
            ->select(array(
                'title',
                'keyword'
            ))
            ->get();
    }

    public function getLeftSidebar($pageId) {
        $langId = KipoModel::getCurrentLangId();
        $pages = PageModel
            ::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('parent_id = ? AND status =1 AND seourl.language_id = ' . $this->table . '_descriptions.language_id AND template = ? AND seourl.language_id = ?', array($pageId->query, $this->table, $langId))
            ->orderBy('order_number')
            ->select(array(
                'title',
                'keyword'
            ))
            ->get();
        return $pages;
    }

    public function getPagesByParentId($parentId) {
        $languageId = KipoModel::getCurrentLangId();

        $pages = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join(
                'seourl',
                $this->table . '.' . $this->primaryKey, '=', 'seourl.query'
            )->whereRaw(
                'parent_id = ?
                 AND status = 1 AND seourl.language_id = ' . $this->table . '_descriptions.language_id
                 AND ' . $this->table . '_descriptions.language_id = ?
                 AND template = ?',
                array($parentId, $languageId, $this->table))
            ->orderBy('order_number', 'ASC')
            ->whereNotIn($this->table . '.' . $this->primaryKey, [$parentId])
            ->get(array(
                'title',
                'keyword',
                'pages.id'

            ));

        return $pages;
    }

    public function getPageByKeyword($keyword) {
        $languageId = KipoModel::getCurrentLangId();
        $page = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('pages_modules', $this->table . '.' . $this->primaryKey, '=', 'pages_modules.page_id', 'left')
            ->join('pages_videos', $this->table . '.' . $this->primaryKey, '=', 'pages_videos.page_id', 'left')
            ->join(
                'seourl',
                $this->table . '.' . $this->primaryKey, '=', 'seourl.query'
            )->whereRaw(
                'status = 1 AND seourl.language_id = ' . $this->table . '_descriptions.language_id
                 AND ' . $this->table . '_descriptions.language_id = ?
                 AND template = ?
                 AND keyword = ? ',
                array($languageId, $this->table, $keyword))
            ->select(array(
                'serialize',
                'video_serialize',
                $this->table . '.' . $this->primaryKey,
                'title',
                'description',
                'meta_title',
                'meta_description',
                'keyword',
                'cover',
                'parent_id'
            ))
            ->first();

        return $page;
    }

    public function getPageById($id) {
        $languageId = KipoModel::getCurrentLangId();
        $page = PageModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('pages_modules', $this->table . '.' . $this->primaryKey, '=', 'pages_modules.page_id', 'left')
            ->join('pages_videos', $this->table . '.' . $this->primaryKey, '=', 'pages_videos.page_id', 'left')
            ->join(
                'seourl',
                $this->table . '.' . $this->primaryKey, '=', 'seourl.query'
            )->whereRaw(
                'status = 1 AND seourl.language_id = ' . $this->table . '_descriptions.language_id
                 AND ' . $this->table . '_descriptions.language_id = ?
                 AND template = ?
                 AND ' . $this->table . '.' . $this->primaryKey . ' = ? ',
                array($languageId, $this->table, $id))
            ->select(array(
                'serialize',
                'video_serialize',
                $this->table . '.' . $this->primaryKey,
                'title',
                'keyword',
                'pages.id',
                'description',
                'meta_title',
                'meta_description',
                'cover',
                'class'
            ))
            ->first();

        return $page;
    }
}