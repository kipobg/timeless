<?php
namespace App;
use App\Http\Controllers\admin\ImageController;
use App\KipoModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use \File;
use \Image;

class SettingModel extends Model {
    public $timestamps = false;
    protected $table = 'settings';
    protected $primaryKey = 'id';
    private $foreignKey = 'setting_id';
    private $folder;
    private $videoFolder;
    private $label;
    private $messages;
    private $languages;
    private $userlogin;
    private $activeMenu = 'settings';
    private $imgPath;
    private $pdfPath;
    private $pdfFolder = 'pdf';
    private $galleryFolder;
    private $partnerFolder = "partners";

    public function __construct() {
        $this->folder = 'settings';
        $this->videoFolder = $this->folder . '/video';
        $this->label = trans('admin/constants.settings');
        $this->messages = trans('admin/messages.settings');
        $this->languages = KipoModel::getLanguages();
        $this->userlogin = Auth::user();
        $this->imgPath = Config::get('app.image_path');
        $this->pdfPath = public_path($this->pdfFolder);
        $this->galleryFolder = $this->folder . '/gallery';
    }

    public function getSettings($langId) {
        $settingQuery = SettingModel::where('key', 'settings')->first();

        if(count($settingQuery) > 0) {
            $settingObj = unserialize($settingQuery->value);

            return [
                'title' => isset($settingObj->home_meta_title[$langId]) ? $settingObj->home_meta_title[$langId] : '',
                'metaDescription' => isset($settingObj->home_meta_description[$langId]) ? $settingObj->home_meta_description[$langId] : '',
                'contacts' => isset($settingObj->contacts[$langId]) ? $settingObj->contacts[$langId] : '',
                'header_info' => isset($settingObj->header_info[$langId]) ? $settingObj->header_info[$langId] : '',
                'footer_info' => isset($settingObj->footer_info[$langId]) ? $settingObj->footer_info[$langId] : '',
                'email' => isset($settingObj->email) ? $settingObj->email : '',
                'phone' => isset($settingObj->phone) ? $settingObj->phone : '',
                'video' => isset($settingObj->video) ? $settingObj->video : '',


            ];
        }
    }

    public function createSettings() {
        $title = $this->label['heading_title_form'];
        $textRemove = $this->label['text_remove'];
        $emptyLang = [];
        $setting = [];
        $settingQuery = SettingModel::where('key', 'settings')->first();

        foreach ($this->languages as $lang) {
            $emptyLang[$lang->language_id] = '';
        }

        $setting = (object) array(
            'home_meta_title' => $emptyLang,
            'home_meta_description' => $emptyLang,
            'footer_info' => $emptyLang,
            'header_info' => $emptyLang,
            'footer_address' => $emptyLang,
            'contacts' => $emptyLang,
            'partners' => $emptyLang,
            'phone' => '',
            'email' => '',
            'weekends' => [],
            'video' => '',
            'file_rule' => '',
            'file_procedure' => '',
            'coordinates' => '',
        );

        if(count($settingQuery) > 0) {
            $settingObj = unserialize($settingQuery->value);

            $setting->home_meta_title = isset($settingObj->home_meta_title) ? $settingObj->home_meta_title : $emptyLang;
            $setting->home_meta_description = isset($settingObj->home_meta_description) ? $settingObj->home_meta_description : $emptyLang;
            $setting->footer_info = isset($settingObj->footer_info) ? $settingObj->footer_info : $setting->footer_info;
            $setting->header_info = isset($settingObj->header_info) ? $settingObj->header_info : $setting->header_info;
            $setting->footer_address = isset($settingObj->footer_address) ? $settingObj->footer_address : $setting->footer_address;

            if(property_exists($settingObj, 'weekends')) {
                $setting->weekends = $settingObj->weekends;
            }

            if(property_exists($settingObj, 'email')) {
                $setting->email = $settingObj->email;
            }

            if(property_exists($settingObj, 'phone')) {
                $setting->phone = $settingObj->phone;
            }

            if(property_exists($settingObj, 'partners')) {
                $setting->partners = $settingObj->partners;
            }

            if(property_exists($settingObj, 'contacts')) {
                $setting->contacts = $settingObj->contacts;
            }

            if(property_exists($settingObj, 'video')) {
                $setting->video = $settingObj->video;
            }

            if(property_exists($settingObj, 'file_rule')) {
                $setting->file_rule = $settingObj->file_rule;
            }

            if(property_exists($settingObj, 'file_procedure')) {
                $setting->file_procedure = $settingObj->file_procedure;
            }

            if(property_exists($settingObj, 'coordinates')) {
                $setting->coordinates = $settingObj->coordinates;
            }
        }

        $viewData = array(
//            'gallery' => $this->getAllGallery(),
            'textRemove' => $textRemove,
            'userlogin' => $this->userlogin,
            'title' => $title,
            'url' => '/admin/users',
            'languages' => $this->languages,
            'settings' => $setting,
            'form_gallery' => ['role' => 'form', 'files' => true, 'url' => '/admin/settings-gallery', 'id' => 'form_brand'],
            'form_partners' => ['role' => 'form', 'files' => true, 'url' => '/admin/settings-partners', 'id' => 'form_partners'],
            'form_data' => ['role' => 'form', 'url' => '/admin/settings', 'id' => 'data_form', 'files' => true],
            'label' => $this->label,
            'edit' => '',
            'activeMenu' => $this->activeMenu,
            'settingId' => '0',
            'folder' => $this->videoFolder,
            'imgPath' => KipoModel::getImgPath(),
            'pdfFolder' => $this->pdfFolder,
        );

        return $viewData;
    }

    public function insertGallery($data) {
        DB::table('settings_gallery')->increment('order_number');

        if(isset($data['cover'])) {
            $cover = $data['cover'];

            $imgName = ImageController::changeImageName($cover);

            KipoModel::saveImage($cover, $this->imgPath . $this->galleryFolder, $imgName);

        } else {
            abort('404');
        }

        $galleryId = DB::table('settings_gallery')->insertGetId(array(
            'cover' => $imgName,
            'url' => KipoModel::checkIsSet($data, 'url', '', ''),
            'title' => KipoModel::checkIsSet($data, 'title', '', '')
        ));

        return $galleryId;
    }

    public function getGallery($galleryId) {
        return DB::table('settings_gallery')->where('id', $galleryId)->get();

    }

    public function getAllGallery() {
        return DB::table('settings_gallery')->orderBy('order_number')->get();

    }

    public function deleteGallery($galleryId) {
        $gallery = DB::table('settings_gallery')->where('id', $galleryId)->first();

        File::delete($this->imgPath . $this->galleryFolder . '/' . $gallery->cover);

        DB::table('settings_gallery')->where('id', $galleryId)->delete();
    }

    public function insertSetting($data) {
        $settings = new \stdClass();
        foreach ($this->languages as $language) {
            $languageId = $language->language_id;

            $settings->home_meta_title[$languageId] = KipoModel::checkIsSet($data, 'home-meta-title', $languageId, '');
            $settings->home_meta_description[$languageId] = KipoModel::checkIsSet($data, 'home-meta-description', $languageId, '');
            $settings->header_info[$languageId] = KipoModel::checkIsSet($data, 'header-info', $languageId, '');
            $settings->footer_info[$languageId] = KipoModel::checkIsSet($data, 'footer-info', $languageId, '');
            $settings->footer_address[$languageId] = KipoModel::checkIsSet($data, 'footer-address', $languageId, '');
            $settings->partners[$languageId] = KipoModel::checkIsSet($data, 'partners', $languageId, '');
            $settings->contacts[$languageId] = KipoModel::checkIsSet($data, 'contacts', $languageId, '');
        }

        $settings->phone = KipoModel::checkIsSet($data, 'phone', '', '');
        $settings->email = KipoModel::checkIsSet($data, 'email', '', '');


        $videoFile = '';
        $videoPath = Config::get("app.video_path");

        if(isset($data['video'])) {
            $videoFile = $data['video'];
            if(isset($data['video']) && !empty($data['old_video'])) {
                if(file_exists($videoPath . '/' . $data['old_video'])) {
                    unlink($videoPath . '/' . $data['old_video']);
                }
            }
        }
        $fileRule = '';
        if(isset($data['file_rule'])) {
            $fileRule = $data['file_rule'];
            if(isset($data['file_rule']) && !empty($data['old_file_rule'])) {
                if(file_exists($this->pdfPath . '/' . $data['old_file_rule'])) {
                    unlink($this->pdfPath . '/' . $data['old_file_rule']);
                }
            }
        }
        $fileProcedure = '';
        if(isset($data['file_procedure'])) {
            $fileProcedure = $data['file_procedure'];
            if(isset($data['file_procedure']) && !empty($data['old_file_procedure'])) {
                if(file_exists($this->pdfPath . '/' . $data['old_file_procedure'])) {
                    unlink($this->pdfPath . '/' . $data['old_file_procedure']);
                }
            }
        }

        $videoName = KipoModel::checkIsSet($data, 'old_video', '', '');
        $date = date('d_m_Y');
        if(is_object($videoFile)) {
            $videoName = ImageController::changeImageName($videoFile);
            $videoFile->move($videoPath, $videoName);
        }
        $fileRuleName = KipoModel::checkIsSet($data, 'old_file_rule', '', '');
        if(is_object($fileRule)) {
            $fileRuleName = ImageController::changeImageName($fileRule);
            $fileRule->move($this->pdfPath, $fileRuleName);
        }
        $fileProcedureName = KipoModel::checkIsSet($data, 'old_file_procedure', '', '');
        if(is_object($fileProcedure)) {
            $fileProcedureName = $fileProcedure;
            $fileProcedure->move($this->pdfPath, $fileProcedureName);
        }
        $settings->video = $videoName;
        $settings->file_rule = $fileRuleName;
        $settings->file_procedure = $fileProcedureName;

        $serialize = serialize($settings);
        SettingModel::where('key','settings')->delete();
        SettingModel::insert(array(
            'key' => 'settings',
            'value' => $serialize,
            'serialized' => 1
        ));

        return ['msg' => $this->messages['success-insert-messages']];
    }

    public function insertAddress(array $data) {
        $addressData = [];

        DB::table("address")->increment("order_number");
        $addressId = DB::table("address")->insertGetId([
            "order_number" => 1
        ]);

        foreach ($this->languages as $language) {
            $addressData[] = [
                "address_id" => $addressId,
                "address" => KipoModel::checkIsSet($data, "address", $language->language_id, ''),
                "language_id" => $language->language_id,
            ];
        }

        DB::table("address_descriptions")->insert($addressData);

        return $addressId;

    }

    public function getAddress(int $addressId = -1) {
        $where = ["language_id" => KipoModel::getCurrentLangId()];

        if($addressId > -1) {
            $where["address_id"] = $addressId;
        }

        $address = DB::table("address")
            ->join("address_descriptions", "address.id", "=", "address_descriptions.address_id")
            ->where($where)
            ->orderBy("order_number")
            ->get();

        return $address;
    }

    public function deleteAddress($id) {
        DB::table("address")->delete($id);
        DB::table("address_descriptions")->where("address_id", $id)->delete();
    }

    public function insertPartner(array $data) {
        if(isset($data['cover'])) {
            $cover = $data['cover'];

            $imgName = ImageController::changeImageName($cover);

            KipoModel::saveImage($cover, $this->imgPath . $this->partnerFolder, $imgName);

        } else {
            abort('404');
        }

        DB::table("partners")->increment("order_number");

        $partnerId = DB::table("partners")->insertGetId([
            "order_number" => 1,
            "cover" => $imgName
        ]);


        return $partnerId ;

    }

    public function getPartners(int $partnerId = -1) {
        $where = [];

        if($partnerId > -1) {
            $where["id"] = $partnerId;
        }

        $partners = DB::table("partners")
            ->where($where)
            ->orderBy("order_number")
            ->get();

        return $partners;
    }

    public function deletePartner($id) {
        $partnerQuery = DB::table("partners");
        $partner = $partnerQuery->first();

        if($partner) {
            File::delete($this->imgPath . $this->partnerFolder . "/" . $partner->cover);
        }

        $partnerQuery->delete($id);
    }
}