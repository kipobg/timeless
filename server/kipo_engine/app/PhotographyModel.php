<?php
namespace App;

use App\BaseModel as BM;
use \File;
use App\Http\Controllers\admin\ImageController;
use \DB;

class PhotographyModel extends BM {
    protected  $table = 'photography';
    protected  $primaryKey = 'id';
    protected  $constantsName = 'photography';
    protected  $foreignKey = 'photography_id';
    protected $className = self::class;
    protected $categoryModel;

    public function __construct($url = '', $folder = '', $isAdmin = false, $languages = [], $isGet = false) {
        parent::__construct($url, $folder, $isAdmin, $languages, $isGet);

        if(!$isGet && $isAdmin) {
            $this->baseData['status'] = ['default' => '0', 'index' => 'status', 'key' => ''];

            $this->baseDataDescription['title'] = ['default' => '', 'index' => 'title', 'key' => 'languageId'];
            $this->baseDataDescription['meta_title'] = ['default' => '', 'index' => 'meta_title', 'key' => 'languageId'];
            $this->baseDataDescription['meta_description'] = ['default' => '', 'index' => 'meta_description', 'key' => 'languageId'];
        }

        $this->resizeWidth = ["large" => 400];
        $this->viewData['dimension'] = '1/1';
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        $this->viewData['photography'] = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->whereRaw($this->table . '_descriptions.language_id = ? ', array($langId))
            ->orderBy('order_number')
            ->select(
                array(
                    'title',
                    'meta_title',
                    'meta_description',
                    'status',
                    'language_id',
                    'cover',
                    'id',
                )
            )
            ->get();

        return parent::index($langId, $callByController, $paginate);
    }

    public function createElement() {
        $photography = new \stdClass();

        $photography->title = $this->emptyLang;
        $photography->sefurl = $this->emptyLang;
        $photography->alergens = $this->emptyLang;
        $photography->meta_title = $this->emptyLang;
        $photography->meta_description = $this->emptyLang;
        $photography->cover = '';
        $photography->cover_list = '';
        $photography->status = 0;

        $this->viewData['prefix'] = 'photography';
        $this->viewData['photography'] = $photography;
        $this->viewData['photographyId'] = 0;
        $this->viewData['images'] = [];

        if(old()) {
            $id = old("photography-id");
            $this->viewData['images'] = $this->getGalleryByPhotographyId($id);
        }
//        $this->viewData['categories-recipes'] = $categories-recipes;

        return parent::createElement();
    }

    public function editElement($id) {
        $photography = new \stdClass();

        $photography->title = $this->emptyLang;
        $photography->alergens = $this->emptyLang;
        $photography->sefurl = $this->emptyLang;
        $photography->meta_title = $this->emptyLang;
        $photography->meta_description = $this->emptyLang;
        $photography->selected_categories = array();
        $photography->cover = '';
        $photography->cover_list = '';
        $photography->status = 0;

//        $photography->category_id = 0;

        $photographyQuery = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '.' . $this->primaryKey . ' = ? AND template = ?', array($id, $this->table))
            ->select(
                array(
                    'title',
                    'meta_title',
                    'meta_description',
                    'status',
                    'seourl.language_id',
                    'keyword',
                    'cover',
                    'cover2',

                )
            )
            ->get();

        if(count($photographyQuery) > 0) {
            $photography->cover = $photographyQuery[0]->cover;
            $photography->cover_list = $photographyQuery[0]->cover2;
            $photography->status = $photographyQuery[0]->status;

        }

        foreach ($photographyQuery as $result) {
            $photography->title[$result->language_id] = $result->title;
            $photography->alergens[$result->language_id] = $result->alergens;
            $photography->meta_description[$result->language_id] = $result->meta_description;
            $photography->meta_title[$result->language_id] = $result->meta_title;
            $photography->sefurl[$result->language_id] = $result->keyword;
        }


        $this->viewData['photography'] = $photography;
        $this->viewData['photographyId'] = $id;
        $this->viewData['images'] = $this->getGalleryByPhotographyId($id);
//        $this->viewData['categories-recipes'] = $categories-recipes;


        return parent::editElement($id);
    }

    protected function addElementData($data, $element)
    {
        if(isset($data['cover_list'])) {
            $cover = $data['cover_list'];

            $coverName = ImageController::changeImageName($cover);
            $elementImgPath = $this->imgPath . $this->folder;

            File::delete($elementImgPath . '/' . $element->cover2);
            KipoModel::saveImage($cover, $elementImgPath, $coverName);

            $element->cover2 = $coverName;
        }

        return parent::addElementData($data, $element); // TODO: Change the autogenerated stub
    }

    public static function pageModule() {
        $module = (object)[
            'controller' => 'App\Http\Controllers\front\PhotographyController',
            'method' => 'index',
            'index' => 'photography',
            'view' => 'photography'
        ];

        return $module;
    }

    public function insertGallery($data, $photographyId) {
        $galleryDescriptions = [];
        $photographyGalleryTable = DB::table("photography_gallery");

        $cover = $data['gallery-cover'];
        $coverName = ImageController::changeImageName($cover);

        KipoModel::saveImage($cover, $this->imgPath . $this->folder . "/gallery", $coverName);


        $photographyGalleryTable->increment("order_number");
        $galleryId = $photographyGalleryTable->insertGetId([
            "order_number" => 1,
            "cover" => $coverName,
            "photography_id" => $photographyId
        ]);

        foreach ($this->languages as $language) {
            $languageId = $language->language_id;
            $title = KipoModel::checkIsSet($data, "gallery-text", $languageId, "");

            $galleryDescriptions[] = [
                "title" => $title,
                "language_id" => $languageId,
                "gallery_id" => $galleryId
            ];
        }

        DB::table("photography_gallery_descriptions")->insert($galleryDescriptions);

        return $galleryId;

    }

    public function getGallery($id) {
        $result = DB::table("photography_gallery")
            ->join("photography_gallery_descriptions", "photography_gallery.id", "=", "photography_gallery_descriptions.gallery_id")
            ->where([
                "gallery_id" => $id,
                "language_id" => $this->langId
            ])
            ->get([
                "gallery_id as id",
                "title",
                "cover"
            ]);

        return $result;
    }

    public function getGalleryByPhotographyId($id) {
        $result = DB::table("photography_gallery")
            ->join("photography_gallery_descriptions", "photography_gallery.id", "=", "photography_gallery_descriptions.gallery_id")
            ->where([
                "photography_id" => $id,
                "language_id" => $this->langId
            ])
            ->orderBy("order_number")
            ->get([
                "gallery_id as id",
                "title",
                "cover"
            ]);

        return $result;
    }

    public function deleteGallery($id) {
        $galleryQuery = DB::table("photography_gallery")->where("id", $id);
        $gallery = $galleryQuery->first();

        if($gallery) {
            \File::delete($this->imgPath . $this->folder . "/gallery/" . $gallery->cover);
            $galleryQuery->delete();
        }

        DB::table("photography_gallery_descriptions")->where("gallery_id", $id);
    }

    public function deleteElement($id)
    {
        parent::deleteElement($id); // TODO: Change the autogenerated stub
    }
}
