<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Facades\DB;


class IndexModel extends Model  {

    public function index($langId) {
        $commercialModel = new CommercialModel();
        $filmModel = new FilmModel();
        $tvProjectModel = new TVProjectModel();
        $photographyModel = new PhotographyModel();

        $commercials = $commercialModel->getElements(
            $langId,
            true,
            false,
            ' AND status = 2',
            [],
            [
                'title',
                'cover',
                'video',
                'type',
                'description',

            ],
            false,
            3
        );

        $films = $filmModel->getElements(
            $langId,
            true,
            true,
            ' AND status = 2',
            [],
            [
                'title',
                'cover',
                'keyword',
            ],
            false,
            2
        );

        $tvProjects = $tvProjectModel->getElements(
            $langId,
            true,
            false,
            ' AND status = 2',
            [],
            [
                'title',
                'cover',
                'video',
                'type',
                'description',
            ],
            false,
            3
        );

        $photography = $photographyModel->getElements(
            $langId,
            true,
            true,
            ' AND status = 2',
            [],
            [
                'title',
                'cover',
                'keyword',
            ],
            false,
            8
        );

//        $blog = $blogModel->getBlogPosts($langId, false, 3);

        return [
            "commercials" => $commercials,
            "films" => $films,
            "tvProjects" => $tvProjects,
            "photography" => $photography,
        ];
    }

    public function makeKeywordML($template, $data) {
        $whereInStr = '';
        $whereInArr = [];
        $whereQueryIn = [];

        foreach ($data as $el) {
            if(!empty($el)) {
                $whereInStr .= '"' . $el . '", ';
                $whereInArr[] = $el;
            }
        }

        $whereInStr = rtrim($whereInStr, ', ');

        $seoResult = DB::table('seourl')
            ->where('template', $template)
            ->whereIn('keyword', $whereInArr)
            ->orderByRaw(DB::raw("FIELD(keyword, $whereInStr)"))
            ->groupBy('query')
            ->get();

        foreach ($seoResult as $result) {
            $whereQueryIn[] = $result->query;
        }

        $whereQueryInStr = implode($whereQueryIn);

        $keywords = DB::table('seourl')
            ->where('template', $template)
            ->whereIn('query', $whereQueryIn)
            ->orderByRaw(DB::raw("FIELD(query, $whereQueryInStr)"))
            ->get();

        return $keywords;
    }
}
