<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Language extends Model  {
	protected $table = 'languages';
        protected $primaryKey = 'language_id';
        public  $timestamps = false;
        private $userlogin;
        private $label;
        private $activeMenu;
        
	public function __construct() {
            $this->userlogin = Auth::user();
            $this->label = trans('constants.language');
            $this->activeMenu = 'languages';
        }
        
        public function showLanguages() {
            $languages = Language::orderBy('order_number','DESC')->paginate(10);
                
            $viewDate = array(
                'languages' => $languages,
                'userlogin' => $this->userlogin,
                'activeMenu' => $this->activeMenu
            );
            
            return $viewDate;
        }
        
        public function createLanguage() {
            $title = $this->label['hanging_title'];
            
            $language = (object) array(
                'name' =>'',
                'code'   => '',
                'local'  => '',
                'status' => 0
            );

            $viewData = array(
                'userlogin' => $this->userlogin,
                'title' => $title,
                'language' => $language,
                'form_data' => ['role' => 'form', 'url' => '/admin/languages'], 
                'label' => $this->label, 
                'activeMenu' => $this->activeMenu,
                'languageId' => 0,
            );
            
            return $viewData;
        }
        
        public function insertLanguage($data) {            
            $max = Language::get()->max('order_number');

            $language = new  Language;
            $language->name = $data['name'];
            $language->code = $data['code'];
            $language->local = $data['local'];
            $language->status = $data['status'];
            $language->order_number = $max+1;
            $language->save();
            
            $languageId = $language->id;
            return $languageId;
        }
        
        public function showLanguage($id) {
            $title = $this->label['hanging_title'];
            
            $language = Language::find($id); 

            $viewData = array(
                'userlogin' => $this->userlogin,
                'title' => $title,
                'language' => $language,
                'form_data' => ['role' => 'form', 'url' => '/admin/languages/'.$id, 'method' => 'put'], 
                'label' => $this->label, 
                'activeMenu' => $this->activeMenu,
                'languageId' => $id,
            );
            
            return $viewData;
        }
        
        public function updateLanguage($id, $data) {
            $language = Language::find($id);
            $language->name = $data['name'];
            $language->code = $data['code'];
            $language->local = $data['local'];
            $language->status = $data['status'];

            $language->save();
        }
        
        public function active($data) {
            Language::where('language_id', $data['id'])
            ->update(array(
                'status' => $data['active']
            ));
        }
        
        public function deleteLanguage($id) {
            Language::destroy($id);
        }
        public function newOrder() {
            return DB::table($table)->max('order_number') +1 ;
        }
}
