<?php

namespace App\Providers;

use \Request;
use App\SettingModel;
use Illuminate\Support\ServiceProvider;
use Validator;

class DeliveryProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('delivery', function($attribute, $value, $parameters) {
            $label = trans("constants.front");
            $shippingMethods = array_keys($label['shipping_methods']);

            if(in_array($value, $shippingMethods)) {

                if($value == 1) {
                    $settingModel = new SettingModel();
                    $addressQuery = $settingModel->getAddress();
                    $address = [];

                    foreach ($addressQuery as $a) {
                        $address[] = $a->id;
                    }
                    $data = Request::all();

                    if(isset($data['delivery-address-id']) && in_array($data['delivery-address-id'], $address)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }

            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
