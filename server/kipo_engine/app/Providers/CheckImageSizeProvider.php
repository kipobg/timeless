<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Support\Facades\Auth;
use \Hash;
use \DateTime;
use \Request;

class CheckImageSizeProvider extends ServiceProvider
{
    private $maxWidth = 1600;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('im-s', function($attribute, $value, $parameters) {
            dd($attribute);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
