<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class HoursDeliveryProviders extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('hours_delivery', function($attribute, $value, $parameters) {
            $label = trans("constants.front");

            $hours = array_keys($label['hours']);

            $data = \Request::all();

            $date = new \DateTime(date("Y-m-d", isset($data['day']) ? strtotime($data['day']) : 0));
            $now = new \DateTime(date("Y-m-d"));

            if(isset($hours[0])) {
                unset($hours[0]);
            }


            if($date->format("w") == 6) {
                //15 -19 h
                unset($hours[2]);
            } else {
                $now->modify("+1 days");
                if($date == $now) {
                    //09 - 13
                    unset($hours[1]);
                }
            }

            if(in_array($value, $hours)) {
                return true;
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
