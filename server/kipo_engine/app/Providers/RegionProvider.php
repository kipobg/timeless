<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class RegionProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('region', function($attribute, $value, $parameters) {
            $label = trans("constants.front");
            $regions = array_keys($label['regions']);
            if(isset($regions[0])) {
                unset($regions[0]);
            }

            if(in_array($value, $regions)) {
                return true;
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
