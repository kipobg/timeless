<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class DateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('date_delivery', function($attribute, $value, $parameters) {
            $date = new \DateTime(date("Y-m-d", strtotime($value)));
            $now = new \DateTime(date("Y-m-d"));

            if($date > $now && $date->format("w") > 0) {
                $now->modify("+7 days");

                if($date <= $now) {
                    return true;
                }
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
