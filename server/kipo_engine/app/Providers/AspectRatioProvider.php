<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Support\Facades\Auth;
use \Hash;
use \DateTime;
use \Request;

class AspectRatioProvider extends ServiceProvider
{
    private $maxWidth = 1600;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('aspect_ratio', function($attribute, $value, $parameters) {
            $data = Request::all();
            if(isset($data[$attribute])) {
                $cover = $data[$attribute];
                $getImageSizeData = getimagesize($cover);
                $width = $getImageSizeData[0];
                $height = $getImageSizeData[1];

                if(isset($parameters[0]) && $width < $this->maxWidth) {
                    $ratio = @eval("return " . $parameters[0] . ";" );
                    return ($width / $height) == $ratio;
                }

            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
