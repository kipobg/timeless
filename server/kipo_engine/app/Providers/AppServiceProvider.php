<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $label = trans("constants.front");

        Blade::directive('priceFormat', function($price) use ($label) {


            return '<?php $price = ' . $price . '; echo number_format($price[0], 2, ".", "") . $price[1]; ?>';
        });

        Blade::directive('date', function($date) use ($label) {
            return '<?php
                $date = ' . $date . ';
                $dateTime = new \DateTime( $date[0]);
                $month = $dateTime->format("n");
                $monthName = $date[1][$month];
                echo ($monthName . $dateTime->format(" d, Y"));
            ?>';
        });

        Blade::directive('checkDescription', function($arguments) use ($label) {
            return '<?php
                $arguments = ' . $arguments . ';
                $description = $arguments[0];
                $length = $arguments[1];
                
                if(mb_strlen($description, "UTF-8") > $length) {
                    echo mb_substr($description, 0, $length, "UTF-8") . "...";
                    
                } else {
                    echo $description;
                }
            ?>';
        });

        Blade::directive('getVideoLink', function($arguments) use ($label) {
            return '<?php
                $arguments = ' . $arguments . ';
                $videoCode = $arguments[0];
                $videoType = $arguments[1];
                
                if($videoType == 1) { // youtube
                    echo "https://www.youtube.com/watch?v=" . $videoCode;
                } elseif($videoType == 2) { //vimeo
                    echo "https://player.vimeo.com/video/" . $videoCode;
                }
            ?>';
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
