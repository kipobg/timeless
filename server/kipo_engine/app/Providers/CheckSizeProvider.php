<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Flysystem\Exception;
use Validator;
use Illuminate\Support\Facades\Auth;
use \Hash;
use \DateTime;
use \Request;

class CheckSizeProvider extends ServiceProvider
{
    private $maxWidth = 1600;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('check_size', function($attribute, $value, $parameters) {
            $data = Request::all();
            if(isset($data[$attribute])) {
                $cover = $data[$attribute];
                $getImageSizeData = getimagesize($cover);
                $width = $getImageSizeData[0];
                $height = $getImageSizeData[1];

                if($parameters < 2) {
                    throw new Exception("Parameters should be width, height", 500);
                }

                $inputWidth = $parameters[0];
                $inputHeight = $parameters[1];

                if($width >= $inputWidth && ($height == 'null' || ($height >= $inputHeight))) {
                    return true;
                }

                return false;
            }

            return false;
        });

        Validator::replacer('check_size', function ($message, $attribute, $rule, $parameters) {
            $result = str_replace(":more", ($parameters[1] == 'null' ? "по голям от" : ""), $message);

            if($parameters[1] == 'null') {
                unset($parameters[1]);
            }

            $result = str_replace(":parameters", implode("x", $parameters), $result) . "px";

            return $result;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
