<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class PaymentProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('payment', function($attribute, $value, $parameters) {
            $label = trans("constants.front");
            $paymentMethods = array_keys($label['payment_methods']);

            if(in_array($value, $paymentMethods)) {
                return true;
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
