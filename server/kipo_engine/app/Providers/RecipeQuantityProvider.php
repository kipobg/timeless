<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Request;

class RecipeQuantityProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('quantity', function($attribute, $value, $parameters) {
            $data = Request::all();

            if(isset($data['type']) && $data['type'] == 1) {
                return $value > 0 && $value % 2 == 0;
            }

            return $value > 0;

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
