<?php
namespace App;

use App\BaseModel as BM;
use \File;
use App\Http\Controllers\admin\ImageController;
use \DB;

class CommercialModel extends BM {
    protected  $table = 'commercials';
    protected  $primaryKey = 'id';
    protected  $constantsName = 'commercials';
    protected  $foreignKey = 'commercial_id';
    protected $className = self::class;
    protected $categoryModel;

    public function __construct($url = '', $folder = '', $isAdmin = false, $languages = [], $isGet = false) {
        parent::__construct($url, $folder, $isAdmin, $languages, $isGet);

        if(!$isGet && $isAdmin) {
            $this->baseData['status'] = ['default' => '0', 'index' => 'status', 'key' => ''];

            $this->baseDataDescription['title'] = ['default' => '', 'index' => 'title', 'key' => 'languageId'];
            $this->baseDataDescription['description'] = ['default' => '', 'index' => 'description', 'key' => 'languageId'];
            $this->baseDataDescription['meta_title'] = ['default' => '', 'index' => 'meta_title', 'key' => 'languageId'];
            $this->baseDataDescription['meta_description'] = ['default' => '', 'index' => 'meta_description', 'key' => 'languageId'];
        }

        $this->resizeWidth = ["large" => 500];
        $this->viewData['dimension'] = '8/5';
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        $this->viewData['commercials'] = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->whereRaw($this->table . '_descriptions.language_id = ? ', array($langId))
            ->orderBy('order_number')
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'language_id',
                    'cover',
                    'id',
                )
            )
            ->get();

        return parent::index($langId, $callByController, $paginate);
    }

    public function createElement() {
        $commercial = new \stdClass();

        $commercial->title = $this->emptyLang;
        $commercial->description = $this->emptyLang;
        $commercial->sefurl = $this->emptyLang;
        $commercial->alergens = $this->emptyLang;
        $commercial->meta_title = $this->emptyLang;
        $commercial->meta_description = $this->emptyLang;
        $commercial->video_code = '';
        $commercial->cover = '';
        $commercial->type = '';
        $commercial->status = 0;

        $this->viewData['prefix'] = 'commercials';
        $this->viewData['commercial'] = $commercial;
        $this->viewData['commercialId'] = 0;
//        $this->viewData['categories-recipes'] = $categories-recipes;

        return parent::createElement();
    }

    public function editElement($id) {
        $commercial = new \stdClass();

        $commercial->title = $this->emptyLang;
        $commercial->description = $this->emptyLang;
        $commercial->alergens = $this->emptyLang;
        $commercial->sefurl = $this->emptyLang;
        $commercial->meta_title = $this->emptyLang;
        $commercial->meta_description = $this->emptyLang;
        $commercial->selected_categories = array();
        $commercial->cover = '';
        $commercial->type = '';
        $commercial->video_code = '';
        $commercial->status = 0;

//        $commercial->category_id = 0;

        $commercialQuery = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '.' . $this->primaryKey . ' = ? AND template = ?', array($id, $this->table))
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'seourl.language_id',
                    'keyword',
                    'cover',
                    'video',
                    'type',

                )
            )
            ->get();

        if(count($commercialQuery) > 0) {
            $commercial->cover = $commercialQuery[0]->cover;
            $commercial->type = $commercialQuery[0]->type;
            $commercial->status = $commercialQuery[0]->status;
//            $commercial->category_id = $commercialQuery[0]->category_id;
            $commercial->video_code = $commercialQuery[0]->video;

        }

        foreach ($commercialQuery as $result) {
            $commercial->title[$result->language_id] = $result->title;
            $commercial->description[$result->language_id] = $result->description;
            $commercial->alergens[$result->language_id] = $result->alergens;
            $commercial->meta_description[$result->language_id] = $result->meta_description;
            $commercial->meta_title[$result->language_id] = $result->meta_title;
            $commercial->sefurl[$result->language_id] = $result->keyword;
        }


        $this->viewData['commercial'] = $commercial;
        $this->viewData['commercialId'] = $id;
//        $this->viewData['categories-recipes'] = $categories-recipes;


        return parent::editElement($id);
    }

    protected function addElementData($data, $element)
    {

        $videoLink = KipoModel::checkIsSet($data, "video-code", '', '');

        if(filter_var($videoLink, FILTER_VALIDATE_URL)) {

            $video = KipoModel::getVideoCode($videoLink, $this->videoType);
            $type = KipoModel::getVideoType($videoLink, $this->videoType);

            $element->video = $video;
            $element->type = $type;
        }

        return parent::addElementData($data, $element);
    }

    public static function pageModule() {
        $module = (object)[
            'controller' => 'App\Http\Controllers\front\CommercialController',
            'method' => 'index',
            'index' => 'commercial',
            'view' => 'commercial'
        ];

        return $module;
    }

    public function deleteElement($id)
    {
        parent::deleteElement($id); // TODO: Change the autogenerated stub
    }
}
