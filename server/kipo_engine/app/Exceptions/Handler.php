<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\KipoModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Exception\HttpResponseException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $parentResponse = parent::render($request, $e);

//        if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
//            return redirect('/');
//        } elseif ($e instanceof MethodNotAllowedHttpException) {
//            return redirect("/");
//    }    elseif($e instanceof HttpException && $e->getCode() == 400) {
//            return $e->getMessage();
//        } elseif($e instanceof TokenMismatchException) {
//            $label = trans("constants.front");
//
//            return response("The CSRF token is invalid. Please try to resubmit the form", 403);
//        } else {
//            if ((!$e instanceof HttpResponseException) && !($e instanceof ValidationException)) {
//                $messages = $parentResponse->getContent();
//
//                KipoModel::sendSupportEmail($messages);
//
//                return Redirect::to('/');
//
//            }
//
//        }
//
        return $parentResponse;

    }
}
