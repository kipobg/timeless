<?php

namespace App\Http\Controllers\front;
use App\AwardModel;
use App\CodeModel;
use App\IndexModel;
use App\KipoFrontModel;
use App\Http\Requests;
use App\KipoModel;
use App\NavBarPagesModel;
use App\PageModel;
use App\RegistrationModel;
use App\UserModel;
use Illuminate\Support\Facades\Session;
use DateTime;
use Request;

class PageController extends FrontBaseController {
    protected $view = 'page';
    protected $folder = '';
    private $registrationModel;

    public function __construct() {
        parent::__construct();
        $this->pageModel = new PageModel();
    }

    public function show($keyword, $keyword1 = '', $keyword2 = '', $keyword3 = '', $keyword4 = '', $keyword5 = '', $keyword6 = '', $keyword7 = '', $keyword8 = '', $keyword9 = '') {
        $pagesPath = [
            $keyword9,
            $keyword8,
            $keyword7,
            $keyword6,
            $keyword5,
            $keyword4,
            $keyword3,
            $keyword2,
            $keyword1,
            $keyword,
        ];
        $listSection = true;

        $locale = $this->langPrefix;
        $breadcrumbs = [];
        $position = 0;

        $result = $this->pageModel->getPagesWhereKeywordIn($pagesPath, $this->langId, $position);

        if(count($result['pages']) == 0) {
            $result = $this->pageModel->getPagesWhereKeywordIn($pagesPath, $this->langId, 1);
        }

        
        $pages = $result['pages'];
        $params = $result['params'];
        $countPages = count($pages);
        $lastPagesIndex = $countPages - 1;

        if($countPages == 0) {
            abort(404);
        } elseif (count($params) != $countPages) {
            abort(404);
        } elseif ($pages[$lastPagesIndex]->parent_id > 0) {
            abort(404);
        }


        $prefix = '';

        for($i = 0; $i < $countPages - 1; $i++) {
            $currentPage = $pages[$i];
            $nextPage = $pages[$i + 1];

            if($currentPage->parent_id != $nextPage->page_id) {
                abort(404);

            }

            $href = $locale . $prefix;

            for($j = $countPages- 1; $j > $i; $j--) {
                $href .= '/' . $pages[$j]->keyword;
            }

            $href .= '/' . $pages[$i]->keyword;

            $breadcrumbs[$i] = [
                'title' => $pages[$i]->title,
                'href' => $href,
            ];
        }



        $breadcrumbs[$countPages] = [
            'title' => $this->label['home'],
            'href' => $locale . '/',
        ];
        $breadcrumbs[$lastPagesIndex] = [
            'title' => $pages[$lastPagesIndex]->title,
            'href' => $locale . $prefix . '/' . $pages[$lastPagesIndex]->keyword,
        ];

        krsort($breadcrumbs);

        $pageId = $pages[0]->page_id;

        $page = $this->pageModel->getPageById($pageId);

        $this->viewData['modules'] = '';
        $this->viewData['videos'] = [];

        if($page->serialize) {
            $modules = unserialize($page->serialize);

            foreach($modules as $module) {
                $controller = new $module->controller;
                $method = $module->method;
                $this->viewData['modules'] .= $controller->$method(true, $page);
            }
        }

        if($page->video_serialize) {
            $videos = unserialize($page->video_serialize);

            foreach($videos as $video) {

                $this->viewData['videos'][] = $video;
            }
        }

        $this->generateLanguages(true, 'pages', '', $pagesPath);

        $this->viewData['urlParams'] = $params;
        $this->viewData['listSection'] = $listSection;
        $this->viewData['breadcrumbs'] = $breadcrumbs;
        $this->viewData['pageImgFolder'] = $this->baseImgFolder . '/pages';
        $this->viewData['title'] = $page->meta_title;
        $this->viewData['description'] = $page->meta_description;
        $this->viewData['page'] = $page;

        return $this->render(true);
    }
}