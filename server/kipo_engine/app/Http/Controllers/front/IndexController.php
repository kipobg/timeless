<?php

namespace App\Http\Controllers\front;
use App\AwardModel;
use App\CodeModel;
use App\IndexModel;
use App\KipoFrontModel;
use App\RegistrationModel;
use Request;

class IndexController extends FrontBaseController {
    protected $view = 'index';
    protected $folder = '';
    protected $products;
    protected $indexModel;

    protected $rules = [
        'full-name' => 'required|max:50',
        'address' => 'required|max:255',
        'phone' => 'required|max:15|bg_phone',
        'email' => 'required|email|max:50',
        'terms-agree' => 'required',
        'invoice' => 'max:255'
    ];


    public function __construct() {
        parent::__construct();
        $this->indexModel = new IndexModel();
    }

    public function index() {
        $this->viewData = array_merge($this->indexModel->index($this->langId), $this->viewData);
        $this->viewData['title'] = $this->viewData['settings']['title'];
        $this->viewData['description'] = $this->viewData['settings']['metaDescription'];
        $this->viewData['footerClass'] = 'home';
        $this->viewData['isHome'] = true;

        $scripts = [
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyBe8f3wc84yXBFnHfHJrdUv_cMKcM78Fzo&callback=initMap&language=' . $this->locale
        ];

        $this->generateLanguages(false, false);
//        $this->addScripts($scripts, 'footer');
        return $this->render(true);
    }


}