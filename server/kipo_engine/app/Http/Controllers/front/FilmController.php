<?php

namespace App\Http\Controllers\front;
use App\FilmModel;
use App\PageModel;
use Request;

class FilmController extends FrontBaseController {
    protected $view = 'index';
    protected $folder = 'films';
    protected $filmModel;

    public function __construct() {
        parent::__construct();
        $this->filmModel = new FilmModel();
    }

    public function index($return = true, &$page = null) {
        $page->class='film-page';
        $page->show_title = false;
        $films = $this->filmModel
            ->getElements(
                $this->langId,
                true,
                false,
                "",
                [],
                ["title", "cover", "description"]
            );


        $this->viewData['films'] = $films;

        return $this->render($return);
    }


}