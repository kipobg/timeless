<?php

namespace App\Http\Controllers\front;
use App\CommercialModel;
use App\PageModel;
use Request;

class CommercialController extends FrontBaseController {
    protected $view = 'index';
    protected $folder = 'commercials';
    protected $commercialModel;

    public function __construct() {
        parent::__construct();
        $this->commercialModel = new CommercialModel();
    }

    public function index($return = true, &$page = null) {
        if($page != null) {
            $page->show_title = false;
        }

        $commercials = $this->commercialModel
            ->getElements(
                $this->langId,
                true,
                false,
                "",
                [],
                ["title", "cover", "video", "type"],
                true,
                12
            );


        $this->viewData['commercials'] = $commercials;
        $this->viewData['isInner'] = true;

        return $this->render($return);
    }




}