<?php

namespace App\Http\Controllers\front;
use App\IndexModel;
use App\NavBarModel;
use App\PageModel;
use App\SettingModel;
use App\UserGroupModel;
use App\UserModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\KipoModel;
use Illuminate\Validation\ValidationException;
use Request;
use Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\InquiryRequest;
use \Config;
use \File;
use App\Http\Traits\CartTrait;
use App\Http\Traits\AbortTrait;
use Illuminate\Support\Facades\Route;

class FrontBaseController extends Controller {
    use CartTrait;
    use AbortTrait;

    protected $folder = '';
    protected $label;
    protected $viewData = [];
    protected $publicFolder = '/kipo_front';
    protected $baseModel;
    protected $langId;
    protected $rules;
    protected $user;
    protected $data;
    protected $baseImgFolder = '/images';
    protected $locale;
    protected $navBarModel;
    protected $pageModel;
    protected $languages;
    protected $indexModel;
    protected $woodworks;
    protected $langPrefix;
    protected $settingsModel;
    protected $settings;
    protected $viewPath = 'kipo_front.views';
    protected $discount = 0;
    protected $view;
    protected $getCartInfo = true;
    protected $msgConstant = "messages";
    protected $messages;
    protected $validator;
    protected $payment = true;
    protected $orderData;
    protected $orderTypes;
    protected $freePrice;

    public function __construct() {
        $this->messages = trans($this->msgConstant);
        $this->settingsModel = new SettingModel();
//        $this->indexModel = new IndexModel();
        $this->navBarModel = new NavBarModel();
        $this->pageModel = new PageModel();

        $this->label = trans('constants.front');
        $this->langId = KipoModel::getCurrentLangId();
        $this->user = Auth::user();
        $this->data = Request::all();

        $this->locale = KipoModel::getLocale();
        $this->langPrefix = KipoModel::checkLangCode($this->locale);
        $this->settings = $this->settingsModel->getSettings($this->langId);
        $this->viewData['user_group'] = [];
        $this->discount = 0;

        $this->viewData['langPrefix'] = $this->langPrefix;
        $this->viewData['locale'] = $this->locale;
        $this->viewData['settings'] = $this->settings;

        $this->orderData = \Config::get("order");
        $this->orderTypes = $this->orderData['types'];

        $topMenu = $this->navBarModel->getNavBar(['position' => 0]);

        if(count($topMenu) == 0) {
            abort(500, 'Please select top menu');
        }


        $this->viewData['keywords'] = Request::segments();
        $this->viewData['viewPath'] = $this->viewPath;
        $this->viewData['languages'] = [];
        $this->viewData['footerClass'] = '';
        $this->viewData['topMenu'] = $this->pageModel->getNavBarPages($this->langId, $topMenu->id);
        $this->viewData['user'] = $this->user;
        $this->viewData['isLogged'] = !empty($this->user);
        $this->viewData['langId'] = $this->langId;
        $this->viewData['label'] = $this->label;
        $this->viewData['publicFolder'] = $this->publicFolder;
        $this->viewData['baseImgFolder'] = $this->baseImgFolder;
        $this->viewData['head'] = [];
        $this->viewData['footer'] = [];
        $this->viewData['head']['scripts'] = [];
        $this->viewData['footer']['scripts'] = [];
        $this->viewData['imgFolder'] = '/images';
        $this->viewData['styles'] = [];
        $this->viewData['scripts'] = [];
        $this->viewData['baseImgPath'] = '/kipo_front/images/brdige.png';
        $this->indexModel = new IndexModel();
    }

    protected function render($return  = false) {
        $action = Route::getCurrentRoute()->getActionName();

        if(strpos($action, "AuthController") == false && strpos($action, "ProfileController") == false) {
            session()->put("previous_url", url()->current());
        }

        $view = view('kipo_front.views.' . $this->folder . '.' . $this->view, $this->viewData)->render();

        if($return) {
            return $view;
        } else {
            echo $view;
        }

    }

    public function sendInquiry(InquiryRequest $request) {
        $data = $request->all();

        $users = UserModel::where('receive_email', 1)->get();
        $title = $this->label['inq_email_title'];
        $description = sprintf(
            $this->label['inq_email_description'],
            $this->label['name'] . ': ' . strip_tags($data['name']),
            $this->label['phone'] . ': ' . strip_tags($data['phone']),
            $this->label['email'] . ': ' . strip_tags($data['email']),
            $this->label['woodwork'] . ': ' . strip_tags($this->woodworks[ $data['woodwork'] ]),
            $this->label['inq_cont'] . ': ' . strip_tags($data['inq-cont'])
        );

        foreach ($users as $user) {
            KipoModel::sendEmail($user->email, $title, $description);
        }

        return json_encode(['success' => $this->label['inq_success']]);
    }
    public function generateLanguages($isSeo, $template, $prefix = '', $whereIn = []) {
        $this->languages = [];

        $languages = KipoModel::getLanguages();

        foreach ($languages as $language) {
            $this->languages[$language->language_id] = [
                'href' => (KipoModel::checkLangCode($language->language_code) . ($prefix ? '/' . $prefix : "")) . (!$isSeo && $language->language_code == 'bg' ? '/' : ''),
                'code' => $language->language_code
            ];
        }

        if($isSeo) {
            $keywords = $this->indexModel->makeKeywordML($template, $whereIn);

            foreach ($keywords as $k) {
                $this->languages[$k->language_id]['href'] .= '/' . $k->keyword;
            }
        }

        $this->viewData['languages'] = $this->languages;

    }

    protected function addScripts($scripts, $position) {
        $this->viewData[$position]['scripts'] = array_merge($this->viewData['scripts'], $scripts);
    }

    protected function addStyles($styles) {
        $this->viewData['styles'] = array_merge($this->viewData['scripts'], $styles);
    }

    protected function isMethod($method) {
        return Request::isMethod($method);
    }

    public function validateForm() {
        $this->validator = Validator::make($this->data, $this->rules);

        if($this->validator->fails()) {
            $firstMsg = $this->validator->messages()->first();
            $validationData = ['errors' => $this->validator->messages(), "msg" => $firstMsg, "text" => $firstMsg];

            $this->abortValidation($this->validator, $validationData);
        }

    }
    public function page404() {
        $this->view = 'page-404';
        $this->viewData['title'] = '404';
        $this->viewData['description'] = '404';

        return Response::make($this->render(), 404);
    }

    public function getPrivateFile($fileName) {
        $imgPath = Config::get('app.image_path');
        $path = $imgPath . 'private_files/' . $fileName;

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
