<?php

namespace App\Http\Controllers\front;
use App\CommercialModel;
use App\PageModel;
use App\TVProjectModel;
use Request;

class TVProjectController extends FrontBaseController {
    protected $view = 'index';
    protected $folder = 'tv-projects';
    protected $tvProjectModel;

    public function __construct() {
        parent::__construct();
        $this->tvProjectModel = new TVProjectModel();
    }

    public function index($return = true, &$page = null) {
        if($page != null) {
            $page->show_title = false;
        }

        $commercials = $this->tvProjectModel
            ->getElements(
                $this->langId,
                true,
                false,
                "",
                [],
                ["title", "cover", "video", "type"],
                true,
                12
            );

        $this->viewData['isInner'] = true;

        $this->viewData['tvProjects'] = $commercials;

        return $this->render($return);
    }


}