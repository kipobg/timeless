<?php

namespace App\Http\Controllers\front;
use App\CommercialModel;
use App\PageModel;
use App\PhotographyModel;
use Request;

class PhotographyController extends FrontBaseController {
    protected $view = 'index';
    protected $folder = 'photography';
    protected $photographyModel;

    public function __construct() {
        parent::__construct();
        $this->photographyModel = new PhotographyModel();
    }

    public function index($return = true, &$page = null) {
        $page->show_title = false;
        $photography = $this->photographyModel
            ->getElements(
                $this->langId,
                true,
                true,
                "",
                [],
                ["title", "cover", "keyword"]
            );


        $this->viewData['photography'] = $photography;

        return $this->render($return);
    }

    public function show($keyword) {
        $this->view = 'show';
        $photography = $this->photographyModel->getElementByKeyword($keyword, $this->langId);

        if(count($photography) == 0) {
            abort(404);
        }

        $breadcrumbs[2] = [
            'title' => $this->label['home'],
            'href' => $this->langPrefix . '/',
        ];
        $breadcrumbs[1] = [
            'title' => $this->label['heading']['photography'],
            'href' => $this->langPrefix . '/photography',
        ];
        $breadcrumbs[0] = [
            'title' => $photography->title,
            'href' => $this->langPrefix . '/photography/' . $photography->keyword,
        ];

        $photographyId = $photography->photography_id;

        $gallery = $this->photographyModel->getGalleryByPhotographyId($photographyId);

        $this->generateLanguages(true, "photography", "photography", [$keyword]);

        $this->viewData['breadcrumbs'] = $breadcrumbs;
        $this->viewData['title'] = $photography->meta_title;
        $this->viewData['description'] = $photography->meta_description;
        $this->viewData['gallery'] = $gallery;

        $this->viewData['photography'] = $photography;
        return $this->render(true);

    }
}