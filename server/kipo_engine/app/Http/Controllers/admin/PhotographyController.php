<?php
namespace App\Http\Controllers\admin;

use App\Http\Requests\GalleryRequest;
use App\Http\Requests\photographyRequest;
use App\PhotographyModel;
use App\Http\Requests\FileRequest;
use App\Http\Controllers\admin\BaseController as BC;
use App\KipoModel;

class PhotographyController extends BC {
    private $label;

    public function __construct() {
        $this->folderName = 'photography';
        $this->url = '/admin/photography';
        $this->id = 'id';
        $this->className = PhotographyModel::class;
        $this->aspectRatio = 2 / 3;

        parent::__construct();

        $this->label = $this->getLabel();
    }

    public function rules($data) {
        $rules = [];

        $filmId = isset($data['photography-id']) ? $data['photography-id'] : 0;

        $rules['status'] = 'required|between:0,1';

        foreach ($this->languages as $lang) {
            $rules['title.' . $lang->language_id] = 'required|min:3|max:255';

            $rules['cover'] = 'mimes:jpeg,jpg,png|check_size:400,400';
            $rules['cover_list'] = 'mimes:jpeg,jpg,png|check_size:400,400';

            if($filmId > 0) {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword,' . $filmId . ',query';
            } else {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword';
            }
        }


        return $rules;
    }

    public function attributes() {
        $attributes = [];

        foreach ($this->languages as $lang) {
            $attributes['title.' . $lang->language_id] = $this->label['title'] . '(' . $lang->language_name . ')';
            $attributes['description.' . $lang->language_id] = $this->label['description'] . '(' . $lang->language_name . ')';
            $attributes['sefurl.' . $lang->language_id] = $this->label['sefurl'] . '(' . $lang->language_name . ')';
            $attributes['meta_title.' . $lang->language_id] = $this->label['meta_title'] . '(' . $lang->language_name . ')';
            $attributes['meta_description.' . $lang->language_id] = $this->label['meta_description'] . '(' . $lang->language_name . ')';
        }


        return $attributes;
    }

    public function store() {
        $filmId = $this->data['photography-id'];

        if($filmId == 0) {
            return parent::store();
        } else {
            return $this->update($filmId);
        }
    }

    public function uploadGallery(GalleryRequest $request) {
        $data = $request->all();
        $photographyId = $data['photography-id'];

        if($photographyId == 0) {
            $result = $this->model->insertElement($data);
            $photographyId = $result['id'];
        }

        $id = $this->model->insertGallery($data, $photographyId);

        $photography = $this->model->getGallery($id);

        $html = KipoModel::renderAdminView('photography.gallery', ['images' => $photography])->render();

        return [
            'photography_id' => $photographyId,
            'html' => $html,
            'data' => $data
        ];
    }

    public function deleteGallery($id) {
        $this->model->deleteGallery($id);

        return $id;
    }

    public function pleachGallery() {
        $data = $this->data;
        $data['objectId'] = $this->id;

        KipoModel::pleach($data, "photography_gallery", false);
    }
}