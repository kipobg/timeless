<?php
/**
 * Created by PhpStorm.
 * User: Hristo
 * Date: 1/3/2016
 * Time: 11:23 AM
 */

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SettingModel;
use Request;
use App\KipoModel;
use App\Http\Requests\SettingRequest;
use Illuminate\Support\Facades\Redirect;
use \DB;
use App\Http\Requests\AddressRequest;

class SettingController extends Controller {

    private $settingModel;
    private $folderName = 'settings';
    private $url = '/admin/settings';
    private $id = 'page_id';

    public function __construct() {
        $this->settingModel = new SettingModel();

        if(!Request::isMethod('GET')) {
            $this->middleware('csrf');
        }

    }

    public function index() {
        $viewData = $this->settingModel->createSettings();

        return view('kipo_admin/views/settings/settings', $viewData);
    }

    public function postGallery(Requests\SettingGalleryRequest $request) {
        $data = $request->all();
        $galleryId = $this->settingModel->insertGallery($data);
        $gallery = $this->settingModel->getGallery($galleryId);
        $html = view('kipo_admin/views/settings/gallery', ['gallery' => $gallery])->render();

        echo $html;
    }

    public function store(SettingRequest $request) {
        $data = Request::all();
        $result = $this->settingModel->insertSetting($data);

        return Redirect::back()->withErrors(['msg' => $result['msg']]);
    }


    public function update($id, PageRequest $req) {
        $data = Request::all();
        $result = $this->SettingModel->updatePage($data, $id);

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function pleach() {
        $data = Request::all();
        $data['objectId'] = 'id';
        foreach ($data['pleach'] as $key=>$el) {
            DB::table('settings_gallery')->where($data['objectId'], $el['id'])
                ->update(array(
                    'order_number' => ($key+1),
                ));
        }
    }

    public function destroy($id) {
        $this->settingModel->deleteGallery($id);

        return $id;
    }

    public function addAddress(AddressRequest $request) {
        $data = $request->all();

        $addressId = $this->settingModel->insertAddress($data);

        $address = $this->settingModel->getAddress($addressId);

        return KipoModel::renderAdminView("settings.address", ["address" => $address]);
    }

    public function pleachAddress() {
        $data = Request::all();
        $data['objectId'] = 'id';
        foreach ($data['pleach'] as $key=>$el) {
            DB::table('address')->where($data['objectId'], $el['id'])
                ->update(array(
                    'order_number' => ($key+1),
                ));
        }
    }

    public function deleteAddress($id) {
        $this->settingModel->deleteAddress($id);

        return $id;
    }

    public function addPartner(Requests\PartnerRequest $request) {
        $data = $request->all();

        $addressId = $this->settingModel->insertPartner($data);

        $address = $this->settingModel->getPartners($addressId);

        return KipoModel::renderAdminView("settings.partners", ["partners" => $address]);
    }

    public function pleachPartner() {
        $data = Request::all();
        $data['objectId'] = 'id';

        foreach ($data['pleach'] as $key=>$el) {
            DB::table('partners')->where($data['objectId'], $el['id'])
                ->update(array(
                    'order_number' => ($key+1),
                ));
        }
    }

    public function deletePartner($id) {
        $this->settingModel->deletePartner($id);

        return $id;
    }
}
