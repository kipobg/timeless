<?php
/**
 * Created by PhpStorm.
 * User: Hristo
 * Date: 1/3/2016
 * Time: 11:23 AM
 */

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GalleryModel;
use Request;
use App\KipoModel;
use App\Http\Requests\SettingRequest;
use Illuminate\Support\Facades\Redirect;
use \DB;
use App\Http\Requests\AddressRequest;

class GalleryController extends Controller {

    private $galleryModel;
    private $folderName = 'gallery';
    private $url = '/admin/gallery';
    private $id = 'id';

    public function __construct() {
        $this->galleryModel = new GalleryModel();

        if(!Request::isMethod('GET')) {
            $this->middleware('csrf');
        }

    }

    public function index() {
        $viewData = $this->galleryModel->createGallery();


        return view('kipo_admin/views/gallery/index', $viewData);
    }

    public function postGallery(Requests\SettingGalleryRequest $request) {
        $data = $request->all();
        $galleryId = $this->galleryModel->insertGallery($data);
        $gallery = $this->galleryModel->getGallery($galleryId);
        $html = view('kipo_admin/views/gallery/gallery', ['gallery' => $gallery])->render();

        echo $html;
    }



    public function pleach() {
        $data = Request::all();
        $data['objectId'] = 'id';
        foreach ($data['pleach'] as $key=>$el) {
            DB::table('settings_gallery')->where($data['objectId'], $el['id'])
                ->update(array(
                    'order_number' => ($key+1),
                ));
        }
    }

    public function destroy($id) {
        $this->galleryModel->deleteGallery($id);

        return $id;
    }

    protected function active() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::active($data, GalleryModel::class);
    }
}
