<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use \Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Config;
use \File;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller {
    static $fitWidth = 360;
    static $fitHeight = 270;
    static $resizeWidth = 1600;
    static $resizeHeight = 900;
    static $selector = 'cover';

    public static function changeImageName($fileName, $length=15, $getOnlyExt = false) {
        $extension = strtolower(File::extension($fileName->getClientOriginalName()));

        if($getOnlyExt) {
            return $extension;
        }

        $name = strtolower($fileName->getClientOriginalName());
        $name = str_replace('.' . $extension, '', $name);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . '.' . $extension;
    }

    public static function checkingImageResolution($img, $defaultWidth, $defaultHeight) {
        if($img === null) {
            return false;
        }
        $getImageSizeData = getimagesize($img);
        $width = $getImageSizeData[0];
        $height = $getImageSizeData[1];
        if($width >= $defaultWidth && $height >= $defaultHeight) {
            return true;
        }
        return false;
    }

    public function upload() {
        $data = Input::all();
		self::$fitWidth = $data['width'];
		self::$fitHeight = $data['height'];
		
        if(self::checkingImageResolution(Input::file('cover'),  self::$fitWidth, self::$fitHeight)) {
            $image_path = Config::get('app.image_path');
            $image = self::changeImageName(Input::file('cover'));
            File::delete([$image_path . $data['folder'] . '/' . $data['file'], $image_path . $data['folder'] . '/' . 'thumbnail_' . $data['file']]);

            Image::make(Input::file('cover'))
                ->save($image_path . $data['folder'] . '/' . $image);
			
			if(isset($data['thumbnail'])) {
				Image::make(Input::file('cover'))
                    ->crop($data['width'], $data['height'], $data['dataX'], $data['dataY'])
                    ->save($image_path . $data['folder'] . '/thumbnail_' . $image);
			}
            //DB::table('setting')->where('key', 'image_count')->increment('count');

            return ['fails' => false, 'selector' => self::$selector, 'imgFolder' => $data['folder'], 'image' => $image, 'response' => $data];

        }
        else {
            return ['fails' => true, 'error' => "Минималният размер на снимката трябва да бъде " . self::$fitWidth . " x " . self::$fitHeight . "!"];
        }
        //return $image_path.$data['folder'].'/big_'.$image;
    }

}
