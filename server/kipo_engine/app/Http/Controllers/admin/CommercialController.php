<?php
namespace App\Http\Controllers\admin;

use App\CommercialModel;
use App\Http\Requests\FileRequest;
use App\Http\Controllers\admin\BaseController as BC;


class CommercialController extends BC {
    private $label;

    public function __construct() {
        $this->folderName = 'commercials';
        $this->url = '/admin/commercials';
        $this->id = 'id';
        $this->className = CommercialModel::class;
        $this->aspectRatio = 2 / 3;

        parent::__construct();

        $this->label = $this->getLabel();
    }

    public function rules($data) {
        $rules = [];

        $commercialId = isset($data['commercial-id']) ? $data['commercial-id'] : 0;

        $rules['status'] = 'required|between:0,1';

        foreach ($this->languages as $lang) {
            $rules['title.' . $lang->language_id] = 'required|min:3|max:255';

            $rules['cover'] = 'mimes:jpeg,jpg,png|check_size:500,313';

            if($commercialId > 0) {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword,' . $commercialId . ',query';
            } else {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword';
            }
        }


        return $rules;
    }

    public function attributes() {
        $attributes = [];

        foreach ($this->languages as $lang) {
            $attributes['title.' . $lang->language_id] = $this->label['title'] . '(' . $lang->language_name . ')';
            $attributes['description.' . $lang->language_id] = $this->label['description'] . '(' . $lang->language_name . ')';
            $attributes['sefurl.' . $lang->language_id] = $this->label['sefurl'] . '(' . $lang->language_name . ')';
            $attributes['meta_title.' . $lang->language_id] = $this->label['meta_title'] . '(' . $lang->language_name . ')';
            $attributes['meta_description.' . $lang->language_id] = $this->label['meta_description'] . '(' . $lang->language_name . ')';
        }


        return $attributes;
    }

    public function store() {
        $commercialId = $this->data['commercial-id'];

        if($commercialId == 0) {
            return parent::store();
        } else {
            return $this->update($commercialId);
        }
    }
}