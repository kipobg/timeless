<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\UserModel;
use App\KipoModel;
use App\Http\Requests\UserRequest;


class UserController extends Controller {
    
    private $userModel;
    private $folderName = 'users';
    private $url = '/admin/users';
    private $id = 'id';
    
    public function __construct() {
        $this->userModel = new UserModel($this->url);

        if(!Request::isMethod('GET')) {
            $this->middleware('csrf');
        }
        
    }

    public function index() {
        $viewData = $this->userModel->showUsers();

        return KipoModel::renderAdminIndex($viewData, $this->folderName);
    }

    public function create() {
        $viewData = $this->userModel->createUser();

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function store(UserRequest $req) {
        $data = Request::all();
        $result = $this->userModel->insertUser($data);
        $id = $result['id'];

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function edit($id) {
            $viewData = $this->userModel->editUser($id);

            return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function update($id, UserRequest $req) {
            $data = Request::all();
            $result = $this->userModel->updateUser($id, $data);

            return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function destroy($id) {
        $this->userModel->deleteUser($id);

        return $id;
    }

//    public function disable() {
//        $data = Request::all();
//        $id = $data['id'];
//        $status = $data['active'];
//        $this->userModel->disableUser($id, $status);
//
//        return $id;
//    }
//
}
