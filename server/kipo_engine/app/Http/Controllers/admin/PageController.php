<?php
/**
 * Created by PhpStorm.
 * User: Hristo
 * Date: 1/3/2016
 * Time: 11:23 AM
 */

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PageModel;
use Request;
use App\KipoModel;
use App\Http\Requests\PageRequest;

class PageController extends Controller {

    private $pageModel;
    private $folderName = 'pages';
    private $url = '/admin/pages';
    private $id = 'id';
    private $langId;

    public function __construct() {
        $this->langId = KipoModel::getCurrentLangId();
        $this->pageModel = new PageModel($this->url, $this->folderName, true);

        if(!Request::isMethod('GET')) {
            $this->middleware('csrf');
        }

    }

    public function index() {
        $viewData = $this->pageModel->index($this->langId, true);

        return KipoModel::renderAdminIndex($viewData, $this->folderName);
    }

    public function create() {
        $viewData = $this->pageModel->createPage();

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function store(PageRequest $request) {
        $data = Request::all();
        $result = $this->pageModel->insertPage($data);
        $id = $result['id'];

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function edit($id) {
        $viewData = $this->pageModel->editPage($id);

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function update($id, PageRequest $req) {
        $data = Request::all();
        $result = $this->pageModel->updatePage($data, $id);

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function destroy($id) {
        $this->pageModel->deletePage($id);

        return $id;
    }

    public function active() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::active($data, PageModel::class);
    }

    public function pleach() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::pleach($data, PageModel::class);
    }
}
