<?php
/**
 * Created by PhpStorm.
 * User: Hristo
 * Date: 1/3/2016
 * Time: 11:23 AM
 */

namespace App\Http\Controllers\admin;

use App\GalleryImageModel;
use App\NavBarModel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\KipoModel;
use App\Http\Requests\GalleryRequest;
use App\Http\Requests\NavBarRequest;
use DB;

class NavBarController extends Controller {

    private $navBarModel;
    private $folderName = 'nav-bar';
    private $url = '/admin/nav-bar';
    private $id = 'id';
    private $langId;
    public function __construct() {
        $this->langId = KipoModel::getCurrentLangId();
        $this->navBarModel = new NavBarModel($this->url, $this->folderName, true);

        if(!Request::isMethod('GET')) {
            $this->middleware('csrf');
        }

    }

    public function index() {
        $viewData = $this->navBarModel->index($this->langId);

        return KipoModel::renderAdminIndex($viewData, $this->folderName);
    }

    public function create() {
        $viewData = $this->navBarModel->createNavBar();

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function store(NavBarRequest $request) {
        $data = Request::all();

        $result = $this->navBarModel->insertNavBar($data);

        $id = $result['id'];

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function edit($id) {
        $viewData = $this->navBarModel->editNavBar($id);

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    public function update($id, NavBarRequest $req) {
        $data = Request::all();

        $result = $this->navBarModel->updateNavBar($data, $id);

        return KipoModel::redirect($data, $result, $id, $this->url);
    }

    public function destroy($id) {
        $this->navBarModel->deleteNavBar($id);

        return $id;
    }

    public function pleach() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::pleach($data, NavBarModel::class, false);
    }

    public function active() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::active($data, NavBarModel::class);
    }
}
