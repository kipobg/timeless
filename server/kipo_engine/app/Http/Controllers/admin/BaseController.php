<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use \Validator;
use Request;
use App\KipoModel;

abstract class BaseController extends Controller {

    protected $model;
    protected $folderName = 'products';
    protected $url = '/admin/products';
    protected $id = 'id';
    protected $langId;
    protected $className;
    protected $requestClass;
    protected $data;
    protected $languages;

    abstract function rules($data);
    abstract function attributes();

    public function __construct() {
        $isGet = Request::isMethod('GET');
        $this->languages = KipoModel::getLanguages();
        $this->langId = KipoModel::getCurrentLangId();
        $this->model = new $this->className($this->url, $this->folderName, true, $this->languages, $isGet);
        $this->data = Request::all();
        if(!$isGet) {
            $this->middleware('csrf');
        }

    }

    protected function index() {
        $viewData = $this->model->index($this->langId);

        return KipoModel::renderAdminIndex($viewData, $this->folderName);
    }

    protected function getLabel() {
        return $this->model->getLabel();
    }

    protected function create() {
        $viewData = $this->model->createElement();

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    protected function store() {
        $rules = $this->rules($this->data);
        $errors = $this->hasErrors($this->data, $rules);

        if(count($errors) > 0) {
            return $this->redirect($this->data, $errors);
        }

        $result = $this->model->insertElement($this->data);
        $id = $result['id'];

        return KipoModel::redirect($this->data, $result, $id, $this->url);
    }

    protected function edit($id) {
        $viewData = $this->model->editElement($id);

        return KipoModel::renderAdminForm($viewData, $this->folderName);
    }

    protected function update($id) {
        $rules = $this->rules($this->data);
        $errors = $this->hasErrors($this->data, $rules);

        if(count($errors) > 0) {
            return $this->redirect($this->data, $errors);
        }

        $result = $this->model->updateElement($this->data, $id);

        return KipoModel::redirect($this->data, $result, $id, $this->url);
    }

    protected function destroy($id) {
        $this->model->deleteElement($id);

        return $id;
    }

    protected function active() {
        $data = Request::all();
        $data['objectId'] = $this->id;
        KipoModel::active($data, $this->className);
    }

    protected function pleach() {
        $data = Request::all();
        $data['objectId'] = $this->id;

        KipoModel::pleach($data, $this->className, false);
    }

    private function hasErrors($data, $rules) {
        foreach ($data as $key=>$value) {
            if(is_array($value)) {
                foreach ($value as $k1=>$v) {
                    if(!is_array($v)) {
                        $data[$key][$k1] = trim($v);
                    }
                }
            } else if(is_string($value)) {
                $data[$key] = trim($value);
            }
        }

        $validator = Validator::make($data, $rules);
        $validator->setAttributeNames($this->attributes());

        if($validator->fails()) {
            return $validator->errors();
        }

        return [];
    }

    private function redirect($data, $errors) {
        return redirect()->back()->withInput($data)->withErrors($errors);
    }
}
