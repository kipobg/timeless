<?php
namespace App\Http\Controllers\admin;

use App\FilmModel;
use App\Http\Requests\FileRequest;
use App\Http\Controllers\admin\BaseController as BC;


class FilmController extends BC {
    private $label;

    public function __construct() {
        $this->folderName = 'films';
        $this->url = '/admin/films';
        $this->id = 'id';
        $this->className = FilmModel::class;
        $this->aspectRatio = 2 / 3;

        parent::__construct();

        $this->label = $this->getLabel();
    }

    public function rules($data) {
        $rules = [];

        $filmId = isset($data['film-id']) ? $data['film-id'] : 0;

        $rules['status'] = 'required|between:0,1';

        foreach ($this->languages as $lang) {
            $rules['title.' . $lang->language_id] = 'required|min:3|max:255';

            $rules['cover'] = 'mimes:jpeg,jpg,png|check_size:400,null';

            if($filmId > 0) {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword,' . $filmId . ',query';
            } else {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword';
            }
        }


        return $rules;
    }

    public function attributes() {
        $attributes = [];

        foreach ($this->languages as $lang) {
            $attributes['title.' . $lang->language_id] = $this->label['title'] . '(' . $lang->language_name . ')';
            $attributes['description.' . $lang->language_id] = $this->label['description'] . '(' . $lang->language_name . ')';
            $attributes['sefurl.' . $lang->language_id] = $this->label['sefurl'] . '(' . $lang->language_name . ')';
            $attributes['meta_title.' . $lang->language_id] = $this->label['meta_title'] . '(' . $lang->language_name . ')';
            $attributes['meta_description.' . $lang->language_id] = $this->label['meta_description'] . '(' . $lang->language_name . ')';
        }


        return $attributes;
    }

    public function store() {
        $filmId = $this->data['film-id'];

        if($filmId == 0) {
            return parent::store();
        } else {
            return $this->update($filmId);
        }
    }
}