<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserModel;
use Validator;
use App\Http\Controllers\front\FrontBaseController as Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use \DB;
use App\KipoModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectPath = '/asdf';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    protected $loginView = 'kipo_front.views.auth.login';
    protected $registerView = 'kipo_front.views.profile.register';

    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $previous = session("previous_url");
        if(strpos($previous, "/checkout") !== false) {
            $previous .= "#checkout-order";
        }

        $this->redirectPath = $previous;
        $this->folder = "profile";
        parent::__construct();
        $this->label = trans("admin/constants");
        

    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        $this->view = "register";

        $breadcrumbs = [
            1 => [
                'title' => $this->label['home'],
                'href' => $this->langPrefix . '/',
            ],
            0 => [
                'title' => $this->label['register'],
                'href' => $this->langPrefix . '/register',
            ]

        ];

        $this->viewData['breadcrumbs'] = $breadcrumbs;
        $this->viewData['title'] = $this->label['register'];
        $this->viewData['description'] = $this->label['register'];

        $this->generateLanguages(false, false, "register");

        return $this->render(true);
    }


    public function logout() {
        Auth::guard($this->getGuard())->logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : $this->langPrefix . '/');
    }

    /**
     * Get a validator for an incoming registration request.
     *

     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $regions = trans("constants.front.regions");
        $regionKeys = array_keys($regions);
        $max = max($regionKeys);

        $rules = [];
        $rules['password-new'] = 'required|min:4|max:255';
        $rules['password-new-repeat'] = 'required|same:password-new';
        $rules['email-register'] = 'required|max:100|email|unique:users,email';
        $rules['region'] = 'integer|min:1|max:' . $max;
        $rules['conditions'] = 'required';

        return Validator::make($data, $rules);
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return $this->label['auth_error'];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User();

        $data['email'] = $data['email-register'];
        $data['password'] = $data['password-new'];

        $userId = $user->id;

        UserModel::addCustomerData($user, $data);
        $token = str_random(60);
        $resetToken = str_random(60);

        return $user;
    }

    protected function getUserByResentToken($resentToken) {
        $accountVerification = DB::table('account_verification')
            ->where('resent_token', $resentToken)
            ->first();

        if(empty($accountVerification)) {
            return [
                'users' => []
            ];
        }

        $user = User::find($accountVerification->user_id);

        return [
            'user' => $user,
            'token' => $accountVerification->token,
            'reset_token' => $accountVerification->resent_token
        ];
    }

    protected function registerConfirm($token) {
        $accountVerification = DB::table('account_verification')
            ->where('token', $token)
            ->first();
        if(empty($accountVerification)) {
            return false;
        }

        $user = User::find($accountVerification->user_id);

        $title = $this->label['new_user_email_title'];
        $description = sprintf($this->label['new_user_email_description'], $user->fullname);

        $secretaries = UserModel::where('group_id', 2)->get();

        foreach ($secretaries as $secretary) {
            KipoModel::sendEmail($secretary->email, $title, $description);
        }


        $user->verified = 1;
        $user->save();

        DB::table('account_verification')
            ->where('token', $token)
            ->delete();

        return true;
    }

    public function showLoginForm()
    {
        $this->folder = "auth";
        $this->view = "login";


        $this->generateLanguages(false, false, "auth");

        return $this->render(true);
    }

}
