<?php

namespace App\Http\Requests;

class SettingGalleryRequest extends Request {
    private $dbLanguages;
    private $label;

    public function __construct() {
        $this->dbLanguages = \App\Language::all();
        $this->label = trans('constants.contacts');
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'cover' => 'required|mimes:jpeg,jpg,gif,png',
        ];

        foreach ($this->dbLanguages as $lang) {
            $rules['gallery-title.' . $lang->language_id] = 'min:3|max:255';
        }

        return $rules;
    }

}
