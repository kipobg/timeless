<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingRequest extends Request {
    private $dbLanguages;

    public function __construct() {
        $this->dbLanguages = \App\Language::all();
        $this->label = trans('admin/constants.settings');
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [];

        $rules['file_terms'] = 'mimes:mp4,webm';
        $rules['file_rule'] = 'mimes:pdf';
        $rules['file_procedure'] = 'mimes:pdf';
        $rules['email'] = 'email';

        foreach ($this->dbLanguages as $lang) {
            $rules["home-meta-title." . $lang->language_id] = "required|min:3|max:255";
            $rules["home-meta-description." . $lang->language_id] = "required|min:3|max:255";
        }

        return $rules;
    }

    public function attributes() {
        $attributes = [];

        $attributes['video'] = $this->label['video'];
        $attributes['file_rule'] = $this->label['rule'];
        $attributes['file_procedure'] = $this->label['procedure'];

        foreach ($this->dbLanguages as $lang) {
            $attributes["home-meta-title." . $lang->language_id] = $this->label['home_meta_title'] . "(" . $lang->language_name . ")";
            $attributes["home-meta-description." . $lang->language_id] = $this->label['home_meta_description'] . "(" . $lang->language_name . ")";
            $attributes["home-text." . $lang->language_id] = $this->label['contacts_description'] . "(" . $lang->language_name . ")";
        }
        
        return $attributes;
    }
}
