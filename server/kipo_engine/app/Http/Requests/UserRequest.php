<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules() {
        $password = Request::get('password');
        $userId = Request::get('user-id');
        $rules = [
            'full-name' => 'required|min:2|max:255',


        ];

        if($userId > 0) {
            if(strlen($password) > 0) {
                $rules['password'] = 'min:4|max:255';
                $rules['password_confirmation'] = 'required|same:password';
            }
            $rules['email'] = 'required|email|unique:users,email,' . $userId . ',id';
        } else {
            $rules['password'] = 'required|min:4|max:255';
            $rules['password_confirmation'] = 'required|same:password';
            $rules['email'] = 'required|email|unique:users,email';
        }

        return $rules;
    }
}
