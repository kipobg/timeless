<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Input;;
class NavBarRequest extends Request {
    private $label;

    public function __construct() {
        $this->label = trans('admin/constants.nav_bar');
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [];
        $galleryId = Request::get('gallery-id');

        $rules['name'] = 'required|min:3|max:255';

        return $rules;
    }

    public function attributes() {
        $attributes = [];

        $attributes['name'] = $this->label['name'];
        return $attributes;
    }

    private function foreachElements($elements, &$container, $selector, $value, $lang, $concat = '') {
        if(isset($elements[$lang->language_id])) {
            foreach($elements[$lang->language_id] as $key=>$element) {
                $container[$selector . '.' . $lang->language_id . '.' . $key] = strlen($concat) > 2 ? $value . $$concat . '(' . $lang->language_name . ')' : $value;
            }
        }
    }
}
