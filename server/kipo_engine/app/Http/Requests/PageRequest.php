<?php

namespace App\Http\Requests;

use App\KipoModel;
use Illuminate\Support\Facades\Input;;
class PageRequest extends Request {
    private $dbLanguages;

    public function __construct() {
        $this->dbLanguages = KipoModel::getLanguages();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [];
        $pageId = Request::get('page-id');

        $rules['status'] = 'required|between:0,1';

        foreach ($this->dbLanguages as $lang) {
            $rules['title.' . $lang->language_id] = 'required|min:2|max:255';
            $rules['description.' . $lang->language_id] = 'min:3';

            if($pageId > 0){
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword,' . $pageId . ',query';
            } else {
                $rules['sefurl.' . $lang->language_id] = 'required|min:3|max:255|unique:seourl,keyword';
            }

            $this->foreachElements($this->sections, $rules, 'sections', 'required|min:10', $lang);
            $this->foreachElements($this->subsections, $rules, 'subsections', 'required|min:10', $lang);
        }


        return $rules;
    }

    public function attributes() {
        $attributes = [];

        foreach ($this->dbLanguages as $lang) {
            $attributes['title.' . $lang->language_id] = 'Заглавие(' . $lang->language_name . ')';
            $attributes['description.' . $lang->language_id] = 'Описание(' . $lang->language_name . ')';
            $attributes['sefurl.' . $lang->language_id] = 'СЕО линк(' . $lang->language_name . ')';
            $attributes['meta_title.' . $lang->language_id] = 'Мета заглавие(' . $lang->language_name . ')';
            $attributes['meta_description.' . $lang->language_id] = 'Мета описание(' . $lang->language_name . ')';

            $this->foreachElements($this->sections, $attributes, 'sections', 'Секция ', $lang, 'key');
            $this->foreachElements($this->subsections, $attributes, 'subsections', 'Подсекция ', $lang, 'key');
        }


        return $attributes;
    }

    private function foreachElements($elements, &$container, $selector, $value, $lang, $concat = '') {
        if(isset($elements[$lang->language_id])) {
            foreach($elements[$lang->language_id] as $key=>$element) {
                $container[$selector . '.' . $lang->language_id . '.' . $key] = strlen($concat) > 2 ? $value . $$concat . '(' . $lang->language_name . ')' : $value;
            }
        }
    }
}
