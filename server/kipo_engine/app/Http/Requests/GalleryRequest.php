<?php

namespace App\Http\Requests;

use App\KipoModel;
use Illuminate\Support\Facades\Input;;
class GalleryRequest extends Request {
    private $dbLanguages;

    public function __construct() {
        $this->dbLanguages = KipoModel::getLanguages();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [];

        $rules['gallery'] = 'required|mimes:jpeg,jpg,png';

        return $rules;
    }
}
