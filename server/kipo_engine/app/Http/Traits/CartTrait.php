<?php

namespace App\Http\Traits;

use App\ProductModel;
use App\RecipeModel;

trait CartTrait
{
    public function cart(bool $asHtml = true, bool $showInfo = false, array $cart = array()) {
        if(count($cart) == 0) {
            $cart = session("cart");
        }

        $result = [];
        $totalCount = 0;
        $totalPrice = 0;

        if(is_array($cart)) {
            foreach ($cart as $type=>$c) {
                $typeTitle = '';
                $primaryKey = '';
                $select = ["title", "keyword", "cover", "template", "price"];

                if($type == 1) {
                    $typeTitle = $this->label['recipes_text'];
                    $primaryKey = "recipe_id";
                    $model = new RecipeModel();
                } elseif ($type == 2) {
                    $typeTitle = $this->label['products_text'];
                    $primaryKey = "product_id";
                    $model = new ProductModel();
                    $select[] = "quantity";
                }

                $select[] = $primaryKey;

                $elements = [];

                foreach ($c as $elementId=>$quantity) {
                    $totalCount += $quantity['quantity'];
                    $elements[$elementId] = $quantity['quantity'];
                }

                if($showInfo) {
                    $whereData = array_keys($elements);
                    $whereIn = array_fill(0, count($whereData), "?");
                    $whereInRaw = '';

                    if(count($whereIn) > 0) {
                        $whereInRaw =  " AND $primaryKey IN (" . implode(", ", $whereIn) . ")";
                    }

                    $resultQuery = [];

                    if(count($elements) > 0) {
                        $resultQuery = $model->getElements($this->langId, true, true, $whereInRaw, $whereData, $select, false, -1);
                    }

                    foreach ($resultQuery as $element) {
                        $quantity = $elements[$element->$primaryKey];
                        $warning = isset($element->quantity) && $element->quantity < $quantity ? $this->messages['out-of-stock'] : "";
                        $price = $element->price;
                        $totalPrice += $price * $quantity;

                        if($warning) {
                            $this->payment = false;
                        }

                        $result[$typeTitle][$element->$primaryKey] = (object)[
                            "title" => $element->title,
                            "cover" => $element->cover,
                            "keyword" => $element->keyword,
                            "prefix" => $element->template,
                            "quantity" => $quantity,
                            "price" => $price,
                            "warning" => $warning,
                            "type" => $type,
                            "max_quantity" => isset($element->quantity) ? $element->quantity : null
                        ];
                    }

                }
            }
        }


        return [
            "totalPrice" => $totalPrice,
            "count" => $asHtml ? view($this->viewPath . '.checkout.counter', ["totalCount" => $totalCount])->render() : $totalCount,
            "quantity" => $totalCount,
            "cart_info" => $showInfo ? ($asHtml ? view($this->viewPath . '.checkout.cart', ["orderTypes" => $this->viewData['orderTypes'], "langPrefix" => $this->langPrefix, "label" => $this->label, "result" => $result, "totalCount" => $totalCount, "totalPrice" => $totalPrice])->render() : $result) : ""
        ];


    }
}
