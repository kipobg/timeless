<?php

namespace App\Http\Traits;

use Illuminate\Validation\ValidationException;

trait AbortTrait
{
    public function abortValidation($validator, $data, $code = 422) {
        throw new ValidationException($validator, response($data, $code));
    }

    public function abortCount($element, $min) {
        if(count($element) < $min) {
            abort(404);
        }
    }
}
