<?php
Route::get("/resize-img", function() {
    $images = DB::table("photography_gallery")->get();

    foreach ($images as $image) {
        $img = Image::make(public_path() . "/images/photography/gallery/" . $image->cover)
            ->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path() . "/images/photography/gallery" . '/thumbnail_' . $image->cover);


    }

    echo 'ok';
    die();
});
Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'auth_admin']], function() {
    Route::get('/', 'admin\CommercialController@index');

    Route::put('/pages/pleach', 'admin\PageController@pleach');

    Route::put('/pages/active', 'admin\PageController@active');

    Route::put('/commercials/pleach', 'admin\CommercialController@pleach');

    Route::put('/commercials/recommended', 'admin\CommercialController@active');

    Route::put('/commercials/active', 'admin\CommercialController@active');

    Route::put('/films/pleach', 'admin\FilmController@pleach');

    Route::put('/films/recommended', 'admin\FilmController@active');

    Route::put('/films/active', 'admin\FilmController@active');

    Route::put('/tv-projects/pleach', 'admin\TVProjectController@pleach');

    Route::put('/tv-projects/active', 'admin\TVProjectController@active');

    Route::put('/tv-projects/recommended', 'admin\TVProjectController@active');

    Route::put('/photography/pleach', 'admin\PhotographyController@pleach');

    Route::put('/photography/active', 'admin\PhotographyController@active');

    Route::put('/photography/recommended', 'admin\PhotographyController@active');

    Route::group(["prefix" => '/photography/gallery'], function() {
        Route::post('/', 'admin\PhotographyController@uploadGallery');

        Route::put('/pleach', 'admin\PhotographyController@pleachGallery');

        Route::delete('/{id}', 'admin\PhotographyController@deleteGallery');
    });

    Route::get('/settings', 'admin\SettingController@index');

    Route::resource('/commercials', 'admin\CommercialController');

    Route::resource('/tv-projects', 'admin\TVProjectController');

    Route::resource('/films', 'admin\FilmController');

    Route::resource('/photography', 'admin\PhotographyController');

    Route::resource('/pages', 'admin\PageController');

    Route::put('/users/disable', 'admin\UserController@disable');

    Route::resource('/users', 'admin\UserController');

    Route::resource('/settings', 'admin\SettingController');

    Route::resource('/nav-bar', 'admin\NavBarController');

    Route::put('/settings-gallery/pleach', 'admin\GalleryController@pleach');

    Route::delete('/settings-gallery/{id}', 'admin\GalleryController@destroy');

    Route::post('/settings-gallery', 'admin\GalleryController@postGallery');

    Route::put('/settings-gallery/recommended', 'admin\GalleryController@active');

    Route::get('/gallery', 'admin\GalleryController@index');

    Route::resource('/gallery', 'admin\GalleryController');
});

\App\KipoModel::localization();

Route::group(array('prefix' => Config::get('app.locale_prefix')), function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::post('/login', 'Auth\AuthController@postLogin');

        Route::get('/login', 'Auth\AuthController@showLoginForm');
        
    });

    Route::get("/commercials-ajax", 'front\CommercialController@index');

    Route::get("/projects-ajax", 'front\TVProjectController@index');

    Route::get('/', 'front\IndexController@index');

    Route::get('/photography/{keyword}', 'front\PhotographyController@show');

    Route::get('/404', 'front\FrontBaseController@page404');

    Route::get('/logout', 'Auth\AuthController@logout');

    Route::get('/{keyword1}/{keyword2?}/{keyword3?}/{keyword4?}/{keyword5?}/{keyword6?}/{keyword7?}/{keyword8?}/{keyword9?}/{keyword10?}/{keyword11?}/', 'front\PageController@show');
});