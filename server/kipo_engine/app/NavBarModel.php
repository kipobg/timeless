<?php
namespace App;
use Dompdf\FrameDecorator\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use App\KipoModel;
use App\Http\Controllers\admin\ImageController;
use \File;
use \DateTime;
use \Image;

class NavBarModel extends Model {
    private $url;
    private $folder;
    private $label;
    private $imgPath;
    private $messages;
    private $languages;
    private $userlogin;
    private $indexConstants;
    public $timestamps = false;
    protected $table = 'navigation_bar';
    protected $primaryKey = 'id';
    private $activeMenu = 'nav_bar';
    private $isAdmin = false;
    private $langId;
    private $pageModel;

    public function __construct($url = '', $folder = '', $isAdmin = false) {
        $this->isAdmin = $isAdmin;
        if($this->isAdmin) {
            $this->langId = KipoModel::getCurrentLangId();
            $this->url = $url;
            $this->label = trans('admin/constants.nav_bar');
            $this->messages = trans('admin/messages.nav_bar');
            $this->userlogin = Auth::user();
            $this->indexConstants = trans('admin/constants.index');
            $this->pageModel = new PageModel();
        }
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        if($callByController) {
            KipoModel::insertCurrentUrl();
        }

        $title = '';

        if($this->isAdmin) {
            $title = $this->label['heading_title_index'];
        }

        $navBar = self::all();

        return array(
            'userlogin' => $this->userlogin,
            'title' => $title,
            'navBar' => $navBar,
            'activeMenu' => $this->activeMenu,
            'label' => $this->indexConstants,
            'url' => $this->url,
            'folder' => $this->folder,

        );
    }

    public function insertGalleryImages($data, $navBarId) {

    }

    public function createNavBar() {
        $title = $this->label['heading_title_form'];
        $emptyLang = [];

        $selectedPages = [];
        $navBarPages = [];

        if(old()) {
            foreach (old('nav-page') as $pageId) {
                $selectedPages[] = (int)$pageId;
            }

            $navBarPages = $this->pageModel->getNavBarPages($this->langId, $selectedPages);


        }


        $navBar = (object) array(
            'name' => '',
            'status' => 0,
            'selected_pages' => $selectedPages,
            'position' => 0
        );

        $whereRaw = 'status = 1 AND seourl.language_id = pages_descriptions.language_id AND template = "pages" AND pages_descriptions.language_id = ?';

        $whereData = [
            $this->langId
        ];

        $select = array(
            'page_id',
            'title'
        );

        $pages = $this->pageModel->getPages($this->langId, $whereRaw, $whereData, $select, true);

        $viewData = array(
            'positions' => KipoModel::getPosition($this->label),
            'pages' => $pages,
            'navBarPages' => $navBarPages,
            'userlogin' => $this->userlogin,
            'title' => $title,
            'navBar' => $navBar,
            'form_data' => ['files' => true, 'role' => 'form', 'url' => $this->url, 'id' => 'data_form'],
            'label' => $this->label,
            'edit' => '',
            'activeMenu' => $this->activeMenu,
            'navBarId' => '0'
        );

        return $viewData;
    }

    public function getImages($navBarId) {
//        DB::table()
    }

    public function editNavBar($id) {
        $title = $this->label['heading_title_form'];

        $navBar = self::whereRaw($this->table . '.' . $this->primaryKey . ' = ?', array($id))
            ->get();

        $selectedPages = [];
        $navBarPages = [];

        if(old()) {
            $pages = KipoModel::getSelectedData('nav-page');

            foreach ($pages as $pageId) {
                $selectedPages[] = $pageId;
            }

        } else {
            $pages = DB::table('pages_to_nav_bar')
                ->where(array('nav_bar_id' => $id))
                ->get();

            foreach ($pages as $page) {
                $selectedPages[] = $page->page_id;
            }

        }

        $navBarPages = $this->pageModel->getNavBarPages($this->langId, $id);


        $data = (object) array(
            'name' => '',
            'selected_pages' => $selectedPages,
            'position' => 0
        );

        if(count($navBar) > 0) {
            $data->name = $navBar[0]->name;
            $data->position = $navBar[0]->position;
        }

        $whereRaw = 'status = 1 AND seourl.language_id = pages_descriptions.language_id AND template = "pages" AND pages_descriptions.language_id = ?';

        $whereData = [
            $this->langId
        ];

        $select = array(
            'page_id',
            'title'
        );

        $pages = $this->pageModel->getPages($this->langId, $whereRaw, $whereData, $select, true);

        $viewData =  array(
            'positions' => KipoModel::getPosition($this->label),
            'navBarPages' => $navBarPages,
            'pages' => $pages,
            'userlogin' => $this->userlogin,
            'form_gallery' => ['role' => 'form', 'files' => true, 'url' => '/admin/gallery/upload', 'id' => 'form_gallery'],
            'title' => $title,
            'languages' => $this->languages,
            'navBar' => $data,
            'form_data' => ['role' => 'form', 'url' => $this->url . '/' . $id, 'method' => 'put', 'id' => 'data_form', 'files' => true],
            'label' => $this->label,
            'activeMenu' => $this->activeMenu,
            'edit' => ' edit',
            'navBarId' => $id,
        );
        return $viewData;
    }

    public function updateNavBar($data, $id) {
        $navBar = self::find($id);

        DB::table('pages_to_nav_bar')->where('nav_bar_id', $id)->delete();

        $this->addNavBarData($data, $navBar);

        return [ 'msg' => $this->messages['success-update-messages'], 'id' => $id ];
    }

    public function insertNavBar($data) {
        $navBar = new NavBarModel();

        $navBarId = $this->addNavBarData($data, $navBar);

        return ['id' => $navBarId, 'msg' => $this->messages['success-insert-messages']];
    }

    private function addNavBarData($data, $navBar) {
        $data['pleach'] = json_decode($data['pages-serialize']);
        $date = new DateTime;

        $pages = KipoModel::checkIsSet($data, 'nav-page', '', []);
        $pagesData = [];

        $navBar->name = KipoModel::checkIsSet($data, 'name', '', '');
        $navBar->position = KipoModel::checkIsSet($data, 'position', '', '0');
        $navBar->save();
        $navBarId = $navBar->id;

        $pagesData = [];

        $this->addNavPages($data['pleach'], $pagesData, $navBarId);

        $data['objectId'] = 'page_id';


        if(!empty($pagesData)) {
            DB::table('pages_to_nav_bar')->insert($pagesData);
        }
        self::pleach($data, $navBarId, NavBarPagesModel::class);

        return $navBarId;
    }

    private function addNavPages($data, &$dd, $navBarId) {
        foreach ($data as $navBar) {
            $dd[] = [
                'nav_bar_id' => $navBarId,
                'page_id' => $navBar->navId,
            ];

            if(isset($navBar->children) && count($navBar->children) > 0) {
                $this->addNavPages($navBar->children, $dd, $navBarId);
            }
        }
    }

    public static function pleach($data, $navBar, $obj, $hasParent = true) {
        foreach ($data['pleach'] as $key=>$el) {
            $whereData = array(
                'order_number' => ($key+1)
            );

            if($hasParent) {
                $whereData['parent_id'] = 0;
            }

            $obj::where([$data['objectId'] => $el->navId, 'nav_bar_id' => $navBar])
                ->update($whereData);
            self::addChildren($el, $navBar, $obj, $data['objectId'], $hasParent);
        }

    }

    public static function addChildren($el, $navBar, $obj, $id, $hasParent = true) {
        if(isset($el->children)) {
            foreach ($el->children as $key_s=>$el_s) {
                $whereData = array(
                    'order_number' => ($key_s+1)
                );

                if($hasParent) {
                    $whereData['parent_id'] = $el->navId;
                }

                $obj::where([$id => $el_s->navId, 'nav_bar_id' => $navBar])
                    ->update($whereData);
                self::addChildren($el_s, $navBar, $obj, $id, $hasParent);
            }
        }
    }

    public function deleteNavBar($id) {
        self::where($this->primaryKey, $id)->delete();
        DB::table('pages_to_nav_bar')->where('nav_bar_id', $id)->delete();


    }

    public function takeNavBar($take = 3) {
        $langId = KipoModel::getCurrentLangId();

        $navBar = GalleryModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('status > 0 AND seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '_descriptions.language_id = ? AND template = ?', array($langId, $this->table))
            ->orderBy('date_published', 'DESC')
            ->select(
                array(
                    $this->table . '.' . $this->primaryKey,
                    'title',
                    'date_published',
                    'keyword'
                ))
            ->take($take)
            ->get();

        return $navBar;
    }



    public function getNavBar($where) {
        $langId = KipoModel::getCurrentLangId();

        $navBar = self::where($where)
            ->first();


        return $navBar;
    }

    public function getNewsByKeyword($keyword) {
        $langId = KipoModel::getCurrentLangId();
        $navBar = GalleryModel::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '_descriptions.language_id = ? AND template = ? AND keyword = ?', array($langId, $this->table, $keyword))
            ->first();

        return $navBar;
    }
}