<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use App\KipoModel;
use App\Http\Controllers\admin\ImageController;
use \File;

class NavBarPagesModel extends Model {
    private $indexConstants;
    private $languages;
    public $timestamps = false;
    protected $table = 'pages_to_nav_bar';
    protected $primaryKey = 'id';
    protected $folder;
    protected $fillable =  [
        'order_number',
        'nav_bar_id',
        'page_id',
        'order_number',
        'category_id',
    ];

    public static function getNavBarByPageId($pageId, $position = 1) {
        $navBar = self::join('navigation_bar', 'pages_to_nav_bar.nav_bar_id', '=', 'navigation_bar.id')
            ->where(['page_id' => $pageId, 'position' => $position])
            ->select('nav_bar_id')
            ->first();

        return $navBar;
    }

}