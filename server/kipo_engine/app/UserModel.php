<?php
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use DateTime;

class UserModel extends Model {
    private $userlogin;
    private $languages;
    private $label;
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private $activeMenu = 'users';
    private $messages;
    private $url;

    public function __construct($url = '', $isFront = false) {
        if(!$isFront) {
            $this->userlogin = Auth::user();
            $this->languages = Language::all();
            $this->label = trans('admin/constants.users');
            $this->messages = trans('admin/messages.users');
            $this->url = $url;

        }
    }

    public function showUsers() {
        $title = $this->label['heading_title_index'];

        $users = [];//User::select(array('id', 'status', 'email', 'created_at', 'fullname', 'group_id'))->where(["group_id" => 0])->get();
        $admins = User::select(array('id', 'status', 'email', 'created_at', 'fullname', 'group_id'))->where(["group_id" => 1])->get();
        return array(
            'userlogin' => $this->userlogin,
            'title' => $title,
            'users' => $users,
            'admins' => $admins,
            'activeMenu' => $this->activeMenu,
            'url' => $this->url,

        );
    }

    public function createUser() {
        $title = $this->label['heading_title_form'];
        $user = (object) array(
            'fullname' => '',
            'email' => '',
            'password' => '',
            'receive_email' => 0,
            'rep_password' => '',
            'selectedMethods' => array(),
        );

        if(count(old('pages')) > 0) {
            $user->selectedMethods = $this->getSelectedPages();
        }

        if(old('type')) {
            $groupId = old('type');
        }



        $methods = trans('admin.constants.input-methods');

        return array(
            'label' => $this->label,
            'userlogin' => $this->userlogin,
            'methods' => $methods,
            'title' => $title,
            'user' => $user,
            'form_data' => ['role' => 'form', 'files' => true, 'url' => '/admin/users', 'id' => 'data_form'],
            'languages' => $this->languages,
            'activeMenu' => $this->activeMenu,
            'userId' => 0,
            'url' => $this->url
        );
    }

    public function editUser($id) {
            $title = $this->label['heading_title_form'];

            $userQuery = UserModel::find($id);

            $user = (object) array(
                'fullname' => $userQuery->fullname,
                'email' => $userQuery->email,
                'password' => '',
                'rep_password' => '',
                'selectedMethods' => array(),
                'receive_email' => $userQuery->receive_email
            );

            $methods = trans('admin.constants.input-methods');


        return array(
                'label' => $this->label,
                'userlogin' => $this->userlogin,
                'title' => $title,
                'user' => $user,
                'form_data' => ['role' => 'form', 'files' => true, 'url' => '/admin/users/' . $id, 'id' => 'data_form', 'method' => 'put'],
                'languages' => $this->languages,
                'activeMenu' => $this->activeMenu,
                'userId' => $id,
                'methods' => $methods,
                'url' => $this->url
            );
    }

    public function updateUser($id, $data) {
        $user = UserModel::find($id);
//        $data['password'] = '';
        $this->addDataToObj($data, $user);

        return [ 'msg' => $this->messages['success-insert-messages'] ];
    }

    public function insertUser($data) {
        $user = new UserModel();

        $user->access_admin = 1;
//        $data['password'] = self::generatePassword();
        $userId = $this->addDataToObj($data, $user);

        if(!isset($data['languageId'])) {
            $data['languageId'] = KipoModel::getCurrentLangId();
        }

        return [ 'id' => $userId, 'msg' => $this->messages['success-insert-messages'] ];
    }

    public static function insertUserBaseData($data, $user) {
        $password = $data['password'];

        $date = new DateTime;
        $fullName = $data['full-name'];

        if(is_array($data['full-name'])) {
            $fullName = reset($data['full-name']);
        }

        $user->fullname = $fullName;
        $user->verified = 1;
        $user->group_id = 1;
        $user->email = $data['email'];
        $user->receive_email = $data['receive-email'];
        $userId = $user->id;

        

        if(mb_strlen($password) > 0) {
            $user->password = Hash::make($password);
        }


        $user->created_at = $date;
        $user->save();

        return $user->id;
    }

    public function addDataToObj($data, &$user) {
        $pagesMethods = [];

        $userId = self::insertUserBaseData($data, $user);

        $pages = KipoModel::checkIsSet($data, 'pages', '', []);

        foreach($pages as $pageId=>$methods) {
            foreach($methods as $methodId=>$bool) {
                $pagesMethods[] = array(
                    'user_id' => $userId,
                    'page_id' => $pageId,
                    'method_id' => $methodId
                );
            }
        }

        return $userId;
    }

    public function deleteUser($id) {
        UserModel::destroy($id);

    }

    public static function generatePassword($length = 15) {
        $password_string = '!@#$%*&abcdefghijklmnpqrstuwxyzABCDEFGHJKLMNPQRSTUWXYZ23456789';
        $password = substr(str_shuffle($password_string), 0, 15);

        return $password;
    }

    public function getSecretaryEmail() {
        $user = UserModel::where(['group_id' => 2, 'status' => '1'])
            ->inRandomOrder()
            ->select(array('email'))
            ->get();

        return $user;
    }

    public function disableUser($id, $status) {
        $user = UserModel::find($id);
        $user->status = $status;
        $user->save();
    }


    public static function addCustomerData(User &$user, array $data) {
        $firstName = KipoModel::checkIsSet($data, 'first-name', "", "");
        $lastName = KipoModel::checkIsSet($data, 'last-name', "", "");

        $user->fullname = ($firstName ? $firstName . " " : "") . $lastName;
        $user->email = $data['email'];
        $user->phone = $data['phone'];

        if($data['password']) {
            $user->password = bcrypt($data['password']);
        }

        $user->sex = '1';
        $user->group_id = 0;
        $user->save();

        $userId = $user->id;

        $customerData = [
            'user_id' => $userId,
            'first_name' => KipoModel::checkIsSet($data, "first-name", "", ""       ),
            'last_name' => KipoModel::checkIsSet($data, "last-name",        "", ""),
            'phone' => KipoModel::checkIsSet($data, "phone", "", ""),
            'address' => KipoModel::checkIsSet($data, "address", ""     , ""),
            'city' => KipoModel::checkIsSet($data, "city", "", ""),
            'region' => KipoModel::checkIsSet($data, "region", "", 0),
        ];

        DB::table("customer_data")->insert($customerData);
    }

    public static function deleteCustomerData($id) {
        DB::table("customer_data")->where("user_id", $id)->delete();
    }

    public function getClientData($clientId) {
        $clientData = DB::table("customer_data")->where("user_id", $clientId)->first();

        return $clientData;
    }

    public function getFullName() {
        return $this->fullname;
    }
}
