<?php
namespace App;

use App\BaseModel as BM;
use \File;
use App\Http\Controllers\admin\ImageController;
use \DB;

class FilmModel extends BM {
    protected  $table = 'films';
    protected  $primaryKey = 'id';
    protected  $constantsName = 'films';
    protected  $foreignKey = 'film_id';
    protected $className = self::class;
    protected $categoryModel;

    public function __construct($url = '', $folder = '', $isAdmin = false, $languages = [], $isGet = false) {
        parent::__construct($url, $folder, $isAdmin, $languages, $isGet);

        if(!$isGet && $isAdmin) {
            $this->baseData['status'] = ['default' => '0', 'index' => 'status', 'key' => ''];

            $this->baseDataDescription['title'] = ['default' => '', 'index' => 'title', 'key' => 'languageId'];
            $this->baseDataDescription['description'] = ['default' => '', 'index' => 'description', 'key' => 'languageId'];
            $this->baseDataDescription['meta_title'] = ['default' => '', 'index' => 'meta_title', 'key' => 'languageId'];
            $this->baseDataDescription['meta_description'] = ['default' => '', 'index' => 'meta_description', 'key' => 'languageId'];
        }

        $this->resizeWidth = ["large" => 400];
        $this->viewData['dimension'] = '';
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        $this->viewData['films'] = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->whereRaw($this->table . '_descriptions.language_id = ? ', array($langId))
            ->orderBy('order_number')
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'language_id',
                    'cover',
                    'id',
                )
            )
            ->get();

        return parent::index($langId, $callByController, $paginate);
    }

    public function createElement() {
        $film = new \stdClass();

        $film->title = $this->emptyLang;
        $film->description = $this->emptyLang;
        $film->sefurl = $this->emptyLang;
        $film->alergens = $this->emptyLang;
        $film->meta_title = $this->emptyLang;
        $film->meta_description = $this->emptyLang;
        $film->cover = '';
        $film->status = 0;

        $this->viewData['prefix'] = 'films';
        $this->viewData['film'] = $film;
        $this->viewData['filmId'] = 0;
//        $this->viewData['categories-recipes'] = $categories-recipes;

        return parent::createElement();
    }

    public function editElement($id) {
        $film = new \stdClass();

        $film->title = $this->emptyLang;
        $film->description = $this->emptyLang;
        $film->alergens = $this->emptyLang;
        $film->sefurl = $this->emptyLang;
        $film->meta_title = $this->emptyLang;
        $film->meta_description = $this->emptyLang;
        $film->selected_categories = array();
        $film->cover = '';
        $film->status = 0;

//        $film->category_id = 0;

        $filmQuery = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '.' . $this->primaryKey . ' = ? AND template = ?', array($id, $this->table))
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'seourl.language_id',
                    'keyword',
                    'cover'

                )
            )
            ->get();

        if(count($filmQuery) > 0) {
            $film->cover = $filmQuery[0]->cover;
            $film->status = $filmQuery[0]->status;

        }

        foreach ($filmQuery as $result) {
            $film->title[$result->language_id] = $result->title;
            $film->description[$result->language_id] = $result->description;
            $film->alergens[$result->language_id] = $result->alergens;
            $film->meta_description[$result->language_id] = $result->meta_description;
            $film->meta_title[$result->language_id] = $result->meta_title;
            $film->sefurl[$result->language_id] = $result->keyword;
        }


        $this->viewData['film'] = $film;
        $this->viewData['filmId'] = $id;
//        $this->viewData['categories-recipes'] = $categories-recipes;


        return parent::editElement($id);
    }

    public static function pageModule() {
        $module = (object)[
            'controller' => 'App\Http\Controllers\front\FilmController',
            'method' => 'index',
            'index' => 'film',
            'view' => 'film'
        ];

        return $module;
    }

    public function deleteElement($id)
    {
        parent::deleteElement($id); // TODO: Change the autogenerated stub
    }
}
