<?php
namespace App;

use App\BaseModel as BM;
use \File;
use App\Http\Controllers\admin\ImageController;
use \DB;

class TVProjectModel extends BM {
    protected  $table = 'tv_projects';
    protected  $primaryKey = 'id';
    protected  $constantsName = 'tv_projects';
    protected  $foreignKey = 'tv_p_id';
    protected $className = self::class;
    protected $categoryModel;

    public function __construct($url = '', $folder = '', $isAdmin = false, $languages = [], $isGet = false) {
        parent::__construct($url, $folder, $isAdmin, $languages, $isGet);

        if(!$isGet && $isAdmin) {
            $this->baseData['status'] = ['default' => '0', 'index' => 'status', 'key' => ''];

            $this->baseDataDescription['title'] = ['default' => '', 'index' => 'title', 'key' => 'languageId'];
            $this->baseDataDescription['description'] = ['default' => '', 'index' => 'description', 'key' => 'languageId'];
            $this->baseDataDescription['meta_title'] = ['default' => '', 'index' => 'meta_title', 'key' => 'languageId'];
            $this->baseDataDescription['meta_description'] = ['default' => '', 'index' => 'meta_description', 'key' => 'languageId'];
        }

        $this->resizeWidth = ["large" => 500];
        $this->viewData['dimension'] = '8/5';
    }

    public function index($langId, $callByController = false, $paginate = 15) {
        $this->viewData['tvProjects'] = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->whereRaw($this->table . '_descriptions.language_id = ? ', array($langId))
            ->orderBy('order_number')
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'language_id',
                    'cover',
                    'id',
                )
            )
            ->get();



        return parent::index($langId, $callByController, $paginate);
    }

    public function createElement() {
        $tvProject = new \stdClass();

        $tvProject->title = $this->emptyLang;
        $tvProject->description = $this->emptyLang;
        $tvProject->sefurl = $this->emptyLang;
        $tvProject->alergens = $this->emptyLang;
        $tvProject->meta_title = $this->emptyLang;
        $tvProject->meta_description = $this->emptyLang;
        $tvProject->video_code = '';
        $tvProject->cover = '';
        $tvProject->type = '';
        $tvProject->status = 0;

        $this->viewData['prefix'] = 'tv_projects';
        $this->viewData['tvProject'] = $tvProject;
        $this->viewData['tvProjectId'] = 0;
//        $this->viewData['categories-recipes'] = $categories-recipes;

        return parent::createElement();
    }

    public function editElement($id) {
        $tvProject = new \stdClass();

        $tvProject->title = $this->emptyLang;
        $tvProject->description = $this->emptyLang;
        $tvProject->alergens = $this->emptyLang;
        $tvProject->sefurl = $this->emptyLang;
        $tvProject->meta_title = $this->emptyLang;
        $tvProject->meta_description = $this->emptyLang;
        $tvProject->selected_categories = array();
        $tvProject->cover = '';
        $tvProject->type = '';
        $tvProject->video_code = '';
        $tvProject->status = 0;

//        $tvProject->category_id = 0;

        $tvProjectQuery = self::join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->join('seourl', $this->table . '.' . $this->primaryKey, '=', 'seourl.query')
            ->whereRaw('seourl.language_id = ' . $this->table . '_descriptions.language_id AND ' . $this->table . '.' . $this->primaryKey . ' = ? AND template = ?', array($id, $this->table))
            ->select(
                array(
                    'title',
                    'description',
                    'meta_title',
                    'meta_description',
                    'status',
                    'seourl.language_id',
                    'keyword',
                    'cover',
                    'video',
                    'type',

                )
            )
            ->get();

        if(count($tvProjectQuery) > 0) {
            $tvProject->cover = $tvProjectQuery[0]->cover;
            $tvProject->type = $tvProjectQuery[0]->type;
            $tvProject->status = $tvProjectQuery[0]->status;
//            $tvProject->category_id = $tvProjectQuery[0]->category_id;
            $tvProject->video_code = $tvProjectQuery[0]->video;

        }

        foreach ($tvProjectQuery as $result) {
            $tvProject->title[$result->language_id] = $result->title;
            $tvProject->description[$result->language_id] = $result->description;
            $tvProject->alergens[$result->language_id] = $result->alergens;
            $tvProject->meta_description[$result->language_id] = $result->meta_description;
            $tvProject->meta_title[$result->language_id] = $result->meta_title;
            $tvProject->sefurl[$result->language_id] = $result->keyword;
        }


        $this->viewData['tvProject'] = $tvProject;
        $this->viewData['tvProjectId'] = $id;
//        $this->viewData['categories-recipes'] = $categories-recipes;


        return parent::editElement($id);
    }

    protected function addElementData($data, $element)
    {
        $videoLink = KipoModel::checkIsSet($data, "video-code", '', '');

        if(filter_var($videoLink, FILTER_VALIDATE_URL)) {
            $video = KipoModel::getVideoCode($videoLink, $this->videoType);
            $type = KipoModel::getVideoType($videoLink, $this->videoType);

            $element->video = $video;
            $element->type = $type;
        }

        return parent::addElementData($data, $element);
    }

    public static function pageModule() {
        $module = (object)[
            'controller' => 'App\Http\Controllers\front\TVProjectController',
            'method' => 'index',
            'index' => 'tvProject',
            'view' => 'tvProject'
        ];

        return $module;
    }

    public function deleteElement($id)
    {
        parent::deleteElement($id); // TODO: Change the autogenerated stub
    }
}
