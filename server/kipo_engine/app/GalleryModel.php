<?php
namespace App;
use App\Http\Controllers\admin\ImageController;
use App\KipoModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Config;
use \File;
use Illuminate\Support\Facades\Input;
use \Image;

class GalleryModel extends Model {
    public $timestamps = false;
    protected $table = 'settings_gallery';
    protected $primaryKey = 'id';
    private $foreignKey = 'gallery_id';
    private $folder;
    private $videoFolder;
    private $label;
    private $messages;
    private $languages;
    private $userlogin;
    private $activeMenu = 'gallery';
    private $imgPath;
    private $pdfPath;
    private $pdfFolder = 'pdf';
    private $galleryFolder;
    private $partnerFolder = "partners";

    public function __construct() {
        $this->folder = 'gallery';
        $this->videoFolder = $this->folder . '/video';
        $this->label = trans('admin/constants.gallery');
        $this->messages = trans('admin/messages.gallery');
        $this->languages = KipoModel::getLanguages();
        $this->userlogin = Auth::user();
        $this->imgPath = Config::get('app.image_path');
        $this->pdfPath = public_path($this->pdfFolder);
        $this->galleryFolder = $this->folder;
    }

    public function createGallery() {
        $title = $this->label['heading_title_form'];
        $textRemove = $this->label['text_remove'];

        $viewData = array(
            'gallery' => $this->getAllGallery(),
            'textRemove' => $textRemove,
            'userlogin' => $this->userlogin,
            'title' => $title,
            'url' => '/admin/users',
            'languages' => $this->languages,
            'form_gallery' => ['role' => 'form', 'files' => true, 'url' => '/admin/settings-gallery', 'id' => 'form_brand'],
            'label' => $this->label,
            'edit' => '',
            'activeMenu' => $this->activeMenu,
            'settingId' => '0',
            'folder' => $this->videoFolder,
            'imgPath' => KipoModel::getImgPath(),
            'pdfFolder' => $this->pdfFolder,
        );

        return $viewData;
    }

    public function insertGallery($data) {
        $videoName = '';
        $imgName = '';

        if(isset($data['cover'])) {
            $cover = $data['cover'];

            $imgName = ImageController::changeImageName($cover);

            KipoModel::saveImage($cover, $this->imgPath . $this->galleryFolder, $imgName);

        } else {
            abort('404');
        }

        if(isset($data['video'])) {
            $video = $data['video'];

            $videoName = ImageController::changeImageName($video);

            $video->move($this->imgPath . $this->galleryFolder, $videoName);

        }

        $galleryId = DB::table('settings_gallery')->insertGetId(array(
            'cover' => $imgName,
            'video' => $videoName
        ));

        $galleryDescription = [];


        foreach ($this->languages as $language) {
            $langId = $language->language_id;

            $galleryDescription[] = [
                'title' => KipoModel::checkIsSet($data, 'gallery-title', $langId, ''),
                'description' => KipoModel::checkIsSet($data, 'gallery-description', $langId, ''),
                'url' => KipoModel::checkIsSet($data, 'gallery-url', $langId, ''),
                'btn_text' => KipoModel::checkIsSet($data, 'btn-text', $langId, ''),
                'btn_type' => Input::get('btn-type'),
                'language_id' => $langId,
                'gallery_id' => $galleryId,

            ];
        }

        DB::table($this->table . '_descriptions')->insert($galleryDescription);
        return $galleryId;
    }

    public function getGallery($galleryId) {
        return DB::table('settings_gallery')
            ->join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->where(['id' => $galleryId, 'language_id' => KipoModel::getCurrentLangId()])->get();

    }

    public function getAllGallery($where = []) {
        $where['language_id'] = KipoModel::getCurrentLangId();

        return DB::table('settings_gallery')
            ->join($this->table . '_descriptions', $this->table . '.' . $this->primaryKey, '=', $this->table . '_descriptions.' . $this->foreignKey)
            ->where($where)->orderBy("id")->get();

    }

    public function deleteGallery($galleryId) {
        $gallery = DB::table('settings_gallery')->where('id', $galleryId)->first();

        File::delete([$this->imgPath . $this->galleryFolder . '/' . $gallery->cover, $this->imgPath . $this->galleryFolder . '/' . $gallery->video]);

        DB::table('settings_gallery')->where('id', $galleryId)->delete();
        DB::table($this->table . '_descriptions')->where('gallery_id', $galleryId)->delete();
    }

    public static function pageModule() {
        $module = (object)[
            'controller' => 'App\Http\Controllers\front\GalleryController',
            'method' => 'index',
            'index' => 'gallery',
            'view' => 'gallery'
        ];

        return $module;
    }

}