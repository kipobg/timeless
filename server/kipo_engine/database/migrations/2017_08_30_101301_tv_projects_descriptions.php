<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TvProjectsDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_projects_descriptions', function (Blueprint $table) {
            $table->integer("tv_p_id");
            $table->string("title");
            $table->string("meta_title");
            $table->string("meta_description");
            $table->text("description");
            $table->integer("language_id");

            $table->index("tv_p_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tv_projects_descriptions');
    }
}
