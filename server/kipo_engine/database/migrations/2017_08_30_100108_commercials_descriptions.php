<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommercialsDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercials_descriptions', function (Blueprint $table) {
            $table->integer("commercial_id");
            $table->string("title");
            $table->string("meta_title");
            $table->string("meta_description");
            $table->text("description");
            $table->integer("language_id");

            $table->index("commercial_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commercials_descriptions');
    }
}
