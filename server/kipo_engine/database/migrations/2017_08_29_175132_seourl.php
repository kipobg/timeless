<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Seourl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seourl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("language_id");
            $table->string("template", 64);
            $table->integer("query");
            $table->string("keyword", 255);

            $table->index("keyword");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seourl');
    }
}
