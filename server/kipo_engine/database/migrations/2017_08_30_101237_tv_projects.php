<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TvProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("order_number");
            $table->integer("status");
            $table->string("video");
            $table->string("cover");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tv_projects');
    }
}
