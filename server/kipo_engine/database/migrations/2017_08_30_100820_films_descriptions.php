<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilmsDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films_descriptions', function (Blueprint $table) {
            $table->integer("film_id");
            $table->string("title");
            $table->string("meta_title");
            $table->string("meta_description");
            $table->text("description");
            $table->integer("language_id");

            $table->index("film_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('films_descriptions');
    }
}
