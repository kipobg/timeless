<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographyGalleryUpdate2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photography_gallery', function (Blueprint $table) {
            $table->dropColumn("gallery_id");

        });

        Schema::table('photography_gallery_descriptions', function (Blueprint $table) {
            $table->integer("gallery_id");

        });
    }


}
