<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographyUpdate3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photography', function (Blueprint $table) {
            $table->dropColumn("cover_list");
            $table->string("list_cover");
            $table->string("cover2");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
