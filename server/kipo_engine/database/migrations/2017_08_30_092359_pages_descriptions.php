<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_descriptions', function (Blueprint $table) {
            $table->integer("page_id");
            $table->string("title", 255);
            $table->longText("description");
            $table->string("meta_title", 255);
            $table->string("meta_description", 255);
            $table->integer("language_id");

            $table->index("page_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages_descriptions');
    }
}
