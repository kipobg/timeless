<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("group_id");
            $table->string("fullname", 255);
            $table->string("email", 255)->unique();
            $table->string("password", 64);
            $table->string("remember_token", 255);
            $table->integer("access_admin");
            $table->tinyInteger("verified")->default(1);
            $table->tinyInteger("status")->default(1);
            $table->tinyInteger("receive_email");
            $table->timestamp('created_at')->useCurrent();

            $table->index("email");
            $table->index("group_id");
        });

        \DB::table("users")->insert([
            [
                "group_id" => 1,
                "fullname" => "Христо Христов",
                "email" => "hristo@kipo.bg",
                "password" => '$2y$10$boew6sdvNqzM4rTl7x16UOLbCIebDHnY/SHS8WT7AMQlUT6ds64ia',
                "access_admin" => "1",
                "receive_email" => "1"
            ],
            [
                "group_id" => 1,
                "fullname" => "Николай Маринов",
                "email" => "niki@kipo.bg",
                "password" => '$2y$10$9XluNOfhjbDQxyOpVb22WuFffiAKAjFGg/07/CYX0jd/Fh6k28nOi',
                "access_admin" => "1",
                "receive_email" => "1"
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
