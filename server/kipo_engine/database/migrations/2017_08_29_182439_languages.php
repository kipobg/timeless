<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Languages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('language_id');
            $table->string("language_name", 64);
            $table->string("language_code", 5);
            $table->string("local", 64);
            $table->integer("status");
            $table->integer("order_number");
        });

        \DB::table("languages")->insert([
            [
                "language_name" => "Български",
                "language_code" => "bg",
                "local" => "bg_BG",
                "status" => "1",
                "order_number" => "1",
            ],
            [
                "language_name" => "English",
                "language_code" => "en",
                "local" => "en_EN",
                "status" => "1",
                "order_number" => "2",
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('languages');
    }
}
