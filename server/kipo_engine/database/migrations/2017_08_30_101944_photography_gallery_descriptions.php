<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographyGalleryDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photography_gallery_descriptions', function (Blueprint $table) {
            $table->integer("photography_gallery_id");
            $table->string("title");
            $table->integer("language_id");

            $table->index("photography_gallery_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photography_gallery_descriptions');
    }
}
