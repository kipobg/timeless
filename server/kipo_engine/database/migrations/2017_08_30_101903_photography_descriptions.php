<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographyDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photography_descriptions', function (Blueprint $table) {
            $table->integer("photography_id");
            $table->string("title");
            $table->string("meta_title");
            $table->string("meta_description");
            $table->integer("language_id");

            $table->index("photography_id");
            $table->index("language_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photography_descriptions');
    }
}
