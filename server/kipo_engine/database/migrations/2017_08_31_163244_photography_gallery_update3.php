<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographyGalleryUpdate3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photography_gallery', function (Blueprint $table) {
            $table->integer("photography_id");

        });

        Schema::table('photography_gallery_descriptions', function (Blueprint $table) {
            $table->dropColumn("photography_gallery_id");

        });
    }


}
