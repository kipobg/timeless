<?php

return array(
    'front' => [
        'page_title' => [
            'login_title' => 'Login',
            'profile' => 'Profile',
            'edit_profile' => 'Редактиране на Профила',
            'order_details' => 'Order details',
        ],
        "home" => "Home",
        "heading" => [
            "service" => "Service",
            "production" => "Production",
            "creative" => "Creative",
            "casting" => "Casting",
            "post_production" => "Post Production",
            "equipment" => "Equipment",
            "locations" => "Locations",
            "commercials" => "Commercials",
            "films" => "Films",
            "tv_projects" => "TV Projects",
            "photography" => "Photography Works",
        ],
        "page404" => "404 - Page not found"
    ],
);
