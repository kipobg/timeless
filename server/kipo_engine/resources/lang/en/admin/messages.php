<?php
return array(
    "users" => array(
        'success-insert-messages' => 'Потребителят е добавен успешно',
        'success-update-messages' => 'Потребителят е редактиран успешно',
    ),
    "pages" => array(
        'success-insert-messages' => 'Страницата е добавена успешно',
        'success-update-messages' => 'Страницата е редактирана успешно',
    ),
    'settings' => array(
        'success-insert-messages' => 'Настройките бяха променени успешно',
        'success-update-messages' => 'Клиентът е редактиран успешно'
    ),
    'commercials' => array(
        'success-insert-messages' => 'Елементът беше добавен успешно',
        'success-update-messages' => 'Елементът беше редактиран успешно'
    ),
    'tv_projects' => array(
        'success-insert-messages' => 'Проектът беше добавен успешно',
        'success-update-messages' => 'Проектът беше редактиран успешно'
    ),
    'films' => array(
        'success-insert-messages' => 'Филмът беше добавен успешно',
        'success-update-messages' => 'Филмът беше редактиран успешно'
    ),
    'photography' => array(
        'success-insert-messages' => 'Фотографията беше добавена успешно',
        'success-update-messages' => 'Фотографията беше редактирана успешно'
    ),
    'nav_bar' => array(
        'success-insert-messages' => 'Менюто е добавено успешно',
        'success-update-messages' => 'Менюто е редактирано успешно'
    ),
);


