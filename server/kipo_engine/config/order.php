<?php

return [
    "types" => [
        1 => 'recipe',
        2 => 'product',
    ],
    "free_deliver_price" => 50
];